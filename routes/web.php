<?php

use App\Http\Controllers\RouteController;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\SalesOrdering\AdditionalOrderDatatablesController;
use App\Http\Controllers\SalesOrdering\AdditionalOrderSweetAlertController;
use App\Http\Controllers\SalesOrdering\FixOrderAjaxController;
use App\Http\Controllers\SalesOrdering\FixOrderDatatableController;
use App\Http\Controllers\SalesOrdering\FixOrderSweetAlertController;
use App\Http\Livewire\AfterSales\Warranty\Convert\ConvertIndex;
use App\Http\Livewire\Page\Home\HomeIndex;
use App\Http\Livewire\Page\About\AboutIndex;
use App\Http\Livewire\Page\Artisan\ArtisanIndex;
use App\Http\Livewire\Page\ChildMenu\ChildMenuIndex;
use App\Http\Livewire\Page\Login\LoginIndex;
use App\Http\Livewire\Page\MenuUserGroup\MenuUserGroupIndex;
use App\Http\Livewire\Page\ParentMenu\ParentMenuIndex;
use App\Http\Livewire\Page\Register\RegisterIndex;
use App\Http\Livewire\Page\SubChildMenu\SubChildMenuIndex;
use App\Http\Livewire\Page\SubSubChildMenu\SubSubChildMenuIndex;
use App\Http\Livewire\Page\UserGroup\UserGroupIndex;
use App\Http\Livewire\Page\User\UserIndex;
use App\Http\Livewire\SalesOrdering\AdditionalOrder\AdditionalOrderAdd;
use App\Http\Livewire\SalesOrdering\AdditionalOrder\AdditionalOrderEdit;
use App\Http\Livewire\SalesOrdering\AdditionalOrder\AdditionalOrderIndex;
use App\Http\Livewire\SalesOrdering\FixOrder\FixOrderAdd;
use App\Http\Livewire\SalesOrdering\AllocatedAtpm\AllocatedAtpmIndex;
use App\Http\Livewire\SalesOrdering\AMConfirmation\AMConfirmationIndex;
use App\Http\Livewire\SalesOrdering\ApprovalBM\ApprovalBMIndex;
use App\Http\Livewire\SalesOrdering\ApprovedBM\ApprovedBMIndex;
use App\Http\Livewire\SalesOrdering\ColourMazda\ColourMazdaIndex;
use App\Http\Livewire\SalesOrdering\FixOrder\FixOrderAtpm;
use App\Http\Livewire\SalesOrdering\FixOrder\FixOrderEdit;
use App\Http\Livewire\SalesOrdering\FixOrder\FixOrderIndex;
use App\Http\Livewire\SalesOrdering\FixOrder\FixOrderPrinciple;
use App\Http\Livewire\SalesOrdering\MasterMonth\MasterMonthIndex;
use App\Http\Livewire\SalesOrdering\ModelColourMazda\ModelColourMazdaIndex;
use App\Http\Livewire\SalesOrdering\MonthException\MonthExceptionIndex;
use App\Http\Livewire\SalesOrdering\RangeMonthRule\RangeMonthRuleIndex;
use App\Http\Livewire\SalesOrdering\Report\ReportIndex;
use App\Http\Livewire\SalesOrdering\ReportAnalyst\ReportAnalystIndex;
use App\Http\Livewire\SalesOrdering\SubmitAtpm\SubmitAtpmIndex;
use App\Http\Livewire\SalesOrdering\ModelMazda\ModelMazdaIndex;
use App\Http\Livewire\SalesOrdering\TypeModelMazda\TypeModelMazdaIndex;
use App\Http\Livewire\SalesOrdering\UserRole\UserRoleIndex;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [RouteController::class, 'index']);
Route::get('/logout', [RouteController::class, 'logout'])->name('logout');

Route::get('/login', LoginIndex::class)->middleware('guest')->name('login.index');
Route::get('/register', RegisterIndex::class)->name('register.index');

Route::middleware('user.session')->group(function() {
    Route::get('/home', HomeIndex::class)->name('home.index');
    Route::get('/about', AboutIndex::class)->name('about.index');

    // Additional Order
    Route::get('/sales/dealer/additional-order', AdditionalOrderIndex::class)->name('additional-order.index');
    Route::get('/sales/dealer/additional-order/add', AdditionalOrderAdd::class)->name('additional-order.add');
    Route::get('/sales/dealer/additional-order/edit/{id?}', AdditionalOrderEdit::class)->name('additional-order.edit');

    // Fix Order
    Route::get('/sales/dealer/fix-order', FixOrderIndex::class)->name('fix-order.index');
    Route::get('/sales/dealer/fix-order/add/{idMonth?}', FixOrderAdd::class)->name('fix-order.add');
    Route::get('/sales/dealer/fix-order/edit/{id?}/{idRule?}', FixOrderEdit::class)->name('fix-order.edit');
});

// Approval BM
Route::middleware(['user.session', 'bm.session'])->group(function() {
    Route::get('/sales/dealer/approval-bm', ApprovalBMIndex::class)->name('approval-bm.index');
    Route::get('/sales/dealer/approved-bm', ApprovedBMIndex::class)->name('approved-bm.index');
    Route::get('/sales/dealer/fix-order-bm', FixOrderPrinciple::class)->name('fix-order.principle');
    Route::get('/sales/dealer/report-order', ReportIndex::class)->name('report-order-dealer.index');
    Route::get('/sales/dealer/report-analyst', ReportAnalystIndex::class)->name('report-analyst-dealer.index');
});

// APproval ATPM
Route::middleware(['user.session', 'atpm.session'])->group(function() {
    Route::get('/sales/atpm/submit-atpm', SubmitAtpmIndex::class)->name('submit-atpm.index');
    Route::get('/sales/atpm/am-confirmation', AMConfirmationIndex::class)->name('am-confirmation.index');
    Route::get('/sales/atpm/allocated-atpm', AllocatedAtpmIndex::class)->name('allocated-atpm.index');
    Route::get('/sales/atpm/fix-order-atpm', FixOrderAtpm::class)->name('fix-order.atpm');
    Route::get('/sales/atpm/report-order', ReportIndex::class)->name('report-order-atpm.index');
    Route::get('/sales/atpm/report-analyst', ReportAnalystIndex::class)->name('report-analyst-atpm.index');
    Route::get('/sales/atpm/user-role', UserRoleIndex::class)->name('user-role.index');
});

// Datatable Json
Route::middleware('user.session')->prefix('datatable')->group(function() {
    // Additional Order
    Route::get('additionalOrderJsonDraft', [AdditionalOrderDatatablesController::class, 'additionalOrderJsonDraft']);
    Route::get('additionalOrderJsonWaitingApprovalDealerPrinciple', [AdditionalOrderDatatablesController::class, 'additionalOrderJsonWaitingApprovalDealerPrinciple']);
    Route::get('additionalOrderJsonApprovalDealerPrinciple', [AdditionalOrderDatatablesController::class, 'additionalOrderJsonApprovalDealerPrinciple']);
    Route::get('additionalOrderJsonSubmittedATPM', [AdditionalOrderDatatablesController::class, 'additionalOrderJsonSubmittedATPM']);
    Route::get('additionalOrderJsonATPMAllocation', [AdditionalOrderDatatablesController::class, 'additionalOrderJsonATPMAllocation']);
    Route::get('additionalOrderJsonAMConfirmation', [AdditionalOrderDatatablesController::class, 'additionalOrderJsonAMConfirmation']);
    Route::get('additionalOrderJsonCanceled/{idCancel?}', [AdditionalOrderDatatablesController::class, 'additionalOrderJsonCanceled']);
    Route::get('detailAdditionalOrderJson/{id}', [AdditionalOrderDatatablesController::class, 'detailAdditionalOrderJson']);
    Route::get('additionalOrderJsonReport', [AdditionalOrderDatatablesController::class, 'additionalOrderJsonReport']);
    Route::get('additionalOrder/getById/{id}', [AdditionalOrderDatatablesController::class, 'getById']);

    // Fix Order
    Route::get('fixOrderJson', [FixOrderDatatableController::class, 'fixOrderJson']);
    Route::get('FixOrderJsonApprovalBM', [FixOrderDatatableController::class, 'FixOrderJsonApprovalBM']);
    Route::get('detailFixOrderJson/{id}', [FixOrderDatatableController::class, 'detailFixOrderJson']);
    Route::get('subDetailFixOrderJson/{id}', [FixOrderDatatableController::class, 'subDetailFixOrderJson']);
    Route::get('FixOrderJsonConfirmationAtpm', [FixOrderDatatableController::class, 'FixOrderJsonConfirmationAtpm']);
    Route::get('FixOrderJsonAllocationAtpm', [FixOrderDatatableController::class, 'FixOrderJsonAllocationAtpm']);
    Route::get('fixOrderJsonReport', [FixOrderDatatableController::class, 'fixOrderJsonReport']);
    Route::get('fixOrder/getByIdForAllocation/{id}', [FixOrderDatatableController::class, 'getByIdForAllocation']);
});

// Sweet Alert
Route::middleware('user.session')->prefix('sweetalert')->group(function() {
    Route::post('additionalOrder/sendToApproval', [AdditionalOrderSweetAlertController::class, 'sendToApproval']);
    Route::post('additionalOrder/approvedBM', [AdditionalOrderSweetAlertController::class, 'approvedBM']);
    Route::post('additionalOrder/submitToAtpm', [AdditionalOrderSweetAlertController::class, 'submitToAtpm']);
    Route::post('additionalOrder/reviseBMDealer', [AdditionalOrderSweetAlertController::class, 'reviseBMDealer']);
    Route::post('additionalOrder/cancelBMDealer', [AdditionalOrderSweetAlertController::class, 'cancelBMDealer']);
    Route::post('additionalOrder/submittedAtpm', [AdditionalOrderSweetAlertController::class, 'submittedAtpm']);
    Route::post('additionalOrder/reviseSubmittedAtpm', [AdditionalOrderSweetAlertController::class, 'reviseSubmittedAtpm']);
    Route::post('additionalOrder/cancelSubmitATPM', [AdditionalOrderSweetAlertController::class, 'cancelSubmitATPM']);
    Route::post('additionalOrder/AMConfirmation', [AdditionalOrderSweetAlertController::class, 'AMConfirmation']);
    Route::post('additionalOrder/reviseAMConfirmation', [AdditionalOrderSweetAlertController::class, 'reviseAMConfirmation']);
    Route::post('additionalOrder/cancelAMConfirmation', [AdditionalOrderSweetAlertController::class, 'cancelAMConfirmation']);
    Route::post('additionalOrder/cancelAllocatedATPM', [AdditionalOrderSweetAlertController::class, 'cancelAllocatedATPM']);

    // Fix Order
    Route::post('fixOrder/sendToApproval', [FixOrderSweetAlertController::class, 'sendToApproval']);
    Route::post('fixOrder/approvalBM', [FixOrderSweetAlertController::class, 'approvalBM']);
    Route::post('fixOrder/planningToAtpm', [FixOrderSweetAlertController::class, 'planningToAtpm']);
    Route::post('fixOrder/reviseBM', [FixOrderSweetAlertController::class, 'reviseBM']);
    Route::post('fixOrder/submitToAtpm', [FixOrderSweetAlertController::class, 'submitToAtpm']);
    Route::post('fixOrder/confirmAM', [FixOrderSweetAlertController::class, 'confirmAM']);
    Route::post('fixOrder/allocatedAtpm', [FixOrderSweetAlertController::class, 'allocatedAtpm']);
});

// Ajax
Route::middleware('user.session')->prefix('ajax')->group(function() {
    Route::get('fixOrder/rangeMonthFixOrder', [FixOrderAjaxController::class, 'rangeMonthFixOrder']);
    Route::get('fixOrder/getRangeMonthId', [FixOrderAjaxController::class, 'getRangeMonthId']);
});

Route::middleware('admin.session')->prefix('admin')->group(function() {
    Route::get('/user', UserIndex::class)->name('user.index');
    Route::get('/user-group', UserGroupIndex::class)->name('user-group.index');
    Route::get('/parent-menu', ParentMenuIndex::class)->name('parent-menu.index');
    Route::get('/child-menu', ChildMenuIndex::class)->name('child-menu.index');
    Route::get('/sub-child-menu', SubChildMenuIndex::class)->name('sub-child-menu.index');
    Route::get('/sub-sub-child-menu', SubSubChildMenuIndex::class)->name('sub-sub-child-menu.index');
    Route::get('/menu-user-group', MenuUserGroupIndex::class)->name('menu-user-group.index');
    Route::get('/master-month', MasterMonthIndex::class)->name('master-month.index');
    Route::get('/month-exception', MonthExceptionIndex::class)->name('month-exception.index');
    Route::get('/range-month-rule', RangeMonthRuleIndex::class)->name('range-month-rule.index');
    Route::get('/model-mazda', ModelMazdaIndex::class)->name('admin-model-mazda.index');
    Route::get('/type-model-mazda', TypeModelMazdaIndex::class)->name('admin-type-model-mazda.index');
    Route::get('/model-colour-mazda', ModelColourMazdaIndex::class)->name('admin-model-colour-mazda.index');
    Route::get('/colour-mazda', ColourMazdaIndex::class)->name('admin-colour-mazda.index');
    Route::get('/artisan', ArtisanIndex::class)->name('artisan.index');
});

Route::middleware('atpm-admin.session')->prefix('atpm-admin')->group(function() {
    Route::get('/master-month', MasterMonthIndex::class)->name('master-month-atpm-admin.index');
    Route::get('/month-exception', MonthExceptionIndex::class)->name('month-exception-atpm-admin.index');
    Route::get('/range-month-rule', RangeMonthRuleIndex::class)->name('range-month-rule-atpm-admin.index');
    Route::get('/model-mazda', ModelMazdaIndex::class)->name('atpm-model-mazda.index');
    Route::get('/type-model-mazda', TypeModelMazdaIndex::class)->name('atpm-type-model-mazda.index');
    Route::get('/model-colour-mazda', ModelColourMazdaIndex::class)->name('atpm-model-colour-mazda.index');
    Route::get('/colour-mazda', ColourMazdaIndex::class)->name('atpm-colour-mazda.index');
});

Route::middleware('atpm.session')->prefix('after-sales')->group(function() {
    Route::prefix('/warranty')->group(function() {
        Route::prefix('/atpm')->group(function() {
            Route::get('/convert', ConvertIndex::class)->name('after-sales.warranty.atpm.convert');
        });
    });
});
