<?php

namespace App\Traits;

use Illuminate\Support\Facades\Config;

trait WithWrsApi
{

    public string $wrsApi;
    public string $wrsAfterSalesApi;

    public function __construct()
    {
        $this->wrsApi = Config::get('constants.wrs_api');
        $this->wrsAfterSalesApi = Config::get('constants.wrs_aftersales_api');
    }
}
