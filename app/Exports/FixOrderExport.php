<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class FixOrderExport implements FromCollection, WithHeadings, WithMapping, WithColumnFormatting, ShouldAutoSize, WithStyles, WithStrictNullComparison
{
    protected $dateStart;
    protected $dateEnd;
    protected $idDealer;
    protected $statusOrder;

    public function __construct($dateStart, $dateEnd, $idDealer, $statusOrder)
    {
        $this->dateStart = $dateStart;
        $this->dateEnd = $dateEnd;
        $this->idDealer = $idDealer;
        $this->statusOrder = $statusOrder;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $data = DB::table('view_fix_order');
        $data = $data->where('status', '1');

        if($this->idDealer != '-') {
            $data = $data->where('id_dealer', $this->idDealer);
        }

        if($this->dateStart != '' && $this->dateEnd != '') {
            $data = $data->where('date_save_order', '>=', $this->dateStart.' 00:00:00')
                    ->where('date_save_order', '<=', $this->dateEnd.' 23:59:59');
        }

        if($this->statusOrder != '-') {
            if($this->statusOrder == 'draft') {
                $data = $data->whereIn('flag_send_approval_dealer', ['0', '2'])
                ->where('flag_approval_dealer', '0')
                ->where('flag_submit_to_atpm', '0')
                ->where('flag_allocation', '0');
            } else if($this->statusOrder == 'waiting-approval') {
                $data = $data->where('flag_send_approval_dealer', '1')
                        ->where('flag_approval_dealer', '0')
                        ->where('flag_submit_to_atpm', '0')
                        ->where('flag_allocation', '0');
            } else if($this->statusOrder == 'approved') {
                $data = $data->where('flag_send_approval_dealer', '1')
                        ->where('flag_approval_dealer', '1')
                        ->where('flag_submit_to_atpm', '0')
                        ->where('flag_allocation', '0');
            } else if($this->statusOrder == 'submitted') {
                $data = $data->where('flag_send_approval_dealer', '1')
                        ->where('flag_approval_dealer', '1')
                        ->where('flag_submit_to_atpm', '1')
                        ->where('flag_allocation', '0');
            } else if($this->statusOrder == 'allocated') {
                 $data = $data->where('flag_send_approval_dealer', '1')
                        ->where('flag_approval_dealer', '1')
                        ->where('flag_submit_to_atpm', '1')
                        ->where('flag_allocation', '1');
                }
        }

        $data = $data->orderBy('no_order_dealer', 'asc');
        return $data->get();
    }

    public function headings(): array
    {
        return [
            'No Order Dealer',
            'No Order ATPM',
            'Nama Dealer',
            'User Order',
            'Submitted By',
            'Allocated By',
            'Month Order',
            'Year Order',
            'Status Order',
            'Model Name',
            'Type Name',
            'Colour Name',
            'Qty Order',
            'Qty Input',
            'Qty Remains',
            'Total Qty',
            'Remark Revise',
            'Remark Cancel',
            'Date Revise',
            'Date Cancel',
            'Date Draft',
            'Date Send Approval To BM',
            'Date Approved By BM',
            'Date Submitted To ATPM',
            'Date Confirmed By AM',
            'Date Allocated'
        ];
    }

    public function map($row): array
    {
        return [
            $row->no_order_dealer,
            $row->no_order_atpm,
            $row->nama_dealer,
            $row->user_order,
            $row->nama_submitted_by,
            $row->nama_allocated_by,
            $row->id_month,
            $row->year_order,
            $this->checkStatusProgress($row),
            $row->model_name,
            $row->type_name,
            $row->colour_name,
            $row->qty,
            $row->qty_input,
            $row->qty_diff,
            $row->total_qty,
            $row->remark_revise,
            $row->remark_cancel,
            $this->dateTime($row->date_revise),
            $this->dateTime($row->date_cancel),
            $this->dateTime($row->date_save_order),
            $this->dateTime($row->date_send_approval),
            $this->dateTime($row->date_approval),
            $this->dateTime($row->date_submit_atpm_order),
            $this->dateTime($row->date_am_confirmation),
            $this->dateTime($row->date_allocation_atpm),
            $this->dateTime($row->date_planning),
        ];
    }

    public function columnFormats(): array
    {
        return [
            'S' => NumberFormat::FORMAT_DATE_DATETIME,
            'T' => NumberFormat::FORMAT_DATE_DATETIME,
            'U' => NumberFormat::FORMAT_DATE_DATETIME,
            'V' => NumberFormat::FORMAT_DATE_DATETIME,
            'W' => NumberFormat::FORMAT_DATE_DATETIME,
            'X' => NumberFormat::FORMAT_DATE_DATETIME,
            'Y' => NumberFormat::FORMAT_DATE_DATETIME,
            'Z' => NumberFormat::FORMAT_DATE_DATETIME,
            //'AA' => NumberFormat::FORMAT_DATE_DATETIME,
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => ['font' => ['bold' => true, 'size' => 12]],
        ];
    }

    private function dateTime($date): string
    {
        return $date ? date("d-M-Y H:i:s", strtotime($date)) : '';
    }

    private function checkStatusProgress($data)
    {
        $flag_send_approval_dealer = $data->flag_send_approval_dealer;
        $flag_approval_dealer = $data->flag_approval_dealer;
        $flag_submit_to_atpm = $data->flag_submit_to_atpm;
        $flag_allocation = $data->flag_allocation;
        $flag_planning = $data->flag_planning;
        $flag_am_confirmation = $data->flag_am_confirmation;

        if($flag_planning == '1') {
            $statusProgress = 'Planning';
        } else if ($flag_send_approval_dealer == '0' && $flag_approval_dealer == '0'
            && $flag_submit_to_atpm == '0' && $flag_am_confirmation == '0' && $flag_allocation == '0') {
            $statusProgress = 'Draft';
        } else if ($flag_send_approval_dealer == '1' && $flag_approval_dealer == '0'
            && $flag_submit_to_atpm == '0' && $flag_am_confirmation == '0' && $flag_allocation == '0') {
            $statusProgress = 'Waiting Approval';
        } else if ($flag_send_approval_dealer == '1' && $flag_approval_dealer == '1'
            && $flag_submit_to_atpm == '0' && $flag_am_confirmation == '0' && $flag_allocation == '0') {
            $statusProgress = 'Approved';
        } else if ($flag_send_approval_dealer == '1' && $flag_approval_dealer == '1'
            && $flag_submit_to_atpm == '1' && $flag_am_confirmation == '0' && $flag_allocation == '0') {
            $statusProgress = 'Submitted';
        } else if ($flag_send_approval_dealer == '1' && $flag_approval_dealer == '1'
            && $flag_submit_to_atpm == '1' && $flag_am_confirmation == '1' && $flag_allocation == '0') {
            $statusProgress = 'AM Confirmed';
        } else if ($flag_send_approval_dealer == '1' && $flag_approval_dealer == '1'
            && $flag_submit_to_atpm == '1' && $flag_am_confirmation == '1' && $flag_allocation == '1') {
            $statusProgress = 'Allocated';
        } elseif ($flag_send_approval_dealer == '2' && $flag_approval_dealer == '0'
            && $flag_submit_to_atpm == '0'  && $flag_am_confirmation == '0' && $flag_allocation == '0') {
            $statusProgress = 'Revised';
        } else {
            $statusProgress = '-';
        }

        return $statusProgress;
    }
}
