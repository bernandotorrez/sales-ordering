<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class UserRoleExport implements FromCollection, WithHeadings, WithMapping, WithColumnFormatting, ShouldAutoSize, WithStyles
{
    protected $status;

    public function __construct($status)
    {
       if($status != 'all') {
            $this->status = ($status == 'whitelist') ? 0 : 1;
       } else {
            $this->status = $status;
       }
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $data = DB::table('view_user');
        $data = $data->where('status', '1');

        if($this->status != 'all') {
            $data = $data->where('flag_blacklist', $this->status);
        }

        $data = $data->orderBy('username', 'asc');

        return $data->get();
    }

    public function headings(): array
    {
        return [
            'Username',
            'Name',
            'Email',
            'Kode Dealer',
            'Dealer Name',
            'Status Blacklist',
            'Date Blacklist',
            'Remarks Blacklist',
            'Blacklist By',
            'Date Whitelist',
            'Remarks Whitelist',
            'Whitelist By',
        ];
    }

    public function map($row): array
    {
        return [
            $row->username,
            $row->nama_user,
            $row->email,
            $row->id_dealer,
            $row->dealer_name,
            ($row->flag_blacklist == 1) ? 'Blacklisted' : 'Whitelisted',
            $this->dateTime($row->date_blacklist),
            $row->remarks_blacklist,
            $row->blacklist_by,
            $this->dateTime($row->date_whitelist),
            $row->remarks_whitelist,
            $row->whitelist_by,
        ];
    }

    private function dateTime($date): string
    {
        return $date ? date("d-M-Y H:i:s", strtotime($date)) : '';
    }

    public function columnFormats(): array
    {
        return [
            'D' => NumberFormat::FORMAT_NUMBER,
            'G' => NumberFormat::FORMAT_DATE_DATETIME,
            'J' => NumberFormat::FORMAT_DATE_DATETIME,
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => ['font' => ['bold' => true, 'size' => 12]],
        ];
    }
}
