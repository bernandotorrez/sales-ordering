<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cache extends Model
{
    use HasFactory;

    protected $table = 'tbl_cache';
    protected $guarded = ['id'];

    protected $searchableColumn =  [
        'id', 
        'cache_name', 
        'id_user',
    ];

    public function getSearchableColumn()
    {
        return $this->searchableColumn;
    }
}
