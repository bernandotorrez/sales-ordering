<?php

namespace App\Models\SalesOrdering;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MazdaModel extends Model
{
    use HasFactory;

    protected $table = 'tbl_model_mazda';
    protected $primaryKey = 'id_model_mazda';
    protected $guarded = ['id_model_mazda'];
    protected $searchableColumn = [
        'id_model',
        'name_model',
        'kind_vehicle',
        'model_year',
        'engine_type',
        'vehicle_type',
        'wmi',
        'model_moc',
        'flag_show_additional',
        'flag_show_fix',
        'status'
    ];

    public function getSearchableColumn()
    {
        return $this->searchableColumn;
    }

    public function mazdaColors()
    {
        return $this->hasMany(ModelColourMazda::class, 'id_model');
    }
}
