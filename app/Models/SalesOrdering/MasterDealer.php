<?php

namespace App\Models\SalesOrdering;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterDealer extends Model
{
    use HasFactory;

    protected $table = 'tbl_master_dealer';
    protected $primaryKey = 'id_master_dealer';
    protected $guarded = ['id_master_dealer'];
    protected $searchableColumn = [
        'id_master_dealer',
        'id_dealer',
        'dealer_name',
        'address',
        'phone',
        'fax',
        'email',
        'status'
    ];

    public function getSearchableColumn()
    {
        return $this->searchableColumn;
    }
}
