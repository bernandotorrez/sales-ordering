<?php

namespace App\Models\SalesOrdering;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModelColourMazda extends Model
{
    use HasFactory;

    protected $table = 'tbl_model_colour_mazda';
    protected $primaryKey = 'id_model_colour_mazda';
    protected $guarded = ['id_model_colour_mazda'];
    protected $searchableColumn = [
        'id_model_colour_mazda',
        'id_model_colour',
        'id_model',
        'id_colour_mazda',
        'id_colour',
        'name_model',
        'name_colour',
        'name_colour_global',
        'flag_show_additional_model_colour',
        'flag_show_fix_model_colour',
        'status'
    ];

    public function getSearchableColumn()
    {
        return $this->searchableColumn;
    }

    public function mazdaModel()
    {
        return $this->belongsTo(MazdaModel::class, 'id_model');
    }

    public function mazdaColour()
    {
        return $this->belongsTo(ColourMazda::class, 'id_colour');
    }
}
