<?php

namespace App\Models\SalesOrdering;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MonthExceptionRule extends Model
{
    use HasFactory;

    protected $table = 'tbl_month_exception_rule';
    protected $primaryKey = 'id_rule_month';
    protected $guarded = ['id_rule_month'];
    protected $searchableColumn = [
        'id_month',
        'month_id_to',
        'date_input_lock_month_start',
        'date_input_lock_month_end',
        'id_dealer',
        'nama_dealer',
        'flag_open_colour',
        'flag_open_volume',
        'nama_id_month',
        'nama_month_id_to'
    ];

    public function getSearchableColumn()
    {
        return $this->searchableColumn;
    }

    public function month()
    {
        return $this->belongsTo(MasterMonthOrder::class, 'id_month');
    }
}
