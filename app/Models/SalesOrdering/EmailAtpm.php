<?php

namespace App\Models\SalesOrdering;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmailAtpm extends Model
{
    use HasFactory;

    protected $table = 'tbl_email_atpm';
    protected $primaryKey = 'id_email_atpm';
    protected $guarded = ['id_email_atpm'];

    protected $searchableColumn =  [
        'id_email_atpm',
        'email_atpm',
        'id_dealer_level',
    ];

    public function getSearchableColumn()
    {
        return $this->searchableColumn;
    }

}
