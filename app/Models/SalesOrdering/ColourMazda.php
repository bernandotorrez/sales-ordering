<?php

namespace App\Models\SalesOrdering;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ColourMazda extends Model
{
    use HasFactory;

    protected $table = 'tbl_colour_mazda';
    protected $primaryKey = 'id_colour_mazda';
    protected $guarded = ['id_colour_mazda'];
    protected $searchableColumn = [
        'id_colour_mazda',
        'id_colour',
        'name_colour',
        'name_colour_global',
        'flag_show_additional',
        'flag_show_fix',
        'status'
    ];

    public function getSearchableColumn()
    {
        return $this->searchableColumn;
    }

    public function mazdaColors()
    {
        return $this->hasMany(ModelColourMazda::class, 'id_colour');
    }
}
