<?php

namespace App\Models\SalesOrdering;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeModelMazda extends Model
{
    use HasFactory;

    protected $table = 'tbl_type_model_mazda';
    protected $primaryKey = 'id_type_model_mazda';
    protected $guarded = ['id_type_model_mazda'];
    protected $searchableColumn = [
        'id_type_model_mazda',
        'id_type_model',
        'id_model',
        'name_type',
        'msc_code',
        'cylinder',
        'msc_code',
        'price',
        'tpt',
        'sut',
        'flag_show_additional_type_model',
        'flag_show_fix_type_model',
        'status'
    ];

    public function getSearchableColumn()
    {
        return $this->searchableColumn;
    }
}
