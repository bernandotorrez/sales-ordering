<?php

namespace App\Http\Controllers\SalesOrdering;

use App\Http\Controllers\Controller;
use App\Repository\Eloquent\MasterFixOrderRepository;
use App\Repository\Eloquent\MasterMonthOrderRepository;
use App\Repository\Eloquent\MonthExceptionRuleRepository;
use App\Repository\Eloquent\RangeMonthFixOrderRepository;
use Illuminate\Http\Request;

class FixOrderAjaxController extends Controller
{
    public function rangeMonthFixOrder(
        Request $request,
        RangeMonthFixOrderRepository $rangeMonthFixOrderRepository,
        MasterMonthOrderRepository $masterMonthOrderRepository,
        MasterFixOrderRepository $masterFixOrderRepository,
        MonthExceptionRuleRepository $monthExceptionRuleRepository
    ) {
        $idUser = session()->get('user')['id_user'];
        $idDealer = session()->get('user')['id_dealer'];
        $idMonth = $request->get('idMonth');
        $monthIdTo = $request->get('monthIdTo');
        $cache_name = 'fixOrder-ButtonRule-idUser-' . $idUser . '-idMonth-' . $idMonth . '-monthIdTo-' . $monthIdTo;
        $data = $rangeMonthFixOrderRepository->getByIdMonthAndMonthIdTo($idMonth, $monthIdTo);

        $dataMonthExceptionRule = $monthExceptionRuleRepository->getByIdDealerAndIdMonthAndMonthId($idDealer, date('m'), $monthIdTo);
        $masterMonth = $masterMonthOrderRepository->getById(date('m'));

        if ($dataMonthExceptionRule) {
            $dataLockDate = $dataMonthExceptionRule;
            $date_input_lock_start = $dataLockDate->date_input_lock_month_start;
            $date_input_lock_end = $dataLockDate->date_input_lock_month_end;
        } else {
            $dataLockDate = $masterMonth;
            $date_input_lock_start = $dataLockDate->date_input_lock_start;
            $date_input_lock_end = $dataLockDate->date_input_lock_end;
        }

        $checkBeforeOrAfter = eval("return ((string) date('Y-m-d') $masterMonth->operator_start '$date_input_lock_start')
                    && ((string) date('Y-m-d') $masterMonth->operator_end '$date_input_lock_end');");

        $where = array(
            'status' => '1',
            'id_dealer' => $idDealer,
            'id_month' => $monthIdTo
        );
        $countOrder = $masterFixOrderRepository->findDuplicate($where);

        return response()->json([
            'checkBeforeOrAfter' => $checkBeforeOrAfter,
            'countOrder' => $countOrder,
            'data' => $data
        ], 200);
    }

    public function getRangeMonthId(Request $request, RangeMonthFixOrderRepository $rangeMonthFixOrderRepository)
    {
        $idMonth = $request->get('idMonth');
        $monthIdTo = $request->get('monthIdTo');
        $idUser = session()->get('user')['id_user'];
        $cache_name = 'fixOrder-ButtonRule-idUser-' . $idUser . '-idMonth-' . $idMonth . '-monthIdTo-' . $monthIdTo;
        $data = $rangeMonthFixOrderRepository->getByIdMonthAndMonthIdTo($idMonth, $monthIdTo);

        return response()->json([
            'data' => $data
        ], 200);
    }
}
