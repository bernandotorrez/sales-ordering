<?php

namespace App\Http\Controllers\SalesOrdering;

use App\Http\Controllers\Controller;
use App\Repository\Eloquent\DetailAdditionalOrderRepository;
use App\Repository\Eloquent\MasterAdditionalOrderRepository;
use App\Repository\Eloquent\CacheRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Yajra\Datatables\Datatables;

class AdditionalOrderDatatablesController extends Controller
{
    public function additionalOrderJsonDraft(
        MasterAdditionalOrderRepository $masterAdditionalOrderRepository,
        CacheRepository $cacheRepository
    ) {
        $idUser = session()->get('user')['id_user'];
        $idDealer = session()->get('user')['id_dealer'];
        $cache_name = 'datatable-additionalOrderJsonDraft-idUser-' . $idUser . '-idDealer-' . $idDealer;
        $datas = Cache::remember($cache_name, 10,
            function () use ($masterAdditionalOrderRepository, $cacheRepository, $idUser, $idDealer, $cache_name) {
                $cacheRepository->firstOrCreate(['cache_name' => $cache_name, 'id_user' => $idUser]);
                return $masterAdditionalOrderRepository->getDraft($idDealer);
            });

        //$datas = $masterAdditionalOrderRepository->getDraft($idDealer);

        return Datatables::of($datas)
            ->addColumn('action', function ($data) {
                return '<label class="new-control new-checkbox checkbox-outline-primary  m-auto">
                <input type="checkbox" class="new-control-input child-chk checkId"
                onclick="updateCheck(' . $data->id_master_additional_order_unit . ')"
                id="' . $data->id_master_additional_order_unit . '"
                value="' . $data->id_master_additional_order_unit . '">
                <span class="new-control-indicator"></span><span style="visibility:hidden">c</span>
                </label>
                ';
            })
            ->addColumn('details_url', function ($data) {
                return url('datatable/detailAdditionalOrderJson/' . $data->id_master_additional_order_unit);
            })
            ->make(true);
    }

    public function additionalOrderJsonWaitingApprovalDealerPrinciple(
        MasterAdditionalOrderRepository $masterAdditionalOrderRepository,
        CacheRepository $cacheRepository
    ) {
        $idUser = session()->get('user')['id_user'];
        $idDealer = session()->get('user')['id_dealer'];
        $cache_name = 'datatable-additionalOrderJsonWaitingApprovalDealerPrinciple-idUser-' . $idUser . '-idDealer-' . $idDealer;
        $datas = Cache::remember($cache_name, 10,
            function () use ($masterAdditionalOrderRepository, $cacheRepository, $idUser, $idDealer, $cache_name) {
                $cacheRepository->firstOrCreate(['cache_name' => $cache_name, 'id_user' => $idUser]);
                return $masterAdditionalOrderRepository->getWaitingApprovalDealerPrinciple($idDealer);
            });

        //$datas = $masterAdditionalOrderRepository->getWaitingApprovalDealerPrinciple($idDealer);

        return Datatables::of($datas)
            ->addColumn('action', function ($data) {
                return '<label class="new-control new-checkbox checkbox-outline-primary  m-auto">
                <input type="checkbox" class="new-control-input child-chk checkId"
                onclick="updateCheck(' . $data->id_master_additional_order_unit . ')"
                id="' . $data->id_master_additional_order_unit . '"
                value="' . $data->id_master_additional_order_unit . '">
                <span class="new-control-indicator"></span><span style="visibility:hidden">c</span>
                </label>
                ';
            })
            ->addColumn('details_url', function ($data) {
                return url('datatable/detailAdditionalOrderJson/' . $data->id_master_additional_order_unit);
            })
            ->make(true);
    }

    public function additionalOrderJsonApprovalDealerPrinciple(
        MasterAdditionalOrderRepository $masterAdditionalOrderRepository,
        CacheRepository $cacheRepository
    ) {
        $idUser = session()->get('user')['id_user'];
        $idDealer = session()->get('user')['id_dealer'];
        $cache_name = 'datatable-additionalOrderJsonApprovalDealerPrinciple-idUser-' . $idUser . '-idDealer-' . $idDealer;
        $datas = Cache::remember($cache_name, 10,
            function () use ($masterAdditionalOrderRepository, $cacheRepository, $idUser, $idDealer, $cache_name) {
                $cacheRepository->firstOrCreate(['cache_name' => $cache_name, 'id_user' => $idUser]);
                return $masterAdditionalOrderRepository->getApprovalDealerPrinciple($idDealer);
            });

        //$datas = $masterAdditionalOrderRepository->getApprovalDealerPrinciple($idDealer);

        return Datatables::of($datas)
            ->addColumn('action', function ($data) {
                return '<label class="new-control new-checkbox checkbox-outline-primary  m-auto">
                <input type="checkbox" class="new-control-input child-chk checkId"
                onclick="updateCheck(' . $data->id_master_additional_order_unit . ')"
                id="' . $data->id_master_additional_order_unit . '"
                value="' . $data->id_master_additional_order_unit . '">
                <span class="new-control-indicator"></span><span style="visibility:hidden">c</span>
                </label>
                ';
            })
            ->addColumn('details_url', function ($data) {
                return url('datatable/detailAdditionalOrderJson/' . $data->id_master_additional_order_unit);
            })
            ->make(true);
    }

    public function additionalOrderJsonSubmittedATPM(
        MasterAdditionalOrderRepository $masterAdditionalOrderRepository,
        CacheRepository $cacheRepository
    ) {
        $idUser = session()->get('user')['id_user'];
        $idDealer = session()->get('user')['id_dealer'];
        $dealers = session()->get('area_manager');
        $cache_name = 'datatable-additionalOrderJsonSubmittedATPM-idUser-' . $idUser . '-idDealer-' . $idDealer;
        $datas = Cache::remember($cache_name, 10,
            function () use ($masterAdditionalOrderRepository, $cacheRepository, $idUser, $idDealer, $cache_name, $dealers) {
                $cacheRepository->firstOrCreate(['cache_name' => $cache_name, 'id_user' => $idUser]);
                return $masterAdditionalOrderRepository->getSubmittedATPM($idDealer, $dealers);
            });

        //$datas = $masterAdditionalOrderRepository->getSubmittedATPM();

        return Datatables::of($datas)
            ->addColumn('action', function ($data) {
                return '<label class="new-control new-checkbox checkbox-outline-primary  m-auto">
                <input type="checkbox" class="new-control-input child-chk checkId"
                onclick="updateCheck(' . $data->id_master_additional_order_unit . ')"
                id="' . $data->id_master_additional_order_unit . '"
                value="' . $data->id_master_additional_order_unit . '">
                <span class="new-control-indicator"></span><span style="visibility:hidden">c</span>
                </label>
                ';
            })
            ->addColumn('details_url', function ($data) {
                return url('datatable/detailAdditionalOrderJson/' . $data->id_master_additional_order_unit);
            })
            ->make(true);
    }

    public function additionalOrderJsonAMConfirmation(
        MasterAdditionalOrderRepository $masterAdditionalOrderRepository,
        CacheRepository $cacheRepository
    ) {
        $idUser = session()->get('user')['id_user'];
        $idDealer = session()->get('user')['id_dealer'];
        $cache_name = 'datatable-additionalOrderJsonAMConfirmation-idUser-' . $idUser . '-idDealer-' . $idDealer;
        $datas = Cache::remember($cache_name, 10,
            function () use ($masterAdditionalOrderRepository, $cacheRepository, $idUser, $cache_name, $idDealer) {
                $cacheRepository->firstOrCreate(['cache_name' => $cache_name, 'id_user' => $idUser]);
                return $masterAdditionalOrderRepository->getAMConfirmation($idDealer);
            });

        //$datas = $masterAdditionalOrderRepository->getATPMAllocation();

        return Datatables::of($datas)
            ->addColumn('action', function ($data) {
                return '<label class="new-control new-checkbox checkbox-outline-primary  m-auto">
                <input type="checkbox" class="new-control-input child-chk checkId"
                onclick="updateCheck(' . $data->id_master_additional_order_unit . ')"
                id="' . $data->id_master_additional_order_unit . '"
                value="' . $data->id_master_additional_order_unit . '">
                <span class="new-control-indicator"></span><span style="visibility:hidden">c</span>
                </label>
                ';
            })
            ->addColumn('details_url', function ($data) {
                return url('datatable/detailAdditionalOrderJson/' . $data->id_master_additional_order_unit);
            })
            ->make(true);
    }

    public function additionalOrderJsonATPMAllocation(
        MasterAdditionalOrderRepository $masterAdditionalOrderRepository,
        CacheRepository $cacheRepository
    ) {
        $idUser = session()->get('user')['id_user'];
        $idDealer = session()->get('user')['id_dealer'];
        $cache_name = 'datatable-additionalOrderJsonATPMAllocation-idUser-' . $idUser . '-idDealer-' . $idDealer;
        $datas = Cache::remember($cache_name, 10,
            function () use ($masterAdditionalOrderRepository, $cacheRepository, $idUser, $cache_name, $idDealer) {
                $cacheRepository->firstOrCreate(['cache_name' => $cache_name, 'id_user' => $idUser]);
                return $masterAdditionalOrderRepository->getATPMAllocation($idDealer);
            });

        //$datas = $masterAdditionalOrderRepository->getATPMAllocation();

        return Datatables::of($datas)
            ->addColumn('action', function ($data) {
                return '<label class="new-control new-checkbox checkbox-outline-primary  m-auto">
                <input type="checkbox" class="new-control-input child-chk checkId"
                onclick="updateCheck(' . $data->id_master_additional_order_unit . ')"
                id="' . $data->id_master_additional_order_unit . '"
                value="' . $data->id_master_additional_order_unit . '">
                <span class="new-control-indicator"></span><span style="visibility:hidden">c</span>
                </label>
                ';
            })
            ->addColumn('details_url', function ($data) {
                return url('datatable/detailAdditionalOrderJson/' . $data->id_master_additional_order_unit);
            })
            ->make(true);
    }

    public function additionalOrderJsonCanceled(
        $idCancel = null,
        MasterAdditionalOrderRepository $masterAdditionalOrderRepository,
        CacheRepository $cacheRepository
    ) {
        $idUser = session()->get('user')['id_user'];
        $idDealer = session()->get('user')['id_dealer'];
        $cache_name = 'datatable-additionalOrderJsonCanceled-idUser-' . $idUser . '-idDealer-' . $idDealer . '-idCancel-' . $idCancel;
        $datas = Cache::remember($cache_name, 10,
            function () use ($masterAdditionalOrderRepository, $cacheRepository, $idUser, $idDealer, $idCancel, $cache_name) {
                $cacheRepository->firstOrCreate(['cache_name' => $cache_name, 'id_user' => $idUser]);
                return $masterAdditionalOrderRepository->getCanceledAdditionalOrder($idDealer, $idCancel);
            });

        //$datas = $masterAdditionalOrderRepository->getCanceledAdditionalOrder($idDealer, $idCancel);

        return Datatables::of($datas)
            ->addColumn('action', function ($data) {
                return '<label class="new-control new-checkbox checkbox-outline-primary  m-auto">
                <input type="checkbox" class="new-control-input child-chk checkId"
                onclick="updateCheck(' . $data->id_master_additional_order_unit . ')"
                id="' . $data->id_master_additional_order_unit . '"
                value="' . $data->id_master_additional_order_unit . '">
                <span class="new-control-indicator"></span><span style="visibility:hidden">c</span>
                </label>
                ';
            })
            ->addColumn('details_url', function ($data) {
                return url('datatable/detailAdditionalOrderJson/' . $data->id_master_additional_order_unit);
            })
            ->make(true);
    }

    public function additionalOrderJsonReport(
        Request $request,
        MasterAdditionalOrderRepository $masterAdditionalOrderRepository,
        CacheRepository $cacheRepository
    ) {
        $idUser = session()->get('user')['id_user'];
        $idDealer = session()->get('user')['id_dealer'];
        $dateStart = $request->get('dateStart');
        $dateEnd = $request->get('dateEnd');
        $statusOrder = $request->get('statusOrder');
        $kdDealer = $request->get('kdDealer');
        $cache_name = 'datatable-additionalOrderJsonReport-idUser-' . $idUser . '-idDealer-' . $idDealer . '-dateStart-' . $dateStart . '-dateEnd-' . $dateEnd . '-statusOrder-' . $statusOrder . '-kdDealer-' . $kdDealer;
        $datas = Cache::remember($cache_name, 10,
            function () use ($masterAdditionalOrderRepository, $cacheRepository, $idUser, $idDealer, $kdDealer, $cache_name, $dateStart, $dateEnd, $statusOrder) {
                $cacheRepository->firstOrCreate(['cache_name' => $cache_name, 'id_user' => $idUser]);
                return $masterAdditionalOrderRepository->getReportAdditionalOrder($idDealer, $kdDealer, $dateStart, $dateEnd, $statusOrder);
            });

        //$datas = $masterAdditionalOrderRepository->getCanceledAdditionalOrder($idDealer, $idCancel);

        return Datatables::of($datas)
            ->addColumn('status_progress', function ($data) {
                $statusProgress = $this->checkStatusProgress($data);

                return $statusProgress;
            })
            ->addColumn('details_url', function ($data) {
                return url('datatable/detailAdditionalOrderJson/' . $data->id_master_additional_order_unit);
            })
            ->make(true);
    }

    public function detailAdditionalOrderJson(
        $id,
        DetailAdditionalOrderRepository $detailAdditionalOrderRepository,
        CacheRepository $cacheRepository
    ) {
        $cache_name = 'datatable-detail-additionalOrderJson-id-' . $id;
        $data = Cache::remember($cache_name, 10,
            function () use ($detailAdditionalOrderRepository, $cacheRepository, $id, $cache_name) {
                $cacheRepository->firstOrCreate(['cache_name' => $cache_name, 'id_user' => session()->get('user')['id_user']]);
                return $detailAdditionalOrderRepository->getByIdMaster($id);
        });

        //$data = $detailAdditionalOrderRepository->getByIdMaster($id);

        return Datatables::of($data)->make(true);
    }

    public function getById(
        $id,
        DetailAdditionalOrderRepository $detailAdditionalOrderRepository,
        CacheRepository $cacheRepository
    ) {
        $cache_name = 'additionalOrder-datatable-getById-id-' . $id;
        $data = Cache::remember($cache_name, 10,
            function () use ($detailAdditionalOrderRepository, $cacheRepository, $id, $cache_name) {
                $cacheRepository->firstOrCreate(['cache_name' => $cache_name, 'id_user' => session()->get('user')['id_user']]);
                return $detailAdditionalOrderRepository->getByIdMaster($id);
        });

        return $data;
    }

    private function checkStatusProgress($data)
    {
        $flag_send_approval_dealer = $data->flag_send_approval_dealer;
        $flag_approval_dealer = $data->flag_approval_dealer;
        $flag_submit_to_atpm = $data->flag_submit_to_atpm;
        $flag_allocation = $data->flag_allocation;
        $flag_am_confirmation = $data->flag_am_confirmation;

        if ($flag_send_approval_dealer == '0' && $flag_approval_dealer == '0'
            && $flag_submit_to_atpm == '0' && $flag_am_confirmation == '0' && $flag_allocation == '0') {
            $statusProgress = 'Draft';
        } else if ($flag_send_approval_dealer == '1' && $flag_approval_dealer == '0'
            && $flag_submit_to_atpm == '0' && $flag_am_confirmation == '0' && $flag_allocation == '0') {
            $statusProgress = 'Waiting Approval';
        } else if ($flag_send_approval_dealer == '1' && $flag_approval_dealer == '1'
            && $flag_submit_to_atpm == '0' && $flag_am_confirmation == '0' && $flag_allocation == '0') {
            $statusProgress = 'Approved';
        } else if ($flag_send_approval_dealer == '1' && $flag_approval_dealer == '1'
            && $flag_submit_to_atpm == '1' && $flag_am_confirmation == '0' && $flag_allocation == '0') {
            $statusProgress = 'Submitted';
        } else if ($flag_send_approval_dealer == '1' && $flag_approval_dealer == '1'
            && $flag_submit_to_atpm == '1' && $flag_am_confirmation == '1' && $flag_allocation == '0') {
            $statusProgress = 'AM Confirmed';
        } else if ($flag_send_approval_dealer == '1' && $flag_approval_dealer == '1'
            && $flag_submit_to_atpm == '1' && $flag_am_confirmation == '1' && $flag_allocation == '1') {
            $statusProgress = 'Allocated';
        } elseif ($flag_send_approval_dealer == '2' && $flag_approval_dealer == '0'
            && $flag_submit_to_atpm == '0'  && $flag_am_confirmation == '0' && $flag_allocation == '0') {
            $statusProgress = 'Revised';
        } else {
            $statusProgress = '-';
        }

        return $statusProgress;
    }
}
