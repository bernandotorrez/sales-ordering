<?php

namespace App\Http\Controllers\SalesOrdering;

use App\Http\Controllers\Controller;
use App\Repository\Eloquent\CacheRepository;
use App\Repository\Eloquent\DetailColourFixOrderRepository;
use App\Repository\Eloquent\DetailFixOrderRepository;
use App\Repository\Eloquent\MasterFixOrderRepository;
use App\Repository\Eloquent\RangeMonthFixOrderRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Yajra\DataTables\DataTables;

class FixOrderDatatableController extends Controller
{
    // In Dealer Admin
    public function FixOrderJson(
        Request $request,
        MasterFixOrderRepository $masterFixOrderRepository,
        RangeMonthFixOrderRepository $rangeMonthFixOrderRepository,
        CacheRepository $cacheRepository
    ) {
        $idUser = session()->get('user')['id_user'];
        $idDealer = session()->get('user')['id_dealer'];
        $month = $request->get('month');
        $monthIdTo = $rangeMonthFixOrderRepository->getMonthIdToByIdMonth(date('m'));
        $cache_name = 'datatable-fixOrderJson-idUser-' . $idUser . '-idDealer-' . $idDealer . '-month-' . $month;
        $datas = Cache::remember($cache_name, 10,
            function () use ($masterFixOrderRepository, $cacheRepository, $idUser, $idDealer, $cache_name, $month, $monthIdTo) {
                $cacheRepository->firstOrCreate(['cache_name' => $cache_name, 'id_user' => $idUser]);
                return $masterFixOrderRepository->getByIdDealerAndMonth($idDealer, $month ? $month : $monthIdTo->month_id_to);
            });

        //$datas = $masterFixOrderRepository->getByIdDealerAndMonth($idDealer, $month ? $month : $monthIdTo->month_id_to);

        return Datatables::of($datas)
            ->addColumn('action', function ($data) {
                if ($data->flag_send_approval_dealer == '1') {
                    return '<label class="new-control new-checkbox checkbox-outline-primary  m-auto">
                <input type="checkbox" checked class="new-control-input child-chk select-customers-info" id="customer-all-info" disabled>
                <span class="new-control-indicator"></span><span style="visibility:hidden">c</span>
                </label>';
                } else {
                    return '<label class="new-control new-checkbox checkbox-outline-primary  m-auto">
                <input type="checkbox" class="new-control-input child-chk checkId"
                onclick="updateCheck(' . $data->id_master_fix_order_unit . ')"
                id="' . $data->id_master_fix_order_unit . '"
                value="' . $data->id_master_fix_order_unit . '">
                <span class="new-control-indicator"></span><span style="visibility:hidden">c</span>
                </label>
                ';
                }

            })
            ->addColumn('status_progress', function ($data) {
                $statusProgress = $this->checkStatusProgress($data);

                return $statusProgress;
            })
            ->addColumn('details_url', function ($data) {
                return url('datatable/detailFixOrderJson/' . $data->id_master_fix_order_unit);
            })
            ->make(true);
    }

    // In Dealer Principle
    public function FixOrderJsonApprovalBM(
        Request $request,
        MasterFixOrderRepository $masterFixOrderRepository,
        RangeMonthFixOrderRepository $rangeMonthFixOrderRepository,
        CacheRepository $cacheRepository
    )
    {
        $idUser = session()->get('user')['id_user'];
        $idDealer = session()->get('user')['id_dealer'];
        $month = $request->get('month');
        $monthIdTo = $rangeMonthFixOrderRepository->getMonthIdToByIdMonth(date('m'));
        $cache_name = 'datatable-FixOrderJsonApprovalBM-idUser-' . $idUser . '-idDealer-' . $idDealer . '-month-' . $month;
        $datas = Cache::remember($cache_name, 10,
            function () use ($masterFixOrderRepository, $cacheRepository, $idUser, $idDealer, $cache_name, $month, $monthIdTo) {
                $cacheRepository->firstOrCreate(['cache_name' => $cache_name, 'id_user' => $idUser]);
                return $masterFixOrderRepository->getByIdDealerAndMonthApprovalBM($idDealer, $month ? $month : $monthIdTo->month_id_to);
            });

        //$datas = $masterFixOrderRepository->getByIdDealerAndMonthApprovalBM($idDealer, $month ? $month : $monthIdTo->month_id_to);

        return Datatables::of($datas)
            ->addColumn('action', function ($data) {
                if ($data->flag_approval_dealer == '1' && $data->flag_submit_to_atpm == '1') {
                    return '<label class="new-control new-checkbox checkbox-outline-primary  m-auto">
                <input type="checkbox" checked class="new-control-input child-chk select-customers-info" id="customer-all-info" disabled>
                <span class="new-control-indicator"></span><span style="visibility:hidden">c</span>
                </label>';
                } else {
                    return '<label class="new-control new-checkbox checkbox-outline-primary  m-auto">
                <input type="checkbox" class="new-control-input child-chk checkId"
                onclick="updateCheck(' . $data->id_master_fix_order_unit . ', ' . $data->flag_approval_dealer . ', ' . $data->flag_planning . ')"
                id="' . $data->id_master_fix_order_unit . '"
                data-approved="' . $data->flag_approval_dealer . '"
                data-planning="' . $data->flag_planning . '"
                value="' . $data->id_master_fix_order_unit . '">
                <span class="new-control-indicator"></span><span style="visibility:hidden">c</span>
                </label>
                ';
                }
            })
            ->addColumn('status_progress', function ($data) {
                $statusProgress = $this->checkStatusProgress($data);

                return $statusProgress;
            })
            ->addColumn('details_url', function ($data) {
                return url('datatable/detailFixOrderJson/' . $data->id_master_fix_order_unit);
            })
            ->make(true);
    }

    // In ATPM
    public function FixOrderJsonConfirmationAtpm(
        Request $request,
        MasterFixOrderRepository $masterFixOrderRepository,
        RangeMonthFixOrderRepository $rangeMonthFixOrderRepository,
        CacheRepository $cacheRepository
    ) {
        $idUser = session()->get('user')['id_user'];
        $idDealer = session()->get('user')['id_dealer'];
        $dealers = session()->get('area_manager');
        $month = $request->get('month');
        $monthIdTo = $rangeMonthFixOrderRepository->getMonthIdToByIdMonth(date('m'));
        $cache_name = 'datatable-FixOrderJsonConfirmationAtpm-idUser-' . $idUser . '-idDealer-' . $idDealer . '-month-' . $month;
        $datas = Cache::remember($cache_name, 10,
            function () use ($masterFixOrderRepository, $cacheRepository, $idUser, $idDealer, $cache_name, $month, $monthIdTo, $dealers) {
                $cacheRepository->firstOrCreate(['cache_name' => $cache_name, 'id_user' => $idUser]);
                return $masterFixOrderRepository->getByIdDealerAndMonthConfirmationAtpm($idDealer, $month ? $month : $monthIdTo->month_id_to, $dealers);
            });

        //$datas = $masterFixOrderRepository->getByIdDealerAndMonthAllocationAtpm($idDealer, $month ? $month : $monthIdTo->month_id_to);

        return Datatables::of($datas)
            ->addColumn('action', function ($data) {
                if ($data->flag_am_confirmation == '1') {
                    return '<label class="new-control new-checkbox checkbox-outline-primary  m-auto">
                <input type="checkbox" checked class="new-control-input child-chk select-customers-info" id="customer-all-info" disabled>
                <span class="new-control-indicator"></span><span style="visibility:hidden">c</span>
                </label>';
                } else {
                    return '<label class="new-control new-checkbox checkbox-outline-primary  m-auto">
                <input type="checkbox" class="new-control-input child-chk checkId"
                onclick="updateCheck(' . $data->id_master_fix_order_unit . ', ' . $data->flag_approval_dealer . ')"
                id="' . $data->id_master_fix_order_unit . '"
                data-approved="' . $data->flag_approval_dealer . '"
                value="' . $data->id_master_fix_order_unit . '">
                <span class="new-control-indicator"></span><span style="visibility:hidden">c</span>
                </label>
                ';
                }
            })
            ->addColumn('status_progress', function ($data) {
                $statusProgress = $this->checkStatusProgress($data);

                return $statusProgress;
            })
            ->addColumn('details_url', function ($data) {
                return url('datatable/detailFixOrderJson/' . $data->id_master_fix_order_unit);
            })
            ->make(true);
    }

    public function FixOrderJsonAllocationAtpm(
        Request $request,
        MasterFixOrderRepository $masterFixOrderRepository,
        RangeMonthFixOrderRepository $rangeMonthFixOrderRepository,
        CacheRepository $cacheRepository
    ) {
        $idUser = session()->get('user')['id_user'];
        $idDealer = session()->get('user')['id_dealer'];
        $dealers = session()->get('area_manager');
        $month = $request->get('month');
        $monthIdTo = $rangeMonthFixOrderRepository->getMonthIdToByIdMonth(date('m'));
        $cache_name = 'datatable-FixOrderJsonAllocationAtpm-idUser-' . $idUser . '-idDealer-' . $idDealer . '-month-' . $month;
        $datas = Cache::remember($cache_name, 10,
            function () use ($masterFixOrderRepository, $cacheRepository, $idUser, $idDealer, $cache_name, $month, $monthIdTo, $dealers) {
                $cacheRepository->firstOrCreate(['cache_name' => $cache_name, 'id_user' => $idUser]);
                return $masterFixOrderRepository->getByIdDealerAndMonthAllocationAtpm($idDealer, $month ? $month : $monthIdTo->month_id_to, $dealers);
            });

        //$datas = $masterFixOrderRepository->getByIdDealerAndMonthAllocationAtpm($idDealer, $month ? $month : $monthIdTo->month_id_to);

        return Datatables::of($datas)
            ->addColumn('action', function ($data) {
                if ($data->flag_allocation == '1') {
                    return '<label class="new-control new-checkbox checkbox-outline-primary  m-auto">
                <input type="checkbox" checked class="new-control-input child-chk select-customers-info" id="customer-all-info" disabled>
                <span class="new-control-indicator"></span><span style="visibility:hidden">c</span>
                </label>';
                } else {
                    return '<label class="new-control new-checkbox checkbox-outline-primary  m-auto">
                <input type="checkbox" class="new-control-input child-chk checkId"
                onclick="updateCheck(' . $data->id_master_fix_order_unit . ', ' . $data->flag_approval_dealer . ')"
                id="' . $data->id_master_fix_order_unit . '"
                data-approved="' . $data->flag_approval_dealer . '"
                value="' . $data->id_master_fix_order_unit . '">
                <span class="new-control-indicator"></span><span style="visibility:hidden">c</span>
                </label>
                ';
                }
            })
            ->addColumn('status_progress', function ($data) {
                $statusProgress = $this->checkStatusProgress($data);

                return $statusProgress;
            })
            ->addColumn('details_url', function ($data) {
                return url('datatable/detailFixOrderJson/' . $data->id_master_fix_order_unit);
            })
            ->make(true);
    }

    public function fixOrderJsonReport(
        Request $request,
        MasterFixOrderRepository $masterFixOrderRepository,
        CacheRepository $cacheRepository
    ) {

        $idUser = session()->get('user')['id_user'];
        $idDealer = session()->get('user')['id_dealer'];
        $dateStart = $request->get('dateStart');
        $dateEnd = $request->get('dateEnd');
        $statusOrder = $request->get('statusOrder');
        $kdDealer = $request->get('kdDealer');
        $cache_name = 'datatable-fixOrderJsonReport-idUser-' . $idUser . '-idDealer-' . $idDealer . '-dateStart-' . $dateStart . '-dateEnd-' . $dateEnd . '-statusOrder-' . $statusOrder . '-kdDealer-' . $kdDealer;
        $datas = Cache::remember($cache_name, 10,
            function () use ($masterFixOrderRepository, $cacheRepository, $idUser, $idDealer, $kdDealer, $cache_name, $dateStart, $dateEnd, $statusOrder) {
                $cacheRepository->firstOrCreate(['cache_name' => $cache_name, 'id_user' => $idUser]);
                return $masterFixOrderRepository->getReportFixOrder($idDealer, $kdDealer, $dateStart, $dateEnd, $statusOrder);
            });

        //$datas = $masterAdditionalOrderRepository->getCanceledAdditionalOrder($idDealer, $idCancel);

        return Datatables::of($datas)
            ->addColumn('status_progress', function ($data) {
                $statusProgress = $this->checkStatusProgress($data);

                return $statusProgress;
            })
            ->addColumn('details_url', function ($data) {
                return url('datatable/detailFixOrderJson/' . $data->id_master_fix_order_unit);
            })
            ->make(true);
    }

    public function detailFixOrderJson(
        $id,
        DetailFixOrderRepository $detailFixOrderRepository,
        CacheRepository $cacheRepository
    ) {
        $cache_name = 'datatable-detail-fixOrderJson-id-' . $id;
        $data = Cache::remember($cache_name, 10, function () use ($detailFixOrderRepository, $cacheRepository, $id, $cache_name) {
            $cacheRepository->firstOrCreate(['cache_name' => $cache_name, 'id_user' => session()->get('user')['id_user']]);
            return $detailFixOrderRepository->getByIdMaster($id);
        });

        //$data = $detailFixOrderRepository->getByIdMaster($id);

        return Datatables::of($data)
            ->addColumn('details_url', function ($data) {
                return url('datatable/subDetailFixOrderJson/' . $data->id_detail_fix_order_unit);
            })->make(true);
    }

    public function subDetailFixOrderJson(
        $id,
        DetailColourFixOrderRepository $detailColourFixOrderRepository,
        CacheRepository $cacheRepository
    ) {
        $cache_name = 'datatable-sub-detail-fixOrderJson-id-' . $id;
        $data = Cache::remember($cache_name, 10, function () use ($detailColourFixOrderRepository, $cacheRepository, $id, $cache_name) {
            $cacheRepository->firstOrCreate(['cache_name' => $cache_name, 'id_user' => session()->get('user')['id_user']]);
            return $detailColourFixOrderRepository->getByIdDetail($id);
        });

        //$data = $detailColourFixOrderRepository->getByIdDetail($id);

        return Datatables::of($data)->make(true);
    }

    public function getByIdForAllocation(
        $id,
        DetailFixOrderRepository $detailFixOrderRepository,
        CacheRepository $cacheRepository
    ) {
        $cache_name = 'fixOrder-datatable-getByIdForAllocation-id-' . $id;
        $data = Cache::remember($cache_name, 10,
            function () use ($detailFixOrderRepository, $cacheRepository, $id, $cache_name) {
                $cacheRepository->firstOrCreate(['cache_name' => $cache_name, 'id_user' => session()->get('user')['id_user']]);
                return $detailFixOrderRepository->getByIdForAllocation($id);
        });

        return $data;
    }

    private function checkStatusProgress($data)
    {
        $flag_send_approval_dealer = $data->flag_send_approval_dealer;
        $flag_approval_dealer = $data->flag_approval_dealer;
        $flag_submit_to_atpm = $data->flag_submit_to_atpm;
        $flag_allocation = $data->flag_allocation;
        $flag_planning = $data->flag_planning;
        $flag_am_confirmation = $data->flag_am_confirmation;

        if($flag_planning == '1') {
            $statusProgress = 'Planning';
        } else if ($flag_send_approval_dealer == '0' && $flag_approval_dealer == '0'
            && $flag_submit_to_atpm == '0' && $flag_am_confirmation == '0' && $flag_allocation == '0') {
            $statusProgress = 'Draft';
        } else if ($flag_send_approval_dealer == '1' && $flag_approval_dealer == '0'
            && $flag_submit_to_atpm == '0' && $flag_am_confirmation == '0' && $flag_allocation == '0') {
            $statusProgress = 'Waiting Approval';
        } else if ($flag_send_approval_dealer == '1' && $flag_approval_dealer == '1'
            && $flag_submit_to_atpm == '0' && $flag_am_confirmation == '0' && $flag_allocation == '0') {
            $statusProgress = 'Approved';
        } else if ($flag_send_approval_dealer == '1' && $flag_approval_dealer == '1'
            && $flag_submit_to_atpm == '1' && $flag_am_confirmation == '0' && $flag_allocation == '0') {
            $statusProgress = 'Submitted';
        } else if ($flag_send_approval_dealer == '1' && $flag_approval_dealer == '1'
            && $flag_submit_to_atpm == '1' && $flag_am_confirmation == '1' && $flag_allocation == '0') {
            $statusProgress = 'AM Confirmed';
        } else if ($flag_send_approval_dealer == '1' && $flag_approval_dealer == '1'
            && $flag_submit_to_atpm == '1' && $flag_am_confirmation == '1' && $flag_allocation == '1') {
            $statusProgress = 'Allocated';
        } elseif ($flag_send_approval_dealer == '2' && $flag_approval_dealer == '0'
            && $flag_submit_to_atpm == '0'  && $flag_am_confirmation == '0' && $flag_allocation == '0') {
            $statusProgress = 'Revised';
        } else {
            $statusProgress = '-';
        }

        return $statusProgress;
    }
}
