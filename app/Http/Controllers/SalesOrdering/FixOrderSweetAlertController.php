<?php

namespace App\Http\Controllers\SalesOrdering;

use App\Http\Controllers\Controller;
use App\Mail\Allocated;
use App\Mail\AMConfirmation;
use App\Mail\Approved;
use App\Mail\Revised;
use App\Mail\Submitted;
use App\Mail\WaitingApproval;
use App\Models\MasterMonthOrder;
use App\Repository\Api\ApiDealerUserRepository;
use App\Repository\Eloquent\DetailColourFixOrderRepository;
use App\Repository\Eloquent\EmailAtpmRepository;
use App\Repository\Eloquent\KodeTahunRepository;
use App\Repository\Eloquent\MasterFixOrderRepository;
use App\Repository\Eloquent\MasterMonthOrderRepository;
use App\Traits\WithDeleteCache;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class FixOrderSweetAlertController extends Controller
{
    use WithDeleteCache;

    public function sendToApproval(
        Request $request,
        MasterFixOrderRepository $masterFixOrderRepository,
        ApiDealerUserRepository $apiDealerUserRepository,
        MasterMonthOrderRepository $masterMonthOrderRepository
    ) {
        $id = $request->post('id');
        $month = $request->post('id_month');
        $periode = $this->getPeriode($id, $masterFixOrderRepository, $masterMonthOrderRepository);

        $data = array(
            'flag_send_approval_dealer' => '1',
            'date_send_approval' => Carbon::now()
        );

        $update = DB::transaction(function () use ($masterFixOrderRepository, $id, $data) {
            return $masterFixOrderRepository->massUpdate($id, $data);
        });

        $idUser = session()->get('user')['id_user'];
        $idDealer = session()->get('user')['id_dealer'];

        if ($update) {
            $callback = array(
                'status' => 'success',
            );

            $this->deleteCaches('datatable-fixOrderJson-idUser-' . $idUser . '-idDealer-' . $idDealer . '-month-' . $month);
            $this->deleteCache();

            Mail::to($this->getEmailToBM($id, $masterFixOrderRepository, $apiDealerUserRepository))->send(new WaitingApproval($id, 'F', $periode));
        } else {
            $callback = array(
                'status' => 'fail',
            );
        }

        return $callback;
    }

    public function approvalBM(
        Request $request,
        MasterFixOrderRepository $masterFixOrderRepository,
        ApiDealerUserRepository $apiDealerUserRepository,
        MasterMonthOrderRepository $masterMonthOrderRepository
    ) {
        $id = $request->post('id');
        $month = $request->post('id_month');
        $periode = $this->getPeriode($id, $masterFixOrderRepository, $masterMonthOrderRepository);

        $data = array(
            'flag_approval_dealer' => '1',
            'date_approval' => Carbon::now()
        );

        $update = DB::transaction(function () use ($masterFixOrderRepository, $id, $data) {
            return $masterFixOrderRepository->massUpdate($id, $data);
        });

        $idUser = session()->get('user')['id_user'];
        $idDealer = session()->get('user')['id_dealer'];

        if ($update) {
            $callback = array(
                'status' => 'success',
            );

            $this->deleteCaches('datatable-FixOrderJsonApprovalBM-idUser-' . $idUser . '-idDealer-' . $idDealer . '-month-' . $month);
            $this->deleteCache();

            Mail::to($this->getEmailToBM($id, $masterFixOrderRepository, $apiDealerUserRepository))->send(new Approved($id, 'F', $periode));
        } else {
            $callback = array(
                'status' => 'fail',
            );
        }

        return $callback;
    }

    public function reviseBM(
        Request $request,
        MasterFixOrderRepository $masterFixOrderRepository,
        ApiDealerUserRepository $apiDealerUserRepository,
        MasterMonthOrderRepository $masterMonthOrderRepository
    ) {
        $id = $request->post('id');
        $month = $request->post('id_month');
        $remark_revise = $request->post('remark_revise');
        $periode = $this->getPeriode($id, $masterFixOrderRepository, $masterMonthOrderRepository);

        $data = array(
            'flag_send_approval_dealer' => '2',
            'flag_approval_dealer' => '0',
            'remark_revise' => $remark_revise,
            'date_revise' => Carbon::now()
        );

        $update = DB::transaction(function () use ($masterFixOrderRepository, $id, $data) {
            return $masterFixOrderRepository->massUpdate($id, $data);
        });

        $idUser = session()->get('user')['id_user'];
        $idDealer = session()->get('user')['id_dealer'];

        if ($update) {
            $callback = array(
                'status' => 'success',
            );

            $this->deleteCaches('datatable-FixOrderJsonApprovalBM-idUser-' . $idUser . '-idDealer-' . $idDealer . '-month-' . $month);
            $this->deleteCache();

            Mail::to($this->getEmailToBM($id, $masterFixOrderRepository, $apiDealerUserRepository))->send(new Revised($id, 'F', $periode));
        } else {
            $callback = array(
                'status' => 'fail',
            );
        }

        return $callback;
    }

    public function planningToAtpm(
        Request $request,
        MasterFixOrderRepository $masterFixOrderRepository
    ) {
        $id = $request->post('id');
        $month = $request->post('id_month');

        $data = array(
            'flag_planning' => '1',
            'date_planning' => Carbon::now()
        );

        $update = DB::transaction(function () use ($masterFixOrderRepository, $id, $data) {
            return $masterFixOrderRepository->massUpdate($id, $data);
        });

        $idUser = session()->get('user')['id_user'];
        $idDealer = session()->get('user')['id_dealer'];

        if ($update) {
            $callback = array(
                'status' => 'success',
            );

            $this->deleteCaches('datatable-FixOrderJsonApprovalBM-idUser-' . $idUser . '-idDealer-' . $idDealer . '-month-' . $month);
            $this->deleteCache();

            //Mail::to('Bernand.Hermawan@eurokars.co.id')->send(new SendEmailToDealerPrinciple);
        } else {
            $callback = array(
                'status' => 'fail',
            );
        }

        return $callback;
    }

    public function submitToAtpm(
        Request $request,
        MasterFixOrderRepository $masterFixOrderRepository,
        KodeTahunRepository $kodeTahunRepository,
        ApiDealerUserRepository $apiDealerUserRepository,
        EmailAtpmRepository $emailAtpmRepository,
        MasterMonthOrderRepository $masterMonthOrderRepository
    ) {
        $id = $request->post('id');
        $month = $request->post('id_month');
        $periode = $this->getPeriode($id, $masterFixOrderRepository, $masterMonthOrderRepository);

        $update = $masterFixOrderRepository->updateSubmitAtpm($id, $kodeTahunRepository);

        $idUser = session()->get('user')['id_user'];
        $idDealer = session()->get('user')['id_dealer'];

        if ($update) {
            $callback = array(
                'status' => 'success',
            );

            $this->deleteCaches('datatable-FixOrderJsonApprovalBM-idUser-' . $idUser . '-idDealer-' . $idDealer . '-month-' . $month);
            $this->deleteCache();

            Mail::to($this->getEmailToATPM($id, $masterFixOrderRepository, $apiDealerUserRepository, $emailAtpmRepository))->send(new Submitted($id, 'F', $periode));
        } else {
            $callback = array(
                'status' => 'fail',
            );
        }

        return $callback;
    }

    public function confirmAM(
        Request $request,
        MasterFixOrderRepository $masterFixOrderRepository,
        ApiDealerUserRepository $apiDealerUserRepository,
        EmailAtpmRepository $emailAtpmRepository,
        MasterMonthOrderRepository $masterMonthOrderRepository,
        DetailColourFixOrderRepository $detailColourFixOrderRepository
    ) {
        $id = $request->post('id');
        $month = $request->post('id_month');

        $data = array(
            'flag_am_confirmation' => '1',
            'date_am_confirmation' => Carbon::now(),
            'am_confirmation_by' => session()->get('user')['username'],
            'nama_am_confirmation_by' => session()->get('user')['nama_user']
        );

        $update = DB::transaction(function () use ($masterFixOrderRepository, $id, $data) {
            return $masterFixOrderRepository->massUpdate($id, $data);
        });

        $idUser = session()->get('user')['id_user'];
        $idDealer = session()->get('user')['id_dealer'];

        if ($update) {
            $callback = array(
                'status' => 'success',
            );

            $this->deleteCaches('datatable-FixOrderJsonConfirmationAtpm-idUser-' . $idUser . '-idDealer-' . $idDealer . '-month-' . $month);
            $this->deleteCache();

            Mail::to($this->getEmailToATPM($id, $masterFixOrderRepository, $apiDealerUserRepository, $emailAtpmRepository))->send(new AMConfirmation($id, 'F', ''));
        } else {
            $callback = array(
                'status' => 'fail',
            );
        }

        return $callback;
    }

    public function allocatedAtpm(
        Request $request,
        MasterFixOrderRepository $masterFixOrderRepository,
        ApiDealerUserRepository $apiDealerUserRepository,
        EmailAtpmRepository $emailAtpmRepository,
        MasterMonthOrderRepository $masterMonthOrderRepository,
        DetailColourFixOrderRepository $detailColourFixOrderRepository
    ) {

        // Update detail Colour
        $id = $request->post('id_master');
        $detail = $request->post('detail');
        $month = $request->post('id_month');

        $updateDetail = DB::transaction(function () use($detailColourFixOrderRepository, $detail) {
            return $detailColourFixOrderRepository->allocate($detail);
        });

        if($updateDetail) {
            $periode = $this->getPeriode($id, $masterFixOrderRepository, $masterMonthOrderRepository);

            $data = array(
                'flag_allocation' => '1',
                'date_allocation_atpm' => Carbon::now(),
                'allocated_by' => session()->get('user')['username'],
                'nama_allocated_by' => session()->get('user')['nama_user']
            );

            // Update Master
            $update = DB::transaction(function () use ($masterFixOrderRepository, $id, $data) {
                return $masterFixOrderRepository->update($id, $data);
            });
        } else {
            $update = false;
        }

        $idUser = session()->get('user')['id_user'];
        $idDealer = session()->get('user')['id_dealer'];

        if ($update) {
            $callback = array(
                'status' => 'success',
            );

            $this->deleteCaches('datatable-FixOrderJsonAllocationAtpm-idUser-' . $idUser . '-idDealer-' . $idDealer . '-month-' . $month);
            $this->deleteCache();

            Mail::to($this->getEmailToATPM($id, $masterFixOrderRepository, $apiDealerUserRepository, $emailAtpmRepository))->send(new Allocated($id, 'F', $periode));
        } else {
            $callback = array(
                'status' => 'fail',
            );
        }

        return $callback;
    }

    private function getPeriode($id, $masterFixOrderRepository, $masterMonthOrderRepository)
    {
        $year = date('Y');

        // Get Month Id from tbl_master_fix
        $dataMasterFix = $masterFixOrderRepository->getById($id);
        $monthIdTo = $dataMasterFix->id_month;

        $dataMasterMonth = $masterMonthOrderRepository->getById($monthIdTo);
        $monthName = Str::substr($dataMasterMonth->month, 0, 3);

        $periode = $monthName . ' ' . $year;

        return $periode;
    }

    private function getEmailToBM($id, $masterFixOrderRepository, $apiDealerUserRepository)
    {
        $email = array();

        // Get Email User Order in tbl_master_additional_order
        $dataMasterFix = $masterFixOrderRepository->getById($id);
        array_push($email, $dataMasterFix->email_user_order);

        // Get Email BM in WRS Api Dealer User
        $idDealer = session()->get('user')['id_dealer'];
        $cache_name = 'api-getEmailToBM-idDealer-' . $idDealer;
        $dataDealer = Cache::remember($cache_name, 30, function () use ($apiDealerUserRepository, $idDealer) {
            return $apiDealerUserRepository->getByIdDealer($idDealer);
        });

        foreach ($dataDealer['data'] as $dealer) {
            if ($dealer['fk_dealer_level'] == 'BM') {
                array_push($email, $dealer['email']);
            }
        }

        if (App::environment(['local', 'staging'])) {
            return [
                'Bernand.Hermawan@eurokars.co.id',
                'Dewi.Purnamasari@eurokars.co.id',
                'Brian.Yunanda@eurokars.co.id',
            ];
        } else {
            return $email;
        }

        // TODO: return $email;
        //

    }

    private function getEmailToATPM($id, $masterFixOrderRepository, $apiDealerUserRepository, $emailAtpmRepository)
    {
        $email = array();

        // Get Email User Order in tbl_master_additional_order
        $dataMasterFix = $masterFixOrderRepository->getById($id);
        array_push($email, $dataMasterFix->email_user_order);

        // Get Email BM in WRS Api Dealer User
        $idDealer = session()->get('user')['id_dealer'];
        $cache_name = 'api-getEmailToATPM-idDealer-' . $idDealer;
        $dataDealer = Cache::remember($cache_name, 30, function () use ($apiDealerUserRepository, $idDealer) {
            return $apiDealerUserRepository->getByIdDealer($idDealer);
        });

        foreach ($dataDealer['data'] as $dealer) {
            if ($dealer['fk_dealer_level'] == 'BM') {
                array_push($email, $dealer['email']);
            }
        }

        // Get Email ATPM in tbl_email_atpm
        $dataEmailAtpm = $emailAtpmRepository->allActive();

        foreach ($dataEmailAtpm as $emailAtpm) {
            array_push($email, $emailAtpm->email_atpm);
        }

        if (App::environment(['local', 'staging'])) {
            return [
                'Bernand.Hermawan@eurokars.co.id',
                'Dewi.Purnamasari@eurokars.co.id',
                'Brian.Yunanda@eurokars.co.id',
            ];
        } else {
            return $email;
        }

        // TODO: return $email;
        //

    }

    private function deleteCaches($cacheName)
    {
        $idUser = session()->get('user')['id_user'];
        Cache::forget($cacheName);
    }
}
