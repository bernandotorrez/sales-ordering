<?php

namespace App\Http\Controllers\SalesOrdering;

use App\Http\Controllers\Controller;
use App\Mail\Allocated;
use App\Mail\AMConfirmation;
use App\Mail\Approved;
use App\Mail\Canceled;
use App\Mail\Revised;
use App\Mail\Submitted;
use App\Mail\WaitingApproval;
use App\Repository\Api\ApiDealerUserRepository;
use App\Repository\Eloquent\DetailAdditionalOrderRepository;
use App\Repository\Eloquent\EmailAtpmRepository;
use App\Repository\Eloquent\KodeTahunRepository;
use App\Repository\Eloquent\MasterAdditionalOrderRepository;
use App\Traits\WithDeleteCache;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class AdditionalOrderSweetAlertController extends Controller
{
    use WithDeleteCache;

    public function sendToApproval(
        Request $request,
        MasterAdditionalOrderRepository $masterAdditionalOrderRepository,
        ApiDealerUserRepository $apiDealerUserRepository
    ) {
        $id = $request->post('id');

        $data = array(
            'flag_send_approval_dealer' => '1',
            'date_send_approval' => Carbon::now()
        );

        $update = DB::transaction(function () use ($masterAdditionalOrderRepository, $id, $data) {
            return $masterAdditionalOrderRepository->massUpdate($id, $data);
        });

        $idUser = session()->get('user')['id_user'];
        $idDealer = session()->get('user')['id_dealer'];

        if ($update) {
            $callback = array(
                'status' => 'success',
            );

            $this->deleteCaches('datatable-additionalOrderJsonDraft-idUser-' . $idUser . '-idDealer-' . $idDealer);
            $this->deleteCache();

            Mail::to($this->getEmailToBM($id, $masterAdditionalOrderRepository, $apiDealerUserRepository))->send(new WaitingApproval($id, 'A', ''));
        } else {
            $callback = array(
                'status' => 'fail',
            );
        }

        return $callback;
    }

    public function approvedBM(
        Request $request,
        MasterAdditionalOrderRepository $masterAdditionalOrderRepository,
        ApiDealerUserRepository $apiDealerUserRepository
    ) {
        $id = $request->post('id');

        $data = array(
            'flag_approval_dealer' => '1',
            'date_approval' => Carbon::now()
        );

        $update = DB::transaction(function () use ($masterAdditionalOrderRepository, $id, $data) {
            return $masterAdditionalOrderRepository->massUpdate($id, $data);
        });

        $idUser = session()->get('user')['id_user'];
        $idDealer = session()->get('user')['id_dealer'];

        if ($update) {
            $callback = array(
                'status' => 'success',
            );

            $this->deleteCaches('datatable-additionalOrderJsonWaitingApprovalDealerPrinciple-idUser-' . $idUser . '-idDealer-' . $idDealer);
            $this->deleteCache();
            Mail::to($this->getEmailToBM($id, $masterAdditionalOrderRepository, $apiDealerUserRepository))->send(new Approved($id, 'A', ''));
        } else {
            $callback = array(
                'status' => 'fail',
            );
        }

        return $callback;
    }

    public function submitToAtpm(
        Request $request,
        MasterAdditionalOrderRepository $masterAdditionalOrderRepository,
        KodeTahunRepository $kodeTahunRepository,
        ApiDealerUserRepository $apiDealerUserRepository,
        EmailAtpmRepository $emailAtpmRepository
    ) {
        $id = $request->post('id');

        $update = $masterAdditionalOrderRepository->updateSubmitAtpm($id, $kodeTahunRepository);

        $idUser = session()->get('user')['id_user'];
        $idDealer = session()->get('user')['id_dealer'];

        if ($update) {
            $callback = array(
                'status' => 'success',
            );

            $this->deleteCaches('datatable-additionalOrderJsonApprovalDealerPrinciple-idUser-' . $idUser . '-idDealer-' . $idDealer);
            $this->deleteCache();
            Mail::to($this->getEmailToATPM($id, $masterAdditionalOrderRepository, $apiDealerUserRepository, $emailAtpmRepository))->send(new Submitted($id, 'A', ''));
        } else {
            $callback = array(
                'status' => 'fail',
            );
        }

        return $callback;
    }

    public function reviseBMDealer(
        Request $request,
        MasterAdditionalOrderRepository $masterAdditionalOrderRepository,
        ApiDealerUserRepository $apiDealerUserRepository
    ) {
        $id = $request->post('id');
        $remark_revise = $request->post('remark_revise');

        $data = array(
            'flag_approval_dealer' => '0',
            'flag_send_approval_dealer' => '2',
            'remark_revise' => $remark_revise,
            'date_revise' => Carbon::now()
        );

        $update = DB::transaction(function () use ($masterAdditionalOrderRepository, $id, $data) {
            return $masterAdditionalOrderRepository->massUpdate($id, $data);
        });

        $idUser = session()->get('user')['id_user'];
        $idDealer = session()->get('user')['id_dealer'];

        if ($update) {
            $callback = array(
                'status' => 'success',
            );

            $this->deleteCaches('datatable-additionalOrderJsonApprovalDealerPrinciple-idUser-' . $idUser . '-idDealer-' . $idDealer);
            $this->deleteCache();

            Mail::to($this->getEmailToBM($id, $masterAdditionalOrderRepository, $apiDealerUserRepository))->send(new Revised($id, 'A', ''));
        } else {
            $callback = array(
                'status' => 'fail',
            );
        }

        return $callback;
    }

    public function cancelBMDealer(
        Request $request,
        MasterAdditionalOrderRepository $masterAdditionalOrderRepository,
        ApiDealerUserRepository $apiDealerUserRepository
    ) {
        $id = $request->post('id');
        $remark_cancel = $request->post('remark_cancel');

        $data = array(
            'status' => '0',
            'id_cancel_status' => '1',
            'remark_cancel' => $remark_cancel,
            'date_cancel' => Carbon::now(),
        );

        $update = DB::transaction(function () use ($masterAdditionalOrderRepository, $id, $data) {
            return $masterAdditionalOrderRepository->massUpdate($id, $data);
        });

        $idUser = session()->get('user')['id_user'];
        $idDealer = session()->get('user')['id_dealer'];

        if ($update) {
            $callback = array(
                'status' => 'success',
            );

            $this->deleteCaches('datatable-additionalOrderJsonApprovalDealerPrinciple-idUser-' . $idUser . '-idDealer-' . $idDealer);
            $this->deleteCache();

            Mail::to($this->getEmailToBM($id, $masterAdditionalOrderRepository, $apiDealerUserRepository))->send(new Canceled($id, 'A', 'BM'));
        } else {
            $callback = array(
                'status' => 'fail',
            );
        }

        return $callback;
    }

    public function submittedAtpm(
        Request $request,
        MasterAdditionalOrderRepository $masterAdditionalOrderRepository,
        ApiDealerUserRepository $apiDealerUserRepository,
        EmailAtpmRepository $emailAtpmRepository
    ) {
        $id = $request->post('id');

        $data = array(
            'flag_am_confirmation' => '1',
            'date_am_confirmation' => Carbon::now(),
            'am_confirmation_by' => session()->get('user')['username'],
            'nama_am_confirmation_by' => session()->get('user')['nama_user']
        );

        $update = DB::transaction(function () use ($masterAdditionalOrderRepository, $id, $data) {
            return $masterAdditionalOrderRepository->massUpdate($id, $data);
        });

        $idUser = session()->get('user')['id_user'];
        $idDealer = session()->get('user')['id_dealer'];

        if ($update) {
            $callback = array(
                'status' => 'success',
            );

            $this->deleteCaches('datatable-additionalOrderJsonSubmittedATPM-idUser-' . $idUser . '-idDealer-' . $idDealer);
            $this->deleteCache();
            Mail::to($this->getEmailToATPM($id, $masterAdditionalOrderRepository, $apiDealerUserRepository, $emailAtpmRepository))->send(new AMConfirmation($id, 'A', ''));
        } else {
            $callback = array(
                'status' => 'fail',
            );
        }

        return $callback;
    }

    public function reviseSubmittedAtpm(
        Request $request,
        MasterAdditionalOrderRepository $masterAdditionalOrderRepository,
        ApiDealerUserRepository $apiDealerUserRepository
    ) {
        $id = $request->post('id');
        $remark_revise = $request->post('remark_revise');

        $data = array(
            'flag_approval_dealer' => '0',
            'flag_submit_to_atpm' => '0',
            'flag_send_approval_dealer' => '2',
            'remark_revise' => $remark_revise,
            'date_revise' => Carbon::now()
        );

        $update = DB::transaction(function () use ($masterAdditionalOrderRepository, $id, $data) {
            return $masterAdditionalOrderRepository->massUpdate($id, $data);
        });

        $idUser = session()->get('user')['id_user'];
        $idDealer = session()->get('user')['id_dealer'];

        if ($update) {
            $callback = array(
                'status' => 'success',
            );

            $this->deleteCaches('datatable-additionalOrderJsonSubmittedATPM-idUser-' . $idUser . '-idDealer-' . $idDealer);
            $this->deleteCache();

            Mail::to($this->getEmailToBM($id, $masterAdditionalOrderRepository, $apiDealerUserRepository))->send(new Revised($id, 'A', ''));
        } else {
            $callback = array(
                'status' => 'fail',
            );
        }

        return $callback;
    }

    public function cancelSubmitATPM(
        Request $request,
        MasterAdditionalOrderRepository $masterAdditionalOrderRepository,
        ApiDealerUserRepository $apiDealerUserRepository
    ) {
        $id = $request->post('id');
        $remark_cancel = $request->post('remark_cancel');

        $data = array(
            'status' => '0',
            'id_cancel_status' => '2',
            'remark_cancel' => $remark_cancel,
            'date_cancel' => Carbon::now(),
        );

        $update = DB::transaction(function () use ($masterAdditionalOrderRepository, $id, $data) {
            return $masterAdditionalOrderRepository->massUpdate($id, $data);
        });

        $idUser = session()->get('user')['id_user'];
        $idDealer = session()->get('user')['id_dealer'];

        if ($update) {
            $callback = array(
                'status' => 'success',
            );

            $this->deleteCaches('datatable-additionalOrderJsonSubmittedATPM-idUser-' . $idUser . '-idDealer-' . $idDealer);
            $this->deleteCache();

            Mail::to($this->getEmailToBM($id, $masterAdditionalOrderRepository, $apiDealerUserRepository))->send(new Canceled($id, 'A', 'ATPM'));
        } else {
            $callback = array(
                'status' => 'fail',
            );
        }

        return $callback;
    }

    public function AMConfirmation(
        Request $request,
        MasterAdditionalOrderRepository $masterAdditionalOrderRepository,
        ApiDealerUserRepository $apiDealerUserRepository,
        EmailAtpmRepository $emailAtpmRepository,
        DetailAdditionalOrderRepository $detailAdditionalOrderRepository
    ) {
        $id = $request->post('id_master');
        $detail = $request->post('detail');

        $updateDetail = DB::transaction(function () use($detailAdditionalOrderRepository, $detail) {
            return $detailAdditionalOrderRepository->allocate($detail);
        });

        if($updateDetail) {
            $data = array(
                'flag_allocation' => '1',
                'date_allocation_atpm' => Carbon::now(),
                'allocated_by' => session()->get('user')['username'],
                'nama_allocated_by' => session()->get('user')['nama_user']
            );

            $update = DB::transaction(function () use ($masterAdditionalOrderRepository, $id, $data) {
                return $masterAdditionalOrderRepository->update($id, $data);
            });
        } else {
            $update = false;
        }

        $idUser = session()->get('user')['id_user'];
        $idDealer = session()->get('user')['id_dealer'];

        if ($update) {
            $callback = array(
                'status' => 'success',
            );

            $this->deleteCaches('datatable-additionalOrderJsonAMConfirmation-idUser-' . $idUser . '-idDealer-' . $idDealer);
            $this->deleteCache();
            Mail::to($this->getEmailToATPM($id, $masterAdditionalOrderRepository, $apiDealerUserRepository, $emailAtpmRepository))->send(new Allocated($id, 'A', ''));
        } else {
            $callback = array(
                'status' => 'fail',
            );
        }

        return $callback;
    }

    public function reviseAMConfirmation(
        Request $request,
        MasterAdditionalOrderRepository $masterAdditionalOrderRepository,
        ApiDealerUserRepository $apiDealerUserRepository
    ) {
        $id = $request->post('id');
        $remark_revise = $request->post('remark_revise');

        $data = array(
            'flag_approval_dealer' => '0',
            'flag_am_confirmation' => '0',
            'flag_submit_to_atpm' => '0',
            'flag_send_approval_dealer' => '2',
            'remark_revise' => $remark_revise,
            'date_revise' => Carbon::now()
        );

        $update = DB::transaction(function () use ($masterAdditionalOrderRepository, $id, $data) {
            return $masterAdditionalOrderRepository->massUpdate($id, $data);
        });

        $idUser = session()->get('user')['id_user'];
        $idDealer = session()->get('user')['id_dealer'];

        if ($update) {
            $callback = array(
                'status' => 'success',
            );

            $this->deleteCaches('datatable-additionalOrderJsonApprovalDealerPrinciple-idUser-' . $idUser . '-idDealer-' . $idDealer);
            $this->deleteCache();

            Mail::to($this->getEmailToBM($id, $masterAdditionalOrderRepository, $apiDealerUserRepository))->send(new Revised($id, 'A', ''));
        } else {
            $callback = array(
                'status' => 'fail',
            );
        }

        return $callback;
    }

    public function cancelAMConfirmation(
        Request $request,
        MasterAdditionalOrderRepository $masterAdditionalOrderRepository,
        ApiDealerUserRepository $apiDealerUserRepository
    ) {
        $id = $request->post('id');
        $remark_cancel = $request->post('remark_cancel');

        $data = array(
            'status' => '0',
            'id_cancel_status' => '3',
            'remark_cancel' => $remark_cancel,
            'date_cancel' => Carbon::now(),
        );

        $update = DB::transaction(function () use ($masterAdditionalOrderRepository, $id, $data) {
            return $masterAdditionalOrderRepository->massUpdate($id, $data);
        });

        $idUser = session()->get('user')['id_user'];
        $idDealer = session()->get('user')['id_dealer'];

        if ($update) {
            $callback = array(
                'status' => 'success',
            );

            $this->deleteCaches('datatable-additionalOrderJsonSubmittedATPM-idUser-' . $idUser . '-idDealer-' . $idDealer);
            $this->deleteCache();

            Mail::to($this->getEmailToBM($id, $masterAdditionalOrderRepository, $apiDealerUserRepository))->send(new Canceled($id, 'A', 'ATPM'));
        } else {
            $callback = array(
                'status' => 'fail',
            );
        }

        return $callback;
    }

    public function cancelAllocatedATPM(
        Request $request,
        MasterAdditionalOrderRepository $masterAdditionalOrderRepository,
        ApiDealerUserRepository $apiDealerUserRepository
    ) {
        $id = $request->post('id');
        $remark_cancel = $request->post('remark_cancel');

        $data = array(
            'status' => '0',
            'id_cancel_status' => '4',
            'remark_cancel' => $remark_cancel,
            'date_cancel' => Carbon::now(),
        );

        $update = DB::transaction(function () use ($masterAdditionalOrderRepository, $id, $data) {
            return $masterAdditionalOrderRepository->massUpdate($id, $data);
        });

        $idUser = session()->get('user')['id_user'];
        $idDealer = session()->get('user')['id_dealer'];

        if ($update) {
            $callback = array(
                'status' => 'success',
            );

            $this->deleteCaches('datatable-additionalOrderJsonATPMAllocation-idUser-' . $idUser . '-idDealer-' . $idDealer);
            $this->deleteCache();

            Mail::to($this->getEmailToBM($id, $masterAdditionalOrderRepository, $apiDealerUserRepository))->send(new Canceled($id, 'A', 'BM'));
        } else {
            $callback = array(
                'status' => 'fail',
            );
        }

        return $callback;
    }

    private function getEmailToBM($id, $masterAdditionalOrderRepository, $apiDealerUserRepository)
    {
        $email = array();

        // Get Email User Order in tbl_master_additional_order
        $dataMasterAdditional = $masterAdditionalOrderRepository->getById($id);
        array_push($email, $dataMasterAdditional->email_user_order);

        // Get Email BM in WRS Api Dealer User
        $idDealer = session()->get('user')['id_dealer'];
        $cache_name = 'api-getEmailToBM-idDealer-' . $idDealer;
        $dataDealer = Cache::remember($cache_name, 30, function () use ($apiDealerUserRepository, $idDealer) {
            return $apiDealerUserRepository->getByIdDealer($idDealer);
        });

        foreach ($dataDealer['data'] as $dealer) {
            if ($dealer['fk_dealer_level'] == 'BM') {
                array_push($email, $dealer['email']);
            }
        }

        if (App::environment(['local', 'staging'])) {
            return [
                'Bernand.Hermawan@eurokars.co.id',
                'Dewi.Purnamasari@eurokars.co.id',
                'Brian.Yunanda@eurokars.co.id',
            ];
        } else {
            return $email;
        }

        // TODO: return $email;
        //

    }

    private function getEmailToATPM($id, $masterAdditionalOrderRepository, $apiDealerUserRepository, $emailAtpmRepository)
    {
        $email = array();

        // Get Email User Order in tbl_master_additional_order
        $dataMasterAdditional = $masterAdditionalOrderRepository->getById($id);
        array_push($email, $dataMasterAdditional->email_user_order);

        // Get Email BM in WRS Api Dealer User
        $idDealer = session()->get('user')['id_dealer'];
        $cache_name = 'api-getEmailToATPM-idDealer-' . $idDealer;
        $dataDealer = Cache::remember($cache_name, 30, function () use ($apiDealerUserRepository, $idDealer) {
            return $apiDealerUserRepository->getByIdDealer($idDealer);
        });

        foreach ($dataDealer['data'] as $dealer) {
            if ($dealer['fk_dealer_level'] == 'BM') {
                array_push($email, $dealer['email']);
            }
        }

        // Get Email ATPM in tbl_email_atpm
        $dataEmailAtpm = $emailAtpmRepository->allActive();

        foreach ($dataEmailAtpm as $emailAtpm) {
            array_push($email, $emailAtpm->email_atpm);
        }

        if (App::environment(['local', 'staging'])) {
            return [
                'Bernand.Hermawan@eurokars.co.id',
                'Dewi.Purnamasari@eurokars.co.id',
                'Brian.Yunanda@eurokars.co.id',
            ];
        } else {
            return $email;
        }

        // TODO: return $email;
        //

    }

    private function deleteCaches($cacheName)
    {
        $idUser = session()->get('user')['id_user'];
        Cache::forget($cacheName);
    }
}
