<?php

namespace App\Http\Controllers;

class RouteController extends Controller
{
    public function index()
    {
        return redirect(route('login.index'));
    }

    public function logout()
    {
        session()->flush();

        return redirect(route('login.index'));
    }
}
