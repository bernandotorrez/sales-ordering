<?php

namespace App\Http\Livewire\SalesOrdering\ModelColourMazda;

use App\Repository\Api\ApiModelColorRepository;
use App\Repository\Eloquent\CacheRepository;
use App\Repository\Eloquent\ColourMazdaRepository;
use App\Repository\Eloquent\MazdaModelRepository;
use App\Repository\Eloquent\ModelColourMazdaRepository;
use App\Traits\WithDeleteCache;
use App\Traits\WithPaginationAttribute;
use App\Traits\WithSorting;
use Illuminate\Support\Facades\Cache;
use Livewire\Component;
use Livewire\WithPagination;

use function PHPSTORM_META\map;

class ModelColourMazdaIndex extends Component
{
    use WithPagination;
    use WithPaginationAttribute;
    use WithSorting;
    use WithDeleteCache;

    /**
     * Page Attributes
     */
    public string $pageTitle = "Model Colour Mazda";
    public bool $isEdit = false, $allChecked = false;
    public array $checked = [];
    protected string $viewName = 'view_model_colour_mazda';

    protected $queryString = [
        'search' => ['except' => ''],
        'page' => ['except' => 1]
    ];

    public $bind = [
        'id_model_colour_mazda',
        'id_model_colour',
        'id_model',
        'id_colour',
        'name_model',
        'name_colour',
        'name_colour_global',
        'flag_show_additional',
        'flag_show_fix'
    ];

    /**
     * Validation Attributes
     */
    protected $rules = [
        'bind.id_model_colour_mazda' => 'required',
        'bind.id_model_colour' => 'required|min:3|max:100',
        'bind.id_model' => 'required',
        'bind.id_colour' => 'required',
        'bind.flag_show_additional' => 'required|in:0,1',
        'bind.flag_show_fix' => 'required|in:0,1',
    ];

    protected $messages = [
        'bind.id_model_colour_mazda.required' => 'Please fill ID Model Colour Mazda field!',
        'bind.id_model_colour.required' => 'Please fill ID Model Colour field!',
        'bind.id_model_colour.min' => 'Please fill ID Model Colour field with Minimal :min Characters!',
        'bind.id_model_colour.max' => 'Please fill ID Model Colour field with Maximal :max Characters!',
        'bind.id_model.required' => 'Please fill Model field!',
        'bind.id_colour.required' => 'Please fill Colour field!',
        'bind.flag_show_additional.required' => 'Please fill Flag Show Additional field!',
        'bind.flag_show_additional.in' => 'Please fill Flag Show Additional field with 0 or 1',
        'bind.flag_show_fix.required' => 'Please fill Flag Show Fix field!',
        'bind.flag_show_fix.in' => 'Please fill Flag Flag Show Fix field with 0 or 1',
    ];

    public function mount()
    {
        $this->sortBy = 'id_model_colour_mazda';
        $this->fill(request()->only('search', 'page'));
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function resetForm()
    {
        $this->reset(['bind']);
    }

    public function render(
        ModelColourMazdaRepository $modelColourMazdaRepository,
        MazdaModelRepository $mazdaModelRepository,
        ColourMazdaRepository $colourMazdaRepository,
        CacheRepository $cacheRepository
    ) {
        $dataModelMazda = $mazdaModelRepository->getAvailableModel();

        $cache_name = 'modelcolourmazda-menu-index-page-'.$this->page.'-pageselected-'.$this->perPageSelected.'-search-'.$this->search;
        $cache_name .= '-sortby-'.$this->sortBy.'-sortdirection-'.$this->sortDirection.'-user-'.session()->get('user')['id_user'];

        $dataModelColourMazda = Cache::remember($cache_name, 15, function () use($modelColourMazdaRepository, $cacheRepository, $cache_name) {
            $cacheRepository->firstOrCreate(['cache_name' => $cache_name, 'id_user' => session()->get('user')['id_user']]);

            return $modelColourMazdaRepository->getAvailableModelColourPagination(
                $this->viewName,
                $this->search,
                $this->sortBy,
                $this->sortDirection,
                $this->perPageSelected
            );
        });

        $dataColourMazda = $colourMazdaRepository->allActive();

        return view('livewire.sales-ordering.model-colour-mazda.model-colour-mazda-index', [
            'dataModelMazda' => $dataModelMazda,
            'dataModelColourMazda' => $dataModelColourMazda,
            'dataColourMazda' => $dataColourMazda
        ])
        ->layout('layouts.app', ['title' => $this->pageTitle]);
    }

    public function allChecked(ModelColourMazdaRepository $modelColourMazdaRepository)
    {
        $datas = $modelColourMazdaRepository->getAvailableModelColourChecked(
            $this->viewName,
            $this->search,
            $this->sortBy,
            $this->sortDirection,
            $this->perPageSelected
        );

        $id = $modelColourMazdaRepository->getPrimaryKey();

        // Dari Unchecked ke Checked
        if($this->allChecked == true) {
            foreach($datas as $data) {
                if(!in_array($data->$id, $this->checked)) {
                    array_push($this->checked, (string) $data->$id);
                }
            }
        } else {
            // Checked ke Unchecked
            $this->checked = [];
        }
    }

    public function addForm()
    {
        $this->isEdit = false;
        $this->resetForm();
        $this->emit('openModal');
    }

    public function addProcess(ModelColourMazdaRepository $modelColourMazdaRepository)
    {
        $this->validate();

        $data = array(
            'id_model_colour' => $this->bind['id_model_colour'],
            'id_model' => $this->bind['id_model'],
            'id_colour' => $this->bind['id_colour'],
            'name_model' => $this->bind['name_model'],
            'name_colour' => $this->bind['name_model'],
            'name_colour_global' => $this->bind['name_colour_global'],
            'flag_show_additional' => $this->bind['flag_show_additional'],
            'flag_show_fix' => $this->bind['flag_show_additional']
        );

        $where = array(
            'id_model' => $this->bind['id_model'],
            'id_colour' => $this->bind['id_colour'],
        );

        $count = $modelColourMazdaRepository->findDuplicate($where);

        if($count <= 0) {
            $insert = $modelColourMazdaRepository->create($data);

            if($insert) {
                $this->resetForm();
                $this->deleteCache();
                $this->emit('closeModal');

                session()->flash('action_message', '<div class="alert alert-success">Insert Data Success!</div>');
            } else {
                session()->flash('action_message', '<div class="alert alert-danger">Insert Data Failed!</div>');
            }
        } else {
            session()->flash('message_duplicate', '<div class="alert alert-warning"><strong>'.$this->bind['name_model'].' & '.$this->bind['name_colour_global'].'</strong> Already Exists!</div>');
        }
    }

    public function editForm(
        ModelColourMazdaRepository $modelColourMazdaRepository,
        MazdaModelRepository $mazdaModelRepository,
        ColourMazdaRepository $colourMazdaRepository
    ) {
        $this->isEdit = true;

        $data = $modelColourMazdaRepository->getByID($this->checked[0]);
        $this->bind['id_model_colour_mazda'] = $data->id_model_colour_mazda;
        $this->bind['id_model_colour'] = $data->id_model_colour;
        $this->bind['id_model'] = $data->id_model;
        $this->bind['id_colour'] = $data->id_colour;
        $this->bind['flag_show_additional'] = $data->flag_show_additional;
        $this->bind['flag_show_fix'] = $data->flag_show_fix;

        $dataModel = $mazdaModelRepository->getByIdModel($data->id_model);
        $this->bind['name_model'] = $dataModel->name_model;

        $dataColour = $colourMazdaRepository->getByIdColour($data->id_colour);
        $this->bind['name_colour'] = $dataColour->name_colour;
        $this->bind['name_colour_global'] = $dataColour->name_colour_global;

        $this->emit('openModal');
    }

    public function syncFromWRS(
        ApiModelColorRepository $apiModelColorRepository,
        MazdaModelRepository $mazdaModelRepository,
        ModelColourMazdaRepository $modelColourMazdaRepository,
        ColourMazdaRepository $colourMazdaRepository,
        CacheRepository $cacheRepository
    ) {
        $dataTypeModel = $apiModelColorRepository->all();

        foreach($dataTypeModel['data'] as $modelColour) {
            $checkDuplicate = Cache::remember('syncFromWrs-ModelColour-kd_model_color-'.$modelColour['kd_model_color'], 30, function () use($modelColourMazdaRepository, $modelColour) {
                $where = array(
                    'id_model_colour' => $modelColour['kd_model_color']
                );
                return $modelColourMazdaRepository->findDuplicate($where);
            });

            if($checkDuplicate == '0') {
                $data = array(
                    'id_model_colour' => $modelColour['kd_model_color'],
                    'id_model' => $modelColour['fk_model'],
                    'id_colour' => $modelColour['fk_color'],
                    'flag_show_additional' => '1',
                    'flag_show_fix' => '1',
                    'status' => '1'
                );
                $insert = $modelColourMazdaRepository->create($data);
            } else {
                $data = array(
                    'id_model_colour' => $modelColour['kd_model_color'],
                    'id_model' => $modelColour['fk_model'],
                    'id_colour' => $modelColour['fk_color'],
                    'flag_show_additional' => '1',
                    'flag_show_fix' => '1',
                    'status' => '1'
                );
                $update = $modelColourMazdaRepository->update($data['id_model_colour'], $data);
            }
        }

        $this->deleteCache();
        $this->render($modelColourMazdaRepository, $mazdaModelRepository, $colourMazdaRepository, $cacheRepository);
    }
}
