<?php

namespace App\Http\Livewire\SalesOrdering\AMConfirmation;

use App\Repository\Eloquent\CancelStatusRepository;
use App\Traits\WithGoTo;
use Livewire\Component;

class AMConfirmationIndex extends Component
{
    use WithGoTo;
    public $pageTitle = 'Allocated';

    public function render(CancelStatusRepository $cancelStatusRepository)
    {
        $dataCancelStatus = $cancelStatusRepository->allActive();
        return view('livewire.sales-ordering.a-m-confirmation.a-m-confirmation-index',[
            'dataCancelStatus' => $dataCancelStatus
        ])->layout('layouts.app', ['title' => 'AM Confirmation']);
    }
}
