<?php

namespace App\Http\Livewire\SalesOrdering\FixOrder;

use App\Repository\Api\ApiColorRepository;
use App\Repository\Api\ApiModelColorRepository;
use App\Repository\Api\ApiModelRepository;
use App\Repository\Api\ApiTypeModelRepository;
use App\Repository\Eloquent\ColourMazdaRepository;
use App\Repository\Eloquent\DetailColourFixOrderRepository;
use App\Repository\Eloquent\DetailFixOrderRepository;
use App\Repository\Eloquent\MasterFixOrderRepository;
use App\Repository\Eloquent\MasterMonthOrderRepository;
use App\Repository\Eloquent\MazdaModelRepository;
use App\Repository\Eloquent\ModelColourMazdaRepository;
use App\Repository\Eloquent\MonthExceptionRuleRepository;
use App\Repository\Eloquent\RangeMonthFixOrderRepository;
use App\Repository\Eloquent\TypeModelMazdaRepository;
use App\Traits\WithDeleteCache;
use App\Traits\WithGoTo;
use App\Traits\WithWrsApi;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Livewire\Component;

class FixOrderEdit extends Component
{
    use WithWrsApi;
    use WithDeleteCache;
    use WithGoTo;

    public $pageTitle = 'Fix Order - Edit';
    public array $detailData = [];
    public $grandTotalQty = 0;
    public $idKey = '';
    public $modelName = '';
    public array $deleteIdDetailColor = [];
    public array $deleteIdDetail = [];
    public $grandTotalQtyBefore = 0;

    public array $flagOpen = [
        'flag_open_colour' => 0,
        'flag_open_volume' => 0,
    ];

    public array $bind = [
        'id_master_fix_order_unit' => '',
        'order_number_dealer' => ''
    ];

    protected $rules = [
        'bind.order_number_dealer' => 'required|min:1|max:100',
        'detailData.*.id_model' => 'required',
        'detailData.*.id_type' => 'required',
        'detailData.*.year_production' => 'required',
        'detailData.*.total_qty' => 'required|min:1|max:99999',
        'detailData.*.selected_colour.*.id_colour' => 'required',
        'detailData.*.selected_colour.*.qty' => 'required|numeric|min:1',
    ];

    protected $messages = [
        'bind.order_number_dealer.required' => 'Please fill Order Number Dealer',
        'bind.order_number_dealer.min' => 'Please fill Order Number Minimal :min Character',
        'bind.order_number_dealer.max' => 'Please fill Order Number Maximal :max Characters',
        'detailData.*.id_model.required' => 'Please Choose Model Name!',
        'detailData.*.id_type.required' => 'Please Choose Type Name!',
        'detailData.*.total_qty.required' => 'Quantity cant be Empty!',
        'detailData.*.year_production.required' => 'Please Choose Year Production!',
        'detailData.*.selected_colour.*.id_colour.required' => 'Please Choose Colour!',
        'detailData.*.selected_colour.*.qty.required' => 'Please Fill Quantity!',
        'detailData.*.selected_colour.*.qty.min' => 'Please Fill Quantity with Minimal :min!',
        'detailData.*.selected_colour.*.qty.number' => 'Please Fill Quantity with Number!',
    ];

    public function mount(
        $id, $idRule,
        MasterFixOrderRepository $masterFixOrderRepository,
        DetailFixOrderRepository $detailFixOrderRepository,
        DetailColourFixOrderRepository $detailColourFixOrderRepository,
        TypeModelMazdaRepository $typeModelMazdaRepository,
        ModelColourMazdaRepository $modelColourMazdaRepository,
        RangeMonthFixOrderRepository $rangeMonthFixOrderRepository,
        MasterMonthOrderRepository $masterMonthOrderRepository,
        MonthExceptionRuleRepository $monthExceptionRuleRepository
    ) {
        $masterFixOrder = $masterFixOrderRepository->getById($id);
        $this->bind['order_number_dealer'] = $masterFixOrder->no_order_dealer;
        $this->bind['id_master_fix_order_unit'] = $masterFixOrder->id_master_fix_order_unit;
        $this->grandTotalQtyBefore = $masterFixOrder->grand_total_qty;
        $this->grandTotalQty = $masterFixOrder->grand_total_qty;

        $checkBeforeOrAfter = $this->getBeforeOrAfter($masterMonthOrderRepository, $monthExceptionRuleRepository);

        if($checkBeforeOrAfter) {
            $flag_open_colour = 1;
            $flag_open_volume = 1;
        } else {
            $dataRangeMonth = $rangeMonthFixOrderRepository->getById($idRule);
            $flag_open_colour = (int) $dataRangeMonth->flag_open_colour;
            $flag_open_volume = (int) $dataRangeMonth->flag_open_volume;
        }

        $this->flagOpen = [
            'flag_open_colour' => $flag_open_colour,
            'flag_open_volume' => $flag_open_volume,
        ];

        $detailFixOrder = $detailFixOrderRepository->getByIdMaster($id);

        $arrayIdDetail = array();

        foreach($detailFixOrder as $detail) {
            $dataType = Cache::remember('data-type-with-id-model-'.$detail->id_model, 10,
            function () use($detail, $typeModelMazdaRepository) {
                return $typeModelMazdaRepository->getByIdModelFix($detail->id_model);
            });
            $dataType = json_decode($dataType, true);
            //$dataType = $apiTypeModelRepository->getByIdModel($detail->id_model);

            $dataColor = Cache::remember('data-color-with-id-model-'.$detail->id_model, 10,
            function () use($detail, $modelColourMazdaRepository) {
                return $modelColourMazdaRepository->getByIdModelFix($detail->id_model);
            });
            $dataColor = json_decode($dataColor, true);
            //$dataColor = $apiModelColorRepository->getByIdModel($detail->id_model);

            $detailData = array(
                'id_detail_fix_order_unit' => $detail->id_detail_fix_order_unit,
                'id_model' => $detail->id_model,
                'model_name' => $detail->model_name,
                'id_type' => $detail->id_type,
                'type_name' => $detail->type_name,
                'qty' => $detail->total_qty,
                'year_production' => $detail->year_production,
                'data_type' => $dataType,
                'data_colour' => $dataColor,
                'total_qty' => $detail->total_qty,
                'selected_colour' => array()
            );

            array_push($this->detailData, $detailData);
            array_push($arrayIdDetail, $detail->id_detail_fix_order_unit);
        }

        // update selected_colour
        foreach($arrayIdDetail as $key => $idDetail) {
            $dataColourFixOrder = $detailColourFixOrderRepository->getByIdDetail($idDetail);

            foreach($dataColourFixOrder as $detailColour) {
                $dataColour = array(
                    'id_detail_color_fix_order_unit' => $detailColour->id_detail_color_fix_order_unit,
                    'id_colour' => $detailColour->id_colour,
                    'colour_name' => $detailColour->colour_name,
                    'qty' => $detailColour->qty,
                );
                array_push($this->detailData[$key]['selected_colour'], $dataColour);
            }
        }

    }

    public function getBeforeOrAfter($masterMonthOrderRepository, $monthExceptionRuleRepository)
    {
        $idDealer = session()->get('user')['id_dealer'];

        $dataMonthExceptionRule = $monthExceptionRuleRepository->getByIdDealerAndIdMonth($idDealer, date('m'));
        $masterMonth = $masterMonthOrderRepository->getById(date('m'));

        if($dataMonthExceptionRule) {
            $dataLockDate = $dataMonthExceptionRule;
            $date_input_lock_start = $dataLockDate->date_input_lock_month_start;
            $date_input_lock_end = $dataLockDate->date_input_lock_month_end;
        } else {
            $dataLockDate = $masterMonth;
            $date_input_lock_start = $dataLockDate->date_input_lock_start;
            $date_input_lock_end = $dataLockDate->date_input_lock_end;
        }

        $checkBeforeOrAfter = eval("return ((string) date('Y-m-d') $masterMonth->operator_start '$date_input_lock_start')
                    && ((string) date('Y-m-d') $masterMonth->operator_end '$date_input_lock_end');");

        return $checkBeforeOrAfter;
    }

    public function addDetail()
    {
        $detailData = array(
            'id_detail_fix_order_unit' => '',
            'id_model' => '',
            'model_name' => '',
            'id_type' => '',
            'type_name' => '',
            'qty' => 0,
            'year_production' => Carbon::now()->year,
            'data_type' => [],
            'data_colour' =>  [],
            'total_qty' => 0,
            'selected_colour' => array(
                0 => array(
                    'id_detail_color_fix_order_unit' => '',
                    'id_colour' => '',
                    'colour_name' => '',
                    'qty' => 0,
                )
            )
        );

        array_push($this->detailData, $detailData);
    }

    public function addSubDetail()
    {
        $subDetailData = array(
            'id_detail_color_fix_order_unit' => '',
            'id_colour' => '',
            'colour_name' => '',
            'qty' => 0,
        );

        array_push($this->detailData[$this->idKey]['selected_colour'], $subDetailData);
    }

    public function deleteDetail($key, $idDetail)
    {
        array_push($this->deleteIdDetail, $idDetail);
        unset($this->detailData[$key]);
        $this->idKey = '';
        $this->sumTotalQty();
        $this->sumGrandTotalQty();
    }

    public function deleteSubDetail($key, $keySub, $idDetailColour)
    {
        array_push($this->deleteIdDetailColor, $idDetailColour);
        //dd($this->detailData[$key]['selected_colour'][$keySub]);
        unset($this->detailData[$key]['selected_colour'][$keySub]);
        // $this->idKey = '';
        $this->reSum();
    }

    public function updated($propertyName)
    {
        $this->reSum();
        $this->validate();
    }

    private function reSum()
    {
        $this->sumTotalQty();
        $this->sumGrandTotalQty();
    }

    private function sumTotalQty()
    {
        $totalQty = 0;
        if($this->idKey !== '') {
            foreach($this->detailData[$this->idKey]['selected_colour'] as $keySelected => $selectedColour)
            {
                    $totalQty += $this->detailData[$this->idKey]['selected_colour'][$keySelected]['qty']
                    ? $this->detailData[$this->idKey]['selected_colour'][$keySelected]['qty'] : 0;
            }

            $this->detailData[$this->idKey]['total_qty'] = $totalQty;
        }

    }

    private function sumGrandTotalQty()
    {
        $grandTotalQty = 0;
        foreach($this->detailData as $key => $detailData)
        {
            $grandTotalQty += $this->detailData[$key]['total_qty'] ? $this->detailData[$key]['total_qty'] : 0;
        }

        $this->grandTotalQty = $grandTotalQty;
    }

    public function updateDataType($key, $value,
        TypeModelMazdaRepository $typeModelMazdaRepository,
        ModelColourMazdaRepository $modelColourMazdaRepository
    ) {
        if($this->detailData[$key]['id_model'] != '') {
            $dataType = Cache::remember('data-type-with-id-model-'.$value, 30, function () use($value, $typeModelMazdaRepository) {
                return $typeModelMazdaRepository->getByIdModelFix($value);
            });
            $dataType = json_decode($dataType, true);
            //$dataType = $apiTypeModelRepository->getByIdModel($value);
            $this->detailData[$key]['data_type'] = $dataType ? $dataType : [];
            $this->modelName = $dataType[0]['name_model'] ? $dataType[0]['name_model'] : '';
        }

        $this->updateDataColour($key, $value, $modelColourMazdaRepository);
    }

    public function updateDataColour($key, $value, $modelColourMazdaRepository)
    {
        if($this->detailData[$key]['id_model'] != '') {
            $dataColor = Cache::remember('data-color-with-id-model-'.$value, 30, function () use($value, $modelColourMazdaRepository) {
                return $modelColourMazdaRepository->getByIdModelFix($value);
            });
            //$dataColor = $apiModelColorRepository->getByIdModel($value);
            // $this->detailData[$key]['data_colour'] = ($dataColor['count'] > 0) ? $dataColor['data'] : [];
            $this->detailData[$key]['data_colour'] = $dataColor ? $dataColor : [];
        } else {
            $this->detailData[$key]['data_colour'] = [];
        }

    }

    public function addForm($key)
    {
        //$this->resetForm();
        $this->idKey = $key;
        $this->emit('openModal');
    }

    public function render(
        MazdaModelRepository $mazdaModelRepository
    ) {
        $dealerName = session()->get('dealer')['nm_dealer'] ? session()->get('dealer')['nm_dealer'] : 'Admin';

        $dataModel = Cache::remember('data-model', 30, function () use($mazdaModelRepository) {
            return $mazdaModelRepository->getAvailableModelFix();
        });

        //$dataModel = $apiModelRepository->all();

        return view('livewire.sales-ordering.fix-order.fix-order-edit', [
            'dealerName' => $dealerName,
            'dataModel' => $dataModel ? $dataModel : [],
        ])
        ->layout('layouts.app', ['title' => $this->pageTitle]);
    }

    public function editProcess(
        MasterFixOrderRepository $masterFixOrderRepository,
        MazdaModelRepository $mazdaModelRepository,
        TypeModelMazdaRepository $typeModelMazdaRepository,
        ColourMazdaRepository $colourMazdaRepository
    ) {
        $this->validate();

        if($this->flagOpen['flag_open_volume'] == '0') {
            if($this->grandTotalQtyBefore != $this->grandTotalQty) {
                return session()->flash('volumeLockError', 'You can\'t change the Volume / Qty !');
            }
        }

        $dataMaster = array(
            'id_master_fix_order_unit' => $this->bind['id_master_fix_order_unit'],
            'grand_total_qty' => $this->grandTotalQty,
            'status' => '1'
        );

        $this->updateModelTypeColour($mazdaModelRepository, $typeModelMazdaRepository, $colourMazdaRepository);

        $where = array('no_order_dealer' => $this->bind['order_number_dealer']);

        $count = $masterFixOrderRepository->findDuplicateEdit($where, $dataMaster['id_master_fix_order_unit']);

        if($count > 0) {
            session()->flash('action_message',
            '<div class="alert alert-warning" role="alert">No Order Dealer : <strong>'.$this->bind['order_number_dealer'].'</strong> is Exists!</div>');
        } else {
            $checkDuplicate = $this->checkDuplicate();
            if($checkDuplicate) {
                session()->flash('action_message_detail', '<div class="alert alert-warning" role="alert">
                <strong>Model and Type are Duplicated!</strong> <p> Please check your Order, ensure if your Order with no Duplicate! </p></div>');
            } else {
                $update = $masterFixOrderRepository->updateDealerOrder($dataMaster, $this->detailData, $this->deleteIdDetail, $this->deleteIdDetailColor);

                if($update) {
                    $this->deleteCache();
                    session()->flash('action_message', '<div class="alert alert-primary" role="alert">Insert Data Success!</div>');
                    return redirect()->to(route('fix-order.index'));
                } else {
                    session()->flash('action_message', '<div class="alert alert-danger" role="alert">Insert Data Failed!</div>');
                }
            }

        }

    }

    /**
     * Check Duplicated Model, Type in Additional Order
     * @author Bernand
     * @return bool true | false
     */
    private function checkDuplicate()
    {
        $arrayModel = array_column($this->detailData, 'id_model');
        $arrayType = array_column($this->detailData, 'id_type');

        $checkDuplicate = $arrayModel != array_unique($arrayModel) && $arrayType != array_unique($arrayType);

        return $checkDuplicate;
    }

    private function updateModelTypeColour($mazdaModelRepository, $typeModelMazdaRepository, $colourMazdaRepository)
    {
        foreach($this->detailData as $key => $detailData) {
            // Model Name
            $cacheModel = 'data-model-getById-'.$this->detailData[$key]['id_model'];
            $dataModel = Cache::remember($cacheModel, 10, function () use($mazdaModelRepository, $key) {
                return $mazdaModelRepository->getByIdModel($this->detailData[$key]['id_model'])->toArray();
            });

            //$dataModel = $apiModelRepository->getById($this->detailData[$key]['id_model']);

            $this->detailData[$key]['model_name'] = $dataModel['name_model'];

            // Type Model Name
            $cacheType = 'data-type-model-getById-'.$this->detailData[$key]['id_type'];
            $dataTypeModel = Cache::remember($cacheType, 10, function () use($typeModelMazdaRepository, $key) {
                return $typeModelMazdaRepository->geByIdTypeModel($this->detailData[$key]['id_type'])->toArray();
            });

            //$dataModel = $apiTypeModelRepository->getById($this->detailData[$key]['id_type']);

            $this->detailData[$key]['type_name'] = $dataTypeModel['name_type'].' - '.$dataTypeModel['msc_code'];

            foreach($this->detailData[$key]['selected_colour'] as $keySelectedColor => $dataSelectedColor) {
                // Model Colour
                $cacheModelColor = 'data-model-color-getById-'.$this->detailData[$key]['selected_colour'][$keySelectedColor]['id_colour'];
                $dataModelColour = Cache::remember($cacheModelColor, 10, function () use($colourMazdaRepository, $key, $keySelectedColor) {
                    return $colourMazdaRepository->getByIdColour($this->detailData[$key]['selected_colour'][$keySelectedColor]['id_colour']);
                });

                //$dataModel = $apiColorRepository->getById($this->detailData[$key]['selected_colour'][$keySelectedColor]['id_colour']);
                $this->detailData[$key]['selected_colour'][$keySelectedColor]['colour_name'] = $dataModelColour['name_colour_global'];
            }

        }
    }
}
