<?php

namespace App\Http\Livewire\SalesOrdering\FixOrder;

use App\Models\MasterFixOrderUnit;
use App\Repository\Api\ApiColorRepository;
use App\Repository\Api\ApiModelColorRepository;
use App\Repository\Api\ApiModelRepository;
use App\Repository\Api\ApiTypeModelRepository;
use App\Repository\Eloquent\ColourMazdaRepository;
use App\Repository\Eloquent\MasterFixOrderRepository;
use App\Repository\Eloquent\MazdaModelRepository;
use App\Repository\Eloquent\ModelColourMazdaRepository;
use App\Repository\Eloquent\RangeMonthFixOrderRepository;
use App\Repository\Eloquent\TypeModelMazdaRepository;
use App\Traits\WithDeleteCache;
use App\Traits\WithGoTo;
use App\Traits\WithWrsApi;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Livewire\Component;

class FixOrderAdd extends Component
{
    use WithWrsApi;
    use WithGoTo;
    use WithDeleteCache;

    public $pageTitle = 'Fix Order - Add';
    public array $detailData = [];
    public $grandTotalQty = 0;
    public $idKey = 0;
    public $modelName = '';
    public $idMonth = '';

    public array $bind = [
        'order_number_dealer' => ''
    ];

    protected $rules = [
        'bind.order_number_dealer' => 'required|min:1|max:100',
        'detailData.*.id_model' => 'required',
        'detailData.*.id_type' => 'required',
        'detailData.*.year_production' => 'required',
        'detailData.*.total_qty' => 'required|min:1|max:99999',
        'detailData.*.selected_colour.*.id_colour' => 'required',
        'detailData.*.selected_colour.*.qty' => 'required|integer|min:1',
    ];

    protected $messages = [
        'bind.order_number_dealer.required' => 'Please fill Order Number Dealer',
        'bind.order_number_dealer.min' => 'Please fill Order Number Minimal :min Character',
        'bind.order_number_dealer.max' => 'Please fill Order Number Maximal :max Characters',
        'detailData.*.id_model.required' => 'Please Choose Model Name!',
        'detailData.*.id_type.required' => 'Please Choose Type Name!',
        'detailData.*.total_qty.required' => 'Quantity cant be Empty!',
        'detailData.*.year_production.required' => 'Please Choose Year Production!',
        'detailData.*.selected_colour.*.id_colour.required' => 'Please Choose Colour!',
        'detailData.*.selected_colour.*.qty.required' => 'Please Fill Quantity!',
        'detailData.*.selected_colour.*.qty.min' => 'Please Fill Quantity with Minimal :min!',
        'detailData.*.selected_colour.*.qty.integer' => 'Please Fill Quantity with Number!',
    ];

    public function mount($idMonth)
    {
        $this->idMonth = $idMonth;
        $detailData = array(
            'id_model' => '',
            'model_name' => '',
            'id_type' => '',
            'type_name' => '',
            'qty' => 0,
            'year_production' => Carbon::now()->year,
            'data_type' => [],
            'data_colour' =>  [],
            'total_qty' => 0,
            'selected_colour' => array(
                0 => array(
                    'id_colour' => '',
                    'colour_name' => '',
                    'qty' => 0,
                )
            )
        );

        array_push($this->detailData, $detailData);
    }

    public function addDetail()
    {
        $detailData = array(
            'id_model' => '',
            'model_name' => '',
            'id_type' => '',
            'type_name' => '',
            'qty' => 0,
            'year_production' => Carbon::now()->year,
            'data_type' => [],
            'data_colour' =>  [],
            'total_qty' => 0,
            'selected_colour' => array(
                0 => array(
                    'id_colour' => '',
                    'colour_name' => '',
                    'qty' => 0,
                )
            )
        );

        array_push($this->detailData, $detailData);
    }

    public function addSubDetail()
    {
        $subDetailData = array(
            'id_colour' => '',
            'colour_name' => '',
            'qty' => 0,
        );

        array_push($this->detailData[$this->idKey]['selected_colour'], $subDetailData);
    }

    public function deleteDetail($key)
    {
        unset($this->detailData[$key]);
    }

    public function deleteSubDetail($key, $keySub)
    {
        unset($this->detailData[$key]['selected_colour'][$keySub]);
        $this->reSum();
    }

    public function updated($propertyName)
    {
        $this->reSum();
        $this->validate();
    }

    private function reSum()
    {
        $this->sumTotalQty();
        $this->sumGrandTotalQty();
    }

    private function sumTotalQty()
    {
        $totalQty = 0;
        foreach($this->detailData[$this->idKey]['selected_colour'] as $keySelected => $selectedColour)
        {
                $totalQty += $this->detailData[$this->idKey]['selected_colour'][$keySelected]['qty']
                ? $this->detailData[$this->idKey]['selected_colour'][$keySelected]['qty'] : 0;
        }

        $this->detailData[$this->idKey]['total_qty'] = $totalQty;
    }

    private function sumGrandTotalQty()
    {
        $grandTotalQty = 0;
        foreach($this->detailData as $key => $detailData)
        {
            $grandTotalQty += $this->detailData[$key]['total_qty'] ? $this->detailData[$key]['total_qty'] : 0;
        }

        $this->grandTotalQty = $grandTotalQty;
    }

    public function addForm($key)
    {
        //$this->resetForm();
        $this->idKey = $key;
        $this->emit('openModal');
    }

    public function updateDataType($key, $value,
        TypeModelMazdaRepository $typeModelMazdaRepository,
        ModelColourMazdaRepository $modelColourMazdaRepository
    ) {
        if($this->detailData[$key]['id_model'] != '') {
            $dataType = Cache::remember('data-type-with-id-model-'.$value, 30, function () use($value, $typeModelMazdaRepository) {
                return $typeModelMazdaRepository->getByIdModelFix($value);
            });
            $dataType = json_decode($dataType, true);

            $this->detailData[$key]['data_type'] = $dataType ? $dataType : [];
        }

        $this->updateDataColour($key, $value, $modelColourMazdaRepository);

    }

    public function updateDataColour($key, $value, $modelColourMazdaRepository)
    {
        if($this->detailData[$key]['id_model'] != '') {
            $dataColor = Cache::remember('data-color-with-id-model-'.$value, 30, function () use($value, $modelColourMazdaRepository) {
                return $modelColourMazdaRepository->getByIdModelFix($value);
            });
            $dataColor = json_decode($dataColor, true);

            $this->detailData[$key]['data_colour'] = $dataColor ? $dataColor : [];
        }

    }

    public function render(
        MazdaModelRepository $mazdaModelRepository
    ) {
        $dealerName = session()->get('dealer')['nm_dealer'] ? session()->get('dealer')['nm_dealer'] : 'Admin';

        $dataModel = Cache::remember('data-model', 30, function () use($mazdaModelRepository) {
            return $mazdaModelRepository->getAvailableModelFix();
        });

        //$dataModel = $apiModelRepository->all();

        return view('livewire.sales-ordering.fix-order.fix-order-add', [
            'dealerName' => $dealerName,
            'dataModel' => $dataModel,
        ])
        ->layout('layouts.app', ['title' => $this->pageTitle]);
    }

    public function addProcess(
        MasterFixOrderRepository $masterFixOrderRepository,
        MazdaModelRepository $mazdaModelRepository,
        TypeModelMazdaRepository $typeModelMazdaRepository,
        ColourMazdaRepository $colourMazdaRepository,
        RangeMonthFixOrderRepository $rangeMonthFixOrderRepository
    ) {
        $this->validate();

        $dataMaster = array(
            'no_order_atpm' => '',
            'no_order_dealer' => $this->bind['order_number_dealer'],
            'date_save_order' => Carbon::now(),
            'id_dealer' => session()->get('user')['id_dealer'],
            'nama_dealer' => session()->get('dealer')['nm_dealer'],
            'id_user' => session()->get('user')['id_user'],
            'user_order' => session()->get('user')['nama_user'],
            'id_month' => $this->idMonth,
            'year_order' => Carbon::now()->year,
            'grand_total_qty' => $this->grandTotalQty,
            'email_user_order' => session()->get('user')['email'],
            'status' => '1'
        );

        $this->updateModelTypeColour($mazdaModelRepository, $typeModelMazdaRepository, $colourMazdaRepository);

        $where = array('no_order_dealer' => $this->bind['order_number_dealer']);

        $count = $masterFixOrderRepository->findDuplicate($where);

        if($count > 0) {
            session()->flash('action_message',
            '<div class="alert alert-warning" role="alert">No Order Dealer : <strong>'.$this->bind['order_number_dealer'].'</strong> is Exists!</div>');
        } else {
            $checkDuplicate = $this->checkDuplicate();
            if($checkDuplicate) {
                session()->flash('action_message_detail', '<div class="alert alert-warning" role="alert">
                <strong>Model and Type are Duplicated!</strong> <p> Please check your Order, ensure if your Order with no Duplicate! </p></div>');
            } else {
                $insert = $masterFixOrderRepository->createDealerOrder($dataMaster, $this->detailData, $this->idMonth);

                if($insert) {
                    $this->deleteCache();
                    session()->flash('action_message', '<div class="alert alert-primary" role="alert">Insert Data Success!</div>');
                    return redirect()->to(route('fix-order.index'));
                } else {
                    session()->flash('action_message', '<div class="alert alert-danger" role="alert">Insert Data Failed!</div>');
                }
            }

        }

    }

    /**
     * Check Duplicated Model, Type in Additional Order
     * @author Bernand
     * @return bool true | false
     */
    private function checkDuplicate()
    {
        $arrayModel = array_column($this->detailData, 'id_model');
        $arrayType = array_column($this->detailData, 'id_type');

        $checkDuplicate = $arrayModel != array_unique($arrayModel) && $arrayType != array_unique($arrayType);

        return $checkDuplicate;
    }

    private function updateModelTypeColour($mazdaModelRepository, $typeModelMazdaRepository, $colourMazdaRepository)
    {
        foreach($this->detailData as $key => $detailData) {
            // Model Name
            $cacheModel = 'data-model-getById-'.$this->detailData[$key]['id_model'];
            $dataModel = Cache::remember($cacheModel, 10, function () use($mazdaModelRepository, $key) {
                return $mazdaModelRepository->getByIdModel($this->detailData[$key]['id_model'])->toArray();
            });

            //$dataModel = $apiModelRepository->getById($this->detailData[$key]['id_model']);

            $this->detailData[$key]['model_name'] = $dataModel['name_model'];

            // Type Model Name
            $cacheType = 'data-type-model-getById-'.$this->detailData[$key]['id_type'];
            $dataTypeModel = Cache::remember($cacheType, 10, function () use($typeModelMazdaRepository, $key) {
                return $typeModelMazdaRepository->geByIdTypeModel($this->detailData[$key]['id_type'])->toArray();
            });

            //$dataModel = $apiTypeModelRepository->getById($this->detailData[$key]['id_type']);

            $this->detailData[$key]['type_name'] = $dataTypeModel['name_type'].' - '.$dataTypeModel['msc_code'];

            foreach($this->detailData[$key]['selected_colour'] as $keySelectedColor => $dataSelectedColor) {
                // Model Colour
                $cacheModelColor = 'data-model-color-getById-'.$this->detailData[$key]['selected_colour'][$keySelectedColor]['id_colour'];
                $dataModelColour = Cache::remember($cacheModelColor, 10, function () use($colourMazdaRepository, $key, $keySelectedColor) {
                    return $colourMazdaRepository->getByIdColour($this->detailData[$key]['selected_colour'][$keySelectedColor]['id_colour']);
                });

                //$dataModel = $apiColorRepository->getById($this->detailData[$key]['selected_colour'][$keySelectedColor]['id_colour']);
                $this->detailData[$key]['selected_colour'][$keySelectedColor]['colour_name'] = $dataModelColour['name_colour_global'];
            }

        }
    }
}
