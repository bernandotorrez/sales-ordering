<?php

namespace App\Http\Livewire\SalesOrdering\FixOrder;

use App\Repository\Eloquent\MasterFixOrderRepository;
use App\Repository\Eloquent\MasterMonthOrderRepository;
use App\Repository\Eloquent\MonthExceptionRuleRepository;
use App\Repository\Eloquent\RangeMonthFixOrderRepository;
use Livewire\Component;
use App\Traits\WithGoTo;

class FixOrderIndex extends Component
{
    use WithGoTo;

    public function render(
        MasterMonthOrderRepository $masterMonthOrderRepository,
        RangeMonthFixOrderRepository $rangeMonthFixOrderRepository,
        MasterFixOrderRepository $masterFixOrderRepository
    ) {
        $idDealer = session()->get('user')['id_dealer'];
        $dataMastermonth = $masterMonthOrderRepository->allActive();
        $dataRangeMonth = $rangeMonthFixOrderRepository->getByIdMonth(date('m'));
        $rangeMonth = array();
        //array_push($rangeMonth, date('m'));

        // Uncomment ini kalau mau tampil 3 tab
        // foreach($dataRangeMonth as $month) {
        //     array_push($rangeMonth, $month->month_id_to);
        // }

        array_push($rangeMonth, $dataRangeMonth[0]->month_id_to);

        $where = array(
            'status' => '1',
            'id_dealer' => $idDealer,
            'id_month' => $rangeMonth[0]
        );
        $countOrder = $masterFixOrderRepository->findDuplicate($where);

        return view('livewire.sales-ordering.fix-order.fix-order-index', [
            'dataMasterMonth' => $dataMastermonth,
            'dataRangeMonth' => $dataRangeMonth,
            'rangeMonth' => $rangeMonth,
            'countOrder' => $countOrder
        ])
        ->layout('layouts.app', ['title' => 'Fix Order Dealer Admin']);
    }
}
