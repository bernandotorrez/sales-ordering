<?php

namespace App\Http\Livewire\SalesOrdering\Report;

use App\Exports\AdditionalOrderExport;
use App\Exports\FixOrderExport;
use App\Repository\Api\ApiDealerRepository;
use App\Repository\Eloquent\MasterMonthOrderRepository;
use App\Traits\WithGoTo;
use Livewire\Component;
use Maatwebsite\Excel\Facades\Excel;

class ReportIndex extends Component
{
    use WithGoTo;

    public $pageTitle = 'Report Order';

    public $bind = [
        'month' => '',
        'year' => ''
    ];

    protected $rules = [
        'bind.month' => 'required',
        'bind.year' => 'required',
    ];

    protected $messages = [
        'bind.month.required' => 'Please Choose Month!',
        'bind.year.required' => 'Please Choose Year!',
    ];

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function render(
        MasterMonthOrderRepository $masterMonthOrderRepository,
        ApiDealerRepository $apiDealerRepository
    ) {
        $dataMonth = $masterMonthOrderRepository->allActive();
        $dataDealer = $apiDealerRepository->all();
        return view('livewire.sales-ordering.report.report-index', ['dataMonth' => $dataMonth, 'dataDealer' => $dataDealer])
        ->layout('layouts.app', ['title' => $this->pageTitle]);
    }

    public function exportAdditionalOrder($dateStart, $dateEnd, $idDealer, $statusOrder)
    {
        $excelName = 'additional_order.xlsx';
        return Excel::download(new AdditionalOrderExport($dateStart, $dateEnd, $idDealer, $statusOrder), $excelName);

    }

    public function exportFixOrder($dateStart, $dateEnd, $idDealer, $statusOrder)
    {
        $excelName = 'fix_order.xlsx';
        return Excel::download(new FixOrderExport($dateStart, $dateEnd, $idDealer, $statusOrder), $excelName);
    }

    public function addForm()
    {
        $this->resetForm();
        $this->emit('openModal');
    }

    private function resetForm()
    {
        $this->reset(['bind']);
    }
}
