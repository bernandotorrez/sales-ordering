<?php

namespace App\Http\Livewire\SalesOrdering\ReportAnalyst;

use Livewire\Component;
use App\Repository\Eloquent\DetailAdditionalOrderRepository;
use App\Repository\Eloquent\DetailFixOrderRepository;
use App\Repository\Eloquent\MasterMonthOrderRepository;

class ReportAnalystIndex extends Component
{
    protected $pageTitle = 'Report Analyst';
    public $bind = [
        'month' => '',
        'year' => ''
    ];

    protected $rules = [
        'bind.month' => 'required',
        'bind.year' => 'required'
    ];

    protected $messages = [
        'bind.month.required' => 'Please Choose Month!',
        'bind.year.required' => 'Please Choose Year!',
    ];

    public function render(
        DetailAdditionalOrderRepository $detailAdditionalOrderRepository,
        DetailFixOrderRepository $detailFixOrderRepository,
        MasterMonthOrderRepository $masterMonthOrderRepository
    ) {
        $statusAtpm = session()->get('user')['status_atpm'];
        $idDealer = session()->get('user')['id_dealer'];

        $dataTopOrderedModelAdditional = $detailAdditionalOrderRepository->getTopOrderedModel($statusAtpm, $idDealer, $this->bind);
        $dataTopOrderedModelFix = $detailFixOrderRepository->getTopOrderedModel($statusAtpm, $idDealer, $this->bind);

        $dataTopOrderedTypeAdditional = $detailAdditionalOrderRepository->getTopOrderedType($statusAtpm, $idDealer, $this->bind);
        $dataTopOrderedTypeFix = $detailFixOrderRepository->getTopOrderedType($statusAtpm, $idDealer, $this->bind);

        $dataMasterMonth = $masterMonthOrderRepository->allActive();

        return view('livewire.sales-ordering.report-analyst.report-analyst-index', [
            'dataTopOrderedModelAdditional' => $dataTopOrderedModelAdditional,
            'dataTopOrderedModelFix' => $dataTopOrderedModelFix,
            'dataTopOrderedTypeAdditional' => $dataTopOrderedTypeAdditional,
            'dataTopOrderedTypeFix' => $dataTopOrderedTypeFix,
            'dataMasterMonth' => $dataMasterMonth
        ])->layout('layouts.app', array('title' => $this->pageTitle));
    }
}
