<?php

namespace App\Http\Livewire\SalesOrdering\ModelMazda;

use App\Repository\Api\ApiModelRepository;
use App\Repository\Eloquent\CacheRepository;
use App\Repository\Eloquent\MazdaModelRepository;
use App\Traits\WithDeleteCache;
use App\Traits\WithPaginationAttribute;
use App\Traits\WithSorting;
use Illuminate\Support\Facades\Cache;
use Livewire\Component;
use Livewire\WithPagination;

class ModelMazdaIndex extends Component
{
    use WithPagination;
    use WithPaginationAttribute;
    use WithSorting;
    use WithDeleteCache;

    /**
     * Page Attributes
     */
    public string $pageTitle = "Model Mazda";
    public bool $isEdit = false, $allChecked = false;
    public array $checked = [];

    protected $queryString = [
        'search' => ['except' => ''],
        'page' => ['except' => 1]
    ];

    public $bind = [
        'id_model_mazda' => '0',
        'id_model' => '',
        'name_model' => '',
        'flag_show_additional' => '',
        'flag_show_fix' => '',
    ];

    /**
     * Validation Attributes
     */
    protected $rules = [
        'bind.id_model_mazda' => 'required',
        'bind.id_model' => 'required',
        'bind.name_model' => 'required|min:3|max:100',
        'bind.flag_show_additional' => 'required|in:0,1',
        'bind.flag_show_fix' => 'required|in:0,1',
    ];

    protected $messages = [
        'bind.id_model_mazda.required' => 'Please fill ID Model Mazda field!',
        'bind.id_model.required' => 'Please fill ID Model field!',
        'bind.name_model.required' => 'Please fill Model Mazda field!',
        'bind.name_model.min' => 'Please fill Model Mazda field with Minimal :min Characters',
        'bind.name_model.max' => 'Please fill Model Mazda field with Maximal :max Characters',
        'bind.flag_show_additional.required' => 'Please fill Flag Show Additional field!',
        'bind.flag_show_additional.in' => 'Please fill Flag Show Additional field with 0 or 1',
        'bind.flag_show_fix.required' => 'Please fill Flag Show Fix field!',
        'bind.flag_show_fix.in' => 'Please fill Flag Flag Show Fix field with 0 or 1',
    ];

    public function mount()
    {
        $this->sortBy = 'name_model';
        $this->fill(request()->only('search', 'page'));
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function resetForm()
    {
        $this->reset(['bind']);
    }

    public function render(
        MazdaModelRepository $mazdaModelRepository,
        CacheRepository $cacheRepository
    ) {
        $mazdaModelCache = 'mazdamodel-menu-index-page-'.$this->page.'-pageselected-'.$this->perPageSelected.'-search-'.$this->search;
        $mazdaModelCache .= '-sortby-'.$this->sortBy.'-sortdirection-'.$this->sortDirection.'-user-'.session()->get('user')['id_user'];

        $dataMazdaModel = Cache::remember($mazdaModelCache, 15, function () use($mazdaModelRepository, $cacheRepository, $mazdaModelCache) {
            $cacheRepository->firstOrCreate(['cache_name' => $mazdaModelCache, 'id_user' => session()->get('user')['id_user']]);

            return $mazdaModelRepository->pagination(
                $this->search,
                $this->sortBy,
                $this->sortDirection,
                $this->perPageSelected
            );
        });

        return view('livewire.sales-ordering.model-mazda.model-mazda-index', [
            'dataMazdaModel' => $dataMazdaModel
        ])
        ->layout('layouts.app', ['title' => $this->pageTitle]);
    }

    public function allChecked(MazdaModelRepository $mazdaModelRepository)
    {
        $datas = $mazdaModelRepository->checked(
            $this->search,
            $this->sortBy,
            $this->sortDirection,
            $this->perPageSelected
        );

        $id = $mazdaModelRepository->getPrimaryKey();

        // Dari Unchecked ke Checked
        if($this->allChecked == true) {
            foreach($datas as $data) {
                if(!in_array($data->$id, $this->checked)) {
                    array_push($this->checked, (string) $data->$id);
                }
            }
        } else {
            // Checked ke Unchecked
            $this->checked = [];
        }
    }

    public function addForm()
    {
        $this->isEdit = false;
        $this->resetForm();
        $this->emit('openModal');
    }

    public function addProcess(MazdaModelRepository $mazdaModelRepository)
    {
        $this->validate();

        $data = array(
            'id_model' => $this->bind['id_model'],
            'name_model' => $this->bind['name_model'],
            'flag_show_additional' => $this->bind['flag_show_additional'],
            'flag_show_fix' => $this->bind['flag_show_fix'],
        );

        $where = array(
            'name_model' => $this->bind['name_model'],
        );

        $count = $mazdaModelRepository->findDuplicate($where);

        if($count <= 0) {
            $insert = $mazdaModelRepository->create($data);

            if($insert) {
                $this->resetForm();
                $this->deleteCache();
                $this->emit('closeModal');

                session()->flash('action_message', '<div class="alert alert-success">Insert Data Success!</div>');
            } else {
                session()->flash('action_message', '<div class="alert alert-danger">Insert Data Failed!</div>');
            }
        } else {
            session()->flash('message_duplicate', '<div class="alert alert-warning"><strong>'.$this->bind['name_model'].'</strong> Already Exists!</div>');
        }
    }

    public function editForm(MazdaModelRepository $mazdaModelRepository)
    {
        $this->isEdit = true;

        $data = $mazdaModelRepository->getByID($this->checked[0]);
        $this->bind['id_model_mazda'] = $data->id_model_mazda;
        $this->bind['id_model'] = $data->id_model;
        $this->bind['name_model'] = $data->name_model;
        $this->bind['flag_show_additional'] = $data->flag_show_additional;
        $this->bind['flag_show_fix'] = $data->flag_show_fix;

        $this->emit('openModal');
    }

    public function editProcess(MazdaModelRepository $mazdaModelRepository)
    {
        $this->validate();

        $data = array(
            'id_model_mazda' => $this->bind['id_model_mazda'],
            'id_model' => $this->bind['id_model'],
            'name_model' => $this->bind['name_model'],
            'flag_show_additional' => $this->bind['flag_show_additional'],
            'flag_show_fix' => $this->bind['flag_show_fix'],
        );

        $where = array(
            'name_model' => $this->bind['name_model'],
        );
        $count = $mazdaModelRepository->findDuplicateEdit($where, $this->bind['id_model_mazda']);

        if($count >= 1) {
            session()->flash('message_duplicate', '<div class="alert alert-warning"><strong>'.$this->bind['name_model'].'</strong> Already Exists!</div>');
        } else {
            $update = $mazdaModelRepository->update($this->bind['id_model_mazda'], $data);

            if($update) {
                $this->isEdit = false;
                $this->resetForm();
                $this->deleteCache();
                $this->emit('closeModal');

                session()->flash('action_message', '<div class="alert alert-success">Update Data Success!</div>');
            } else {
                session()->flash('action_message', '<div class="alert alert-danger">Update Data Failed!</div>');
            }
        }
    }

    public function deleteProcess(MazdaModelRepository $mazdaModelRepository)
    {
        $delete = $mazdaModelRepository->massDelete($this->checked);

        if($delete) {
            $this->resetForm();
            $this->checked = [];
            $this->deleteCache();
            $deleteStatus = 'success';
        } else {
            $deleteStatus = 'failed';
        }

        $this->emit('deleted', $deleteStatus);
    }

    public function syncFromWRS(
        ApiModelRepository $apiModelRepository,
        MazdaModelRepository $mazdaModelRepository,
        CacheRepository $cacheRepository
    ) {
        $dataMazdaModel = $apiModelRepository->all();

        foreach($dataMazdaModel['data'] as $model) {
            $checkDuplicate = Cache::remember('syncFromWrs-Model-kd_model-'.$model['kd_model'], 30, function () use($mazdaModelRepository, $model) {
                $where = array(
                    'id_model' => $model['kd_model']
                );
                return $mazdaModelRepository->findDuplicate($where);
            });

            if($checkDuplicate == '0') {
                $data = array(
                    'id_model' => $model['kd_model'],
                    'name_model' => $model['nm_model'],
                    'kind_vehicle' => $model['jenis_kendaraan'],
                    'model_year' => $model['model_year'],
                    'engine_type' => $model['engine_type'],
                    'vehicle_type' => $model['vehicle_type'],
                    'wmi' => $model['wmi'],
                    'model_moc' => $model['model_moc'],
                    'flag_show_additional' => '1',
                    'flag_show_fix' => '1',
                    'status' => '1'
                );
                $insert = $mazdaModelRepository->create($data);
            } else {
                $data = array(
                    'id_model' => $model['kd_model'],
                    'name_model' => $model['nm_model'],
                    'kind_vehicle' => $model['jenis_kendaraan'],
                    'model_year' => $model['model_year'],
                    'engine_type' => $model['engine_type'],
                    'vehicle_type' => $model['vehicle_type'],
                    'wmi' => $model['wmi'],
                    'model_moc' => $model['model_moc'],
                    'flag_show_additional' => '1',
                    'flag_show_fix' => '1',
                    'status' => '1'
                );
                $update = $mazdaModelRepository->update($data['id_model'], $data);
            }
        }

        $this->deleteCache();
        $this->render($mazdaModelRepository, $cacheRepository);
    }
}
