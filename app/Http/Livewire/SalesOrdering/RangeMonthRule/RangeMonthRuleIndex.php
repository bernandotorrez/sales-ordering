<?php

namespace App\Http\Livewire\SalesOrdering\RangeMonthRule;

use App\Repository\Eloquent\CacheRepository;
use App\Repository\Eloquent\MasterMonthOrderRepository;
use App\Repository\Eloquent\RangeMonthFixOrderRepository;
use App\Traits\WithDeleteCache;
use App\Traits\WithPaginationAttribute;
use App\Traits\WithSorting;
use Illuminate\Support\Facades\Cache;
use Livewire\Component;
use Livewire\WithPagination;

class RangeMonthRuleIndex extends Component
{
    use WithPagination;
    use WithPaginationAttribute;
    use WithSorting;
    use WithDeleteCache;

     /**
     * Page Attributes
     */
    public string $pageTitle = "Range Month Rule";
    public bool $isEdit = false, $allChecked = false;
    public array $checked = [];
    protected string $viewName = 'view_range_month_rule';

    protected $queryString = [
        'search' => ['except' => ''],
        'page' => ['except' => 1]
    ];

    public $bind = [
        'id_rule_month',
        'id_month',
        'month_id_to',
        'flag_open_colour',
        'flag_open_volume',
        'flag_button_add_before',
        'flag_button_amend_before',
        'flag_button_send_approval_before',
        'flag_button_revise_before',
        'flag_button_planning_before',
        'flag_button_submit_before',
        'flag_button_approve_before',
        'flag_button_add_after',
        'flag_button_amend_after',
        'flag_button_send_approval_after',
        'flag_button_revise_after',
        'flag_button_planning_after',
        'flag_button_submit_after',
        'flag_button_approve_after',
    ];

    /**
     * Validation Attributes
     */
    protected $rules = [
        'bind.id_month' => 'required',
        'bind.month_id_to' => 'required',
        'bind.flag_open_colour' => 'required|in:0,1',
        'bind.flag_open_volume' => 'required|in:0,1',
        'bind.flag_button_add_before' => 'required|in:0,1',
        'bind.flag_button_amend_before' => 'required|in:0,1',
        'bind.flag_button_send_approval_before' => 'required|in:0,1',
        'bind.flag_button_revise_before' => 'required|in:0,1',
        'bind.flag_button_planning_before' => 'required|in:0,1',
        'bind.flag_button_submit_before' => 'required|in:0,1',
        'bind.flag_button_approve_before' => 'required|in:0,1',
        'bind.flag_button_add_after' => 'required|in:0,1',
        'bind.flag_button_amend_after' => 'required|in:0,1',
        'bind.flag_button_send_approval_after' => 'required|in:0,1',
        'bind.flag_button_revise_after' => 'required|in:0,1',
        'bind.flag_button_planning_after' => 'required|in:0,1',
        'bind.flag_button_submit_after' => 'required|in:0,1',
        'bind.flag_button_approve_after' => 'required|in:0,1',
    ];

    protected $messages = [
        'bind.id_month.required' => 'Please fill Month field!',
        'bind.month_id_to.required' => 'Please fill Month ID field!',
        'bind.flag_open_colour.required' => 'Please fill Flag Open Colour field!',
        'bind.flag_open_colour.in' => 'Please fill Flag Open Colour field with 0 or 1',
        'bind.flag_open_volume.required' => 'Please fill Flag Open Volume field!',
        'bind.flag_open_volume.in' => 'Please fill Flag Open Volume field with 0 or 1',
        'bind.flag_button_add_before.required' => 'Please fill Flag Add Before field!',
        'bind.flag_button_add_before.in' => 'Please fill Flag Button Add Before field with 0 or 1',
        'bind.flag_button_amend_before.required' => 'Please fill Flag Button Amend Before field!',
        'bind.flag_button_amend_before.in' => 'Please fill Flag Button Amend Before field with 0 or 1',
        'bind.flag_button_send_approval_before.required' => 'Please fill Flag Button Send Approval Before field!',
        'bind.flag_button_send_approval_before.in' => 'Please fill Flag Button Send Approval Before field with 0 or 1',
        'bind.flag_button_revise_before.required' => 'Please fill Flag Button Revise Before field!',
        'bind.flag_button_revise_before.in' => 'Please fill Flag Button Revise Before field with 0 or 1',
        'bind.flag_button_planning_before.required' => 'Please fill Flag Button Planning Before field!',
        'bind.flag_button_planning_before.in' => 'Please fill Flag Button Planning Before field with 0 or 1',
        'bind.flag_button_submit_before.required' => 'Please fill Flag Button Submit Before field!',
        'bind.flag_button_submit_before.in' => 'Please fill Flag Button Revise Submit field with 0 or 1',
        'bind.flag_button_approve_before.required' => 'Please fill Flag Button Approve Before field!',
        'bind.flag_button_approve_before.in' => 'Please fill Flag Button Approve Submit field with 0 or 1',
        'bind.flag_button_add_after.required' => 'Please fill Flag Add After field!',
        'bind.flag_button_add_after.in' => 'Please fill Flag Button Add After field with 0 or 1',
        'bind.flag_button_amend_after.required' => 'Please fill Flag Button Amend After field!',
        'bind.flag_button_amend_after.in' => 'Please fill Flag Button Amend After field with 0 or 1',
        'bind.flag_button_send_approval_after.required' => 'Please fill Flag Button Send Approval After field!',
        'bind.flag_button_send_approval_after.in' => 'Please fill Flag Button Send Approval After field with 0 or 1',
        'bind.flag_button_revise_after.required' => 'Please fill Flag Button Revise After field!',
        'bind.flag_button_revise_after.in' => 'Please fill Flag Button Revise After field with 0 or 1',
        'bind.flag_button_planning_after.required' => 'Please fill Flag Button Planning After field!',
        'bind.flag_button_planning_after.in' => 'Please fill Flag Button Planning After field with 0 or 1',
        'bind.flag_button_submit_after.required' => 'Please fill Flag Button Submit After field!',
        'bind.flag_button_submit_after.in' => 'Please fill Flag Button Revise Submit field with 0 or 1',
        'bind.flag_button_approve_after.required' => 'Please fill Flag Button Approve After field!',
        'bind.flag_button_approve_after.in' => 'Please fill Flag Button Approve Submit field with 0 or 1',
    ];

    public function mount()
    {
        $this->sortBy = 'id_range_rule';
        $this->fill(request()->only('search', 'page'));
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function resetForm()
    {
        $this->reset(['bind']);
    }

    public function render(
        RangeMonthFixOrderRepository $rangeMonthFixOrderRepository,
        MasterMonthOrderRepository $masterMonthOrderRepository,
        CacheRepository $cacheRepository
    ) {

        $masterMonthCache = 'rangemonthrule-masterMonth-index-page-'.$this->page.'-pageselected-'.$this->perPageSelected.'-search-'.$this->search;
        $masterMonthCache .= '-sortby-'.$this->sortBy.'-sortdirection-'.$this->sortDirection.'-user-'.session()->get('user')['id_user'];

        $dataMasterMonth = Cache::remember($masterMonthCache, 15, function () use($masterMonthOrderRepository, $cacheRepository, $masterMonthCache) {
            $cacheRepository->firstOrCreate(['cache_name' => $masterMonthCache, 'id_user' => session()->get('user')['id_user']]);
            return $masterMonthOrderRepository->allActive();
        });

        $rangeMonthCache = 'rangemonthrule-RangeMonth-index-page-'.$this->page.'-pageselected-'.$this->perPageSelected.'-search-'.$this->search;
        $rangeMonthCache .= '-sortby-'.$this->sortBy.'-sortdirection-'.$this->sortDirection.'-user-'.session()->get('user')['id_user'];

        $dataRangeMonth = Cache::remember($rangeMonthCache, 15, function () use($rangeMonthFixOrderRepository, $cacheRepository, $rangeMonthCache) {
            $cacheRepository->firstOrCreate(['cache_name' => $rangeMonthCache, 'id_user' => session()->get('user')['id_user']]);
            return $rangeMonthFixOrderRepository->viewPagination(
                $this->viewName,
                $this->search,
                $this->sortBy,
                $this->sortDirection,
                $this->perPageSelected
            );
        });

        return view('livewire.sales-ordering.range-month-rule.range-month-rule-index', [
            'dataMasterMonth' => $dataMasterMonth,
            'dataRangeMonth' => $dataRangeMonth
        ])
        ->layout('layouts.app', ['title' => $this->pageTitle]);
    }

    public function allChecked(RangeMonthFixOrderRepository $rangeMonthFixOrderRepository)
    {
        $datas = $rangeMonthFixOrderRepository->viewChecked(
            $this->viewName,
            $this->search,
            $this->sortBy,
            $this->sortDirection,
            $this->perPageSelected,
        );

        $id = $rangeMonthFixOrderRepository->getPrimaryKey();

        // Dari Unchecked ke Checked
        if($this->allChecked == true) {
            foreach($datas as $data) {
                if(!in_array($data->$id, $this->checked)) {
                    array_push($this->checked, (string) $data->$id);
                }
            }
        } else {
            // Checked ke Unchecked
            $this->checked = [];
        }
    }

    public function addForm()
    {
        $this->isEdit = false;
        $this->resetForm();
        $this->emit('openModal');
    }

    public function addProcess(RangeMonthFixOrderRepository $rangeMonthFixOrderRepository)
    {
        $this->validate();

        $data = array(
            'id_month' => $this->bind['id_month'],
            'month_id_to' => $this->bind['month_id_to'],
            'flag_open_colour' => $this->bind['flag_open_colour'],
            'flag_open_volume' => $this->bind['flag_open_volume'],
            'flag_button_add_before' => $this->bind['flag_button_add_before'],
            'flag_button_amend_before' => $this->bind['flag_button_amend_before'],
            'flag_button_send_approval_before' => $this->bind['flag_button_send_approval_before'],
            'flag_button_revise_before' => $this->bind['flag_button_revise_before'],
            'flag_button_planning_before' => $this->bind['flag_button_planning_before'],
            'flag_button_submit_before' => $this->bind['flag_button_submit_before'],
            'flag_button_approve_before' => $this->bind['flag_button_approve_before'],
            'flag_button_add_after' => $this->bind['flag_button_add_after'],
            'flag_button_amend_after' => $this->bind['flag_button_amend_after'],
            'flag_button_send_approval_after' => $this->bind['flag_button_send_approval_after'],
            'flag_button_revise_after' => $this->bind['flag_button_revise_after'],
            'flag_button_planning_after' => $this->bind['flag_button_planning_after'],
            'flag_button_submit_after' => $this->bind['flag_button_submit_after'],
            'flag_button_approve_after' => $this->bind['flag_button_submit_after'],
        );

        $where = array(
            'id_month' => $this->bind['id_month'],
            'month_id_to' => $this->bind['month_id_to']
        );

        $count = $rangeMonthFixOrderRepository->findDuplicate($where);

        if($count <= 0) {
            $insert = $rangeMonthFixOrderRepository->create($data);

            if($insert) {
                $this->resetForm();
                $this->deleteCache();
                $this->emit('closeModal');

                session()->flash('action_message', '<div class="alert alert-success">Insert Data Success!</div>');
            } else {
                session()->flash('action_message', '<div class="alert alert-danger">Insert Data Failed!</div>');
            }
        } else {
            session()->flash('message_duplicate', '<div class="alert alert-warning">Rule Already Exists!</div>');
        }
    }

    public function editForm(RangeMonthFixOrderRepository $rangeMonthFixOrderRepository)
    {
        $this->isEdit = true;

        $data = $rangeMonthFixOrderRepository->getByID($this->checked[0]);
        $this->bind['id_range_rule'] = $data->id_range_rule;
        $this->bind['id_month'] = $data->id_month;
        $this->bind['month_id_to'] = $data->month_id_to;
        $this->bind['flag_open_colour'] = $data->flag_open_colour;
        $this->bind['flag_open_volume'] = $data->flag_open_volume;
        $this->bind['flag_button_add_before'] = $data->flag_button_add_before;
        $this->bind['flag_button_amend_before'] = $data->flag_button_amend_before;
        $this->bind['flag_button_send_approval_before'] = $data->flag_button_send_approval_before;
        $this->bind['flag_button_revise_before'] = $data->flag_button_revise_before;
        $this->bind['flag_button_planning_before'] = $data->flag_button_planning_before;
        $this->bind['flag_button_submit_before'] = $data->flag_button_submit_before;
        $this->bind['flag_button_approve_before'] = $data->flag_button_approve_before;
        $this->bind['flag_button_add_after'] = $data->flag_button_add_after;
        $this->bind['flag_button_amend_after'] = $data->flag_button_amend_after;
        $this->bind['flag_button_send_approval_after'] = $data->flag_button_send_approval_after;
        $this->bind['flag_button_revise_after'] = $data->flag_button_revise_after;
        $this->bind['flag_button_planning_after'] = $data->flag_button_planning_after;
        $this->bind['flag_button_submit_after'] = $data->flag_button_submit_after;
        $this->bind['flag_button_approve_after'] = $data->flag_button_approve_after;

        $this->emit('openModal');
    }

    public function editProcess(RangeMonthFixOrderRepository $rangeMonthFixOrderRepository)
    {
        $this->validate();

        $data = array(
            'id_range_rule' => $this->bind['id_range_rule'],
            'id_month' => $this->bind['id_month'],
            'month_id_to' => $this->bind['month_id_to'],
            'flag_open_colour' => $this->bind['flag_open_colour'],
            'flag_open_volume' => $this->bind['flag_open_volume'],
            'flag_button_add_before' => $this->bind['flag_button_add_before'],
            'flag_button_amend_before' => $this->bind['flag_button_amend_before'],
            'flag_button_send_approval_before' => $this->bind['flag_button_send_approval_before'],
            'flag_button_revise_before' => $this->bind['flag_button_revise_before'],
            'flag_button_planning_before' => $this->bind['flag_button_planning_before'],
            'flag_button_submit_before' => $this->bind['flag_button_submit_before'],
            'flag_button_approve_before' => $this->bind['flag_button_approve_before'],
            'flag_button_add_after' => $this->bind['flag_button_add_after'],
            'flag_button_amend_after' => $this->bind['flag_button_amend_after'],
            'flag_button_send_approval_after' => $this->bind['flag_button_send_approval_after'],
            'flag_button_revise_after' => $this->bind['flag_button_revise_after'],
            'flag_button_planning_after' => $this->bind['flag_button_planning_after'],
            'flag_button_submit_after' => $this->bind['flag_button_submit_after'],
            'flag_button_approve_after' => $this->bind['flag_button_submit_after'],
        );

        $where = array(
            'id_month' => $this->bind['id_month'],
            'month_id_to' => $this->bind['month_id_to'],
        );

        $count = $rangeMonthFixOrderRepository->findDuplicateEdit($where, $this->bind['id_range_rule']);

        if($count >= 1) {
            session()->flash('message_duplicate', '<div class="alert alert-warning">Rule Already Exists!</div>');
        } else {
            $update = $rangeMonthFixOrderRepository->update($this->bind['id_range_rule'], $data);

            if($update) {
                $this->isEdit = false;
                $this->resetForm();
                $this->deleteCache();
                $this->emit('closeModal');

                session()->flash('action_message', '<div class="alert alert-success">Update Data Success!</div>');
            } else {
                session()->flash('action_message', '<div class="alert alert-danger">Update Data Failed!</div>');
            }
        }
    }

    public function deleteProcess(RangeMonthFixOrderRepository $rangeMonthFixOrderRepository)
    {
        $delete = $rangeMonthFixOrderRepository->massDelete($this->checked);

        if($delete) {
            $this->resetForm();
            $this->checked = [];
            $this->deleteCache();
            $deleteStatus = 'success';
        } else {
            $deleteStatus = 'failed';
        }

        $this->emit('deleted', $deleteStatus);
    }
}
