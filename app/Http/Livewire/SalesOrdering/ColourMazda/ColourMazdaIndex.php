<?php

namespace App\Http\Livewire\SalesOrdering\ColourMazda;

use App\Repository\Eloquent\CacheRepository;
use App\Repository\Eloquent\ColourMazdaRepository;
use App\Traits\WithDeleteCache;
use App\Traits\WithPaginationAttribute;
use App\Traits\WithSorting;
use Illuminate\Support\Facades\Cache;
use Livewire\Component;
use Livewire\WithPagination;

class ColourMazdaIndex extends Component
{
    use WithPagination;
    use WithPaginationAttribute;
    use WithSorting;
    use WithDeleteCache;

    /**
     * Page Attributes
     */
    public string $pageTitle = "Colour Mazda";
    public bool $isEdit = false, $allChecked = false;
    public array $checked = [];

    protected $queryString = [
        'search' => ['except' => ''],
        'page' => ['except' => 1]
    ];

    public $bind = [
        'id_colour_mazda' => '0',
        'id_colour' => '',
        'name_colour' => '',
        'name_colour_global' => '',
        'flag_show_additional' => '',
        'flag_show_fix' => '',
    ];

    /**
     * Validation Attributes
     */
    protected $rules = [
        'bind.id_colour_mazda' => 'required',
        'bind.id_colour' => 'required',
        'bind.name_colour' => 'required|min:3|max:100',
        'bind.name_colour_global' => 'required|min:3|max:100',
        'bind.flag_show_additional' => 'required|in:0,1',
        'bind.flag_show_fix' => 'required|in:0,1',
    ];

    protected $messages = [
        'bind.id_colour_mazda.required' => 'Please fill ID Colour Mazda field!',
        'bind.id_colour.required' => 'Please fill ID Colour field!',
        'bind.name_colour.required' => 'Please fill Indonesian Name Colour field!',
        'bind.name_colour.min' => 'Please fill Indonesian Name Colour field with Minimal :min Characters',
        'bind.name_colour.max' => 'Please fill Indonesian Name Colour field with Maximal :max Characters',
        'bind.name_colour_global.required' => 'Please fill English Name Colour field!',
        'bind.name_colour_global.min' => 'Please fill English Name Colour field with Minimal :min Characters',
        'bind.name_colour_global.max' => 'Please fill English Name Colour field with Maximal :max Characters',
        'bind.flag_show_additional.required' => 'Please fill Flag Show Additional field!',
        'bind.flag_show_additional.in' => 'Please fill Flag Show Additional field with 0 or 1',
        'bind.flag_show_fix.required' => 'Please fill Flag Show Fix field!',
        'bind.flag_show_fix.in' => 'Please fill Flag Flag Show Fix field with 0 or 1',
    ];

    public function mount()
    {
        $this->sortBy = 'name_colour_global';
        $this->fill(request()->only('search', 'page'));
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function resetForm()
    {
        $this->reset(['bind']);
    }

    public function render(
        ColourMazdaRepository $colourMazdaRepository,
        CacheRepository $cacheRepository
    ) {
        $cache_name = 'colourmazda-menu-index-page-'.$this->page.'-pageselected-'.$this->perPageSelected.'-search-'.$this->search;
        $cache_name .= '-sortby-'.$this->sortBy.'-sortdirection-'.$this->sortDirection.'-user-'.session()->get('user')['id_user'];

        $dataColourMazda = Cache::remember($cache_name, 15, function () use($colourMazdaRepository, $cacheRepository, $cache_name) {
            $cacheRepository->firstOrCreate(['cache_name' => $cache_name, 'id_user' => session()->get('user')['id_user']]);
            return $colourMazdaRepository->pagination(
                $this->search,
                $this->sortBy,
                $this->sortDirection,
                $this->perPageSelected
            );
        });

        return view('livewire.sales-ordering.colour-mazda.colour-mazda-index', [
            'dataColourMazda' => $dataColourMazda
        ])->layout('layouts.app', ['title' => $this->pageTitle]);
    }

    public function allChecked(ColourMazdaRepository $colourMazdaRepository)
    {
        $datas = $colourMazdaRepository->pagination(
            $this->search,
            $this->sortBy,
            $this->sortDirection,
            $this->perPageSelected
        );

        $id = $colourMazdaRepository->getPrimaryKey();

        // Dari Unchecked ke Checked
        if($this->allChecked == true) {
            foreach($datas as $data) {
                if(!in_array($data->$id, $this->checked)) {
                    array_push($this->checked, (string) $data->$id);
                }
            }
        } else {
            // Checked ke Unchecked
            $this->checked = [];
        }
    }

    public function addForm()
    {
        $this->isEdit = false;
        $this->resetForm();
        $this->emit('openModal');
    }

    public function addProcess(ColourMazdaRepository $colourMazdaRepository)
    {
        $this->validate();

        $data = array(
            'id_colour' => $this->bind['id_colour'],
            'name_colour' => $this->bind['name_colour'],
            'name_colour_global' => $this->bind['name_colour_global'],
            'flag_show_additional' => $this->bind['flag_show_additional'],
            'flag_show_fix' => $this->bind['flag_show_fix'],
        );

        $where = array(
            'id_colour' => $this->bind['id_colour']
        );

        $count = $colourMazdaRepository->findDuplicate($where);

        if($count <= 0) {
            $insert = $colourMazdaRepository->create($data);

            if($insert) {
                $this->resetForm();
                $this->deleteCache();
                $this->emit('closeModal');

                session()->flash('action_message', '<div class="alert alert-success">Insert Data Success!</div>');
            } else {
                session()->flash('action_message', '<div class="alert alert-danger">Insert Data Failed!</div>');
            }
        } else {
            session()->flash('message_duplicate', '<div class="alert alert-warning"><strong>'.$this->bind['id_colour'].'</strong> Already Exists!</div>');
        }
    }

    public function editForm(ColourMazdaRepository $colourMazdaRepository)
    {
        $this->isEdit = true;

        $data = $colourMazdaRepository->getByID($this->checked[0]);
        $this->bind['id_colour_mazda'] = $data->id_colour_mazda;
        $this->bind['id_colour'] = $data->id_colour;
        $this->bind['name_colour'] = $data->name_colour;
        $this->bind['name_colour_global'] = $data->name_colour_global;
        $this->bind['flag_show_additional'] = $data->flag_show_additional;
        $this->bind['flag_show_fix'] = $data->flag_show_fix;

        $this->emit('openModal');
    }

    public function editProcess(ColourMazdaRepository $colourMazdaRepository)
    {
        $this->validate();

        $data = array(
            'id_colour' => $this->bind['id_colour'],
            'name_colour' => $this->bind['name_colour'],
            'name_colour_global' => $this->bind['name_colour_global'],
            'flag_show_additional' => $this->bind['flag_show_additional'],
            'flag_show_fix' => $this->bind['flag_show_fix'],
        );

        $where = array(
            'id_colour' => $this->bind['id_colour']
        );
        $count = $colourMazdaRepository->findDuplicateEdit($where, $this->bind['id_colour_mazda']);

        if($count >= 1) {
            session()->flash('message_duplicate', '<div class="alert alert-warning"><strong>'.$this->bind['id_colour'].'</strong> Already Exists!</div>');
        } else {
            $update = $colourMazdaRepository->update($this->bind['id_colour_mazda'], $data);

            if($update) {
                $this->isEdit = false;
                $this->resetForm();
                $this->deleteCache();
                $this->emit('closeModal');

                session()->flash('action_message', '<div class="alert alert-success">Update Data Success!</div>');
            } else {
                session()->flash('action_message', '<div class="alert alert-danger">Update Data Failed!</div>');
            }
        }
    }

    public function deleteProcess(ColourMazdaRepository $colourMazdaRepository)
    {
        $delete = $colourMazdaRepository->massDelete($this->checked);

        if($delete) {
            $this->resetForm();
            $this->checked = [];
            $this->deleteCache();
            $deleteStatus = 'success';
        } else {
            $deleteStatus = 'failed';
        }

        $this->emit('deleted', $deleteStatus);
    }
}
