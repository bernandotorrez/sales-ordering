<?php

namespace App\Http\Livewire\SalesOrdering\MasterMonth;

use App\Repository\Eloquent\CacheRepository;
use App\Repository\Eloquent\MasterMonthOrderRepository;
use App\Traits\WithDeleteCache;
use App\Traits\WithPaginationAttribute;
use App\Traits\WithSorting;
use Illuminate\Support\Facades\Cache;
use Livewire\Component;
use Livewire\WithPagination;

class MasterMonthIndex extends Component
{
    use WithPagination;
    use WithPaginationAttribute;
    use WithSorting;
    use WithDeleteCache;

    /**
     * Page Attributes
     */
    public string $pageTitle = "Master Month";
    public bool $isEdit = false, $allChecked = false;
    public array $checked = [];

    protected $queryString = [
        'search' => ['except' => ''],
        'page' => ['except' => 1]
    ];

    public $bind = [
        'id_month' => '',
        'month' => '',
        'date_input_lock_start' => '',
        'date_input_lock_end' => '',
        'operator_start' => '',
        'operator_end' => '',
    ];

    /**
     * Validation Attributes
     */
    protected $rules = [
        'bind.month' => 'required',
        'bind.date_input_lock_start' => 'required',
        'bind.date_input_lock_end' => 'required',
        'bind.operator_start' => 'required',
        'bind.operator_end' => 'required',
    ];

    protected $messages = [
        'bind.month.required' => 'Please fill Month field!',
        'bind.date_input_lock_start.required' => 'Please fill Date Lock Start field!',
        'bind.date_input_lock_end.required' => 'Please fill Date Lock End field!',
        'bind.operator_start.required' => 'Please fill Operator Start field!',
        'bind.operator_end.required' => 'Please fill Operator End field!',
    ];

    public function mount()
    {
        $this->sortBy = 'id_month';
        $this->fill(request()->only('search', 'page'));
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function resetForm()
    {
        $this->reset(['bind']);
    }

    public function render(
        MasterMonthOrderRepository $masterMonthOrderRepository,
        CacheRepository $cacheRepository
    ) {
        $cache_name = 'mastermonth-menu-index-page-'.$this->page.'-pageselected-'.$this->perPageSelected.'-search-'.$this->search;
        $cache_name .= '-sortby-'.$this->sortBy.'-sortdirection-'.$this->sortDirection.'-user-'.session()->get('user')['id_user'];

        $dataMasterMonth = Cache::remember($cache_name, 15, function () use($masterMonthOrderRepository, $cacheRepository, $cache_name) {
            $cacheRepository->firstOrCreate(['cache_name' => $cache_name, 'id_user' => session()->get('user')['id_user']]);
            return $masterMonthOrderRepository->pagination(
                $this->search,
                $this->sortBy,
                $this->sortDirection,
                $this->perPageSelected
            );
        });
        return view('livewire.sales-ordering.master-month.master-month-index', [
            'dataMasterMonth' => $dataMasterMonth
        ])
        ->layout('layouts.app', ['title' => $this->pageTitle]);
    }

    public function allChecked(MasterMonthOrderRepository $masterMonthOrderRepository)
    {
        $datas = $masterMonthOrderRepository->pagination(
            $this->search,
            $this->sortBy,
            $this->sortDirection,
            $this->perPageSelected
        );

        $id = $masterMonthOrderRepository->getPrimaryKey();

        // Dari Unchecked ke Checked
        if($this->allChecked == true) {
            foreach($datas as $data) {
                if(!in_array($data->$id, $this->checked)) {
                    array_push($this->checked, (string) $data->$id);
                }
            }
        } else {
            // Checked ke Unchecked
            $this->checked = [];
        }
    }

    public function addForm()
    {
        $this->isEdit = false;
        $this->resetForm();
        $this->emit('openModal');
    }

    public function addProcess(MasterMonthOrderRepository $masterMonthOrderRepository)
    {
        $this->validate();

        $data = array(
            'month' => $this->bind['month'],
            'date_input_lock_start' => date('Y-m-d', strtotime($this->bind['date_input_lock_start'])),
            'date_input_lock_end' => date('Y-m-d', strtotime($this->bind['date_input_lock_end'])),
            'operator_start' => $this->bind['operator_start'],
            'operator_end' => $this->bind['operator_end'],
        );

        $where = array(
            'month' => $this->bind['month'],
        );

        $count = $masterMonthOrderRepository->findDuplicate($where);

        if($count <= 0) {
            $insert = $masterMonthOrderRepository->create($data);

            if($insert) {
                $this->resetForm();
                $this->deleteCache();
                $this->emit('closeModal');

                session()->flash('action_message', '<div class="alert alert-success">Insert Data Success!</div>');
            } else {
                session()->flash('action_message', '<div class="alert alert-danger">Insert Data Failed!</div>');
            }
        } else {
            session()->flash('message_duplicate', '<div class="alert alert-warning"><strong>'.$this->bind['month'].'</strong> Already Exists!</div>');
        }
    }

    public function editForm(MasterMonthOrderRepository $masterMonthOrderRepository)
    {
        $this->isEdit = true;

        $data = $masterMonthOrderRepository->getByID($this->checked[0]);
        $this->bind['id_month'] = $data->id_month;
        $this->bind['month'] = $data->month;
        $this->bind['date_input_lock_start'] = $data->date_input_lock_start;
        $this->bind['date_input_lock_end'] = $data->date_input_lock_end;
        $this->bind['operator_start'] = $data->operator_start;
        $this->bind['operator_end'] = $data->operator_end;

        $this->emit('openModal');
    }

    public function editProcess(MasterMonthOrderRepository $masterMonthOrderRepository)
    {
        $this->validate();

        $data = array(
            'id_month' => $this->bind['id_month'],
            'month' => $this->bind['month'],
            'date_input_lock_start' => date('Y-m-d', strtotime($this->bind['date_input_lock_start'])),
            'date_input_lock_end' => date('Y-m-d', strtotime($this->bind['date_input_lock_end'])),
            'operator_start' => $this->bind['operator_start'],
            'operator_end' => $this->bind['operator_end'],
        );

        $where = array(
            'month' => $this->bind['month']
        );

        $count = $masterMonthOrderRepository->findDuplicateEdit($where, $this->bind['id_month']);

        if($count >= 1) {
            session()->flash('message_duplicate', '<div class="alert alert-warning"><strong>'.$this->bind['month'].'</strong> Already Exists!</div>');
        } else {
            $update = $masterMonthOrderRepository->update($this->bind['id_month'], $data);

            if($update) {
                $this->isEdit = false;
                $this->resetForm();
                $this->deleteCache();
                $this->emit('closeModal');

                session()->flash('action_message', '<div class="alert alert-success">Update Data Success!</div>');
            } else {
                session()->flash('action_message', '<div class="alert alert-danger">Update Data Failed!</div>');
            }
        }
    }

    public function deleteProcess(MasterMonthOrderRepository $masterMonthOrderRepository)
    {
        $delete = $masterMonthOrderRepository->massDelete($this->checked);

        if($delete) {
            $this->resetForm();
            $this->checked = [];
            $this->deleteCache();
            $deleteStatus = 'success';
        } else {
            $deleteStatus = 'failed';
        }

        $this->emit('deleted', $deleteStatus);
    }
}
