<?php

namespace App\Http\Livewire\SalesOrdering\MonthException;

use App\Repository\Eloquent\CacheRepository;
use App\Repository\Api\ApiDealerRepository;
use App\Repository\Eloquent\MasterMonthOrderRepository;
use App\Repository\Eloquent\MonthExceptionRuleRepository;
use App\Traits\WithDeleteCache;
use App\Traits\WithPaginationAttribute;
use App\Traits\WithSorting;
use Illuminate\Support\Facades\Cache;
use Livewire\Component;
use Livewire\WithPagination;

class MonthExceptionIndex extends Component
{
    use WithPagination;
    use WithPaginationAttribute;
    use WithSorting;
    use WithDeleteCache;


    /**
     * Page Attributes
     */
    public string $pageTitle = "Month Exception";
    public bool $isEdit = false, $allChecked = false;
    public array $checked = [];
    protected string $viewName = 'view_month_exception';

    protected $queryString = [
        'search' => ['except' => ''],
        'page' => ['except' => 1]
    ];

    public $bind = [
        'id_rule_month' => '',
        'id_month' => '',
        'month_id_to' => '',
        'id_dealer' => '',
        'date_input_lock_month_start' => '',
        'date_input_lock_month_end' => '',
        'id_dealer' => '',
        'flag_open_colour' => '',
        'flag_open_volume' => '',
    ];

    /**
     * Validation Attributes
     */
    protected $rules = [
        'bind.id_month' => 'required',
        'bind.month_id_to' => 'required',
        'bind.date_input_lock_month_start' => 'required',
        'bind.date_input_lock_month_end' => 'required',
        'bind.id_dealer' => 'required',
        'bind.flag_open_colour' => 'required|in:0,1',
        'bind.flag_open_volume' => 'required|in:0,1',
    ];

    protected $messages = [
        'bind.id_month.required' => 'Please fill Month field!',
        'bind.month_id_to.required' => 'Please fill Date Lock Start field!',
        'bind.date_input_lock_month_start.required' => 'Please fill Date Lock Start field!',
        'bind.date_input_lock_month_end.required' => 'Please fill Date Lock End field!',
        'bind.id_dealer.required' => 'Please fill Operator Start field!',
        'bind.flag_open_colour.required' => 'Please fill Flag Open Colour field!',
        'bind.flag_open_colour.in' => 'Please fill Flag Open Colour field with 0 or 1',
        'bind.flag_open_volume.required' => 'Please fill Flag Open Volume field!',
        'bind.flag_open_volume.in' => 'Please fill Flag Open Volume field with 0 or 1',
    ];

    public function mount()
    {
        $this->sortBy = 'id_rule_month';
        $this->fill(request()->only('search', 'page'));
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function resetForm()
    {
        $this->reset(['bind']);
    }

    public function render(
        MonthExceptionRuleRepository $monthExceptionRuleRepository,
        MasterMonthOrderRepository $masterMonthOrderRepository,
        ApiDealerRepository $apiDealerRepository,
        CacheRepository $cacheRepository
    ) {

        $monthExceptionCache = 'monthexception-menu-index-page-'.$this->page.'-pageselected-'.$this->perPageSelected.'-search-'.$this->search;
        $monthExceptionCache .= '-sortby-'.$this->sortBy.'-sortdirection-'.$this->sortDirection.'-user-'.session()->get('user')['id_user'];

        $dataMonthException = Cache::remember($monthExceptionCache, 15, function () use($monthExceptionRuleRepository, $cacheRepository, $monthExceptionCache) {
            $cacheRepository->firstOrCreate(['cache_name' => $monthExceptionCache, 'id_user' => session()->get('user')['id_user']]);
            return $monthExceptionRuleRepository->viewPagination(
                $this->viewName,
                $this->search,
                $this->sortBy,
                $this->sortDirection,
                $this->perPageSelected,
            );
        });

        $masterMonthCache = 'monthexception-masterMonth-index-page-'.$this->page.'-pageselected-'.$this->perPageSelected.'-search-'.$this->search;
        $masterMonthCache .= '-sortby-'.$this->sortBy.'-sortdirection-'.$this->sortDirection.'-user-'.session()->get('user')['id_user'];

        $dataMasterMonth = Cache::remember($masterMonthCache, 15, function () use($masterMonthOrderRepository, $cacheRepository, $masterMonthCache) {
            $cacheRepository->firstOrCreate(['cache_name' => $masterMonthCache, 'id_user' => session()->get('user')['id_user']]);
            return $masterMonthOrderRepository->allActive();
        });

        $apiDealerCache = 'monthexception-apiDealer-index-page-'.$this->page.'-pageselected-'.$this->perPageSelected.'-search-'.$this->search;
        $apiDealerCache .= '-sortby-'.$this->sortBy.'-sortdirection-'.$this->sortDirection.'-user-'.session()->get('user')['id_user'];

        $dataDealer = Cache::remember($apiDealerCache, 15, function () use($apiDealerRepository, $cacheRepository, $apiDealerCache) {
            $cacheRepository->firstOrCreate(['cache_name' => $apiDealerCache, 'id_user' => session()->get('user')['id_user']]);
            return $apiDealerRepository->all();
        });

        return view('livewire.sales-ordering.month-exception.month-exception-index', [
            'dataMonthException' => $dataMonthException,
            'dataMasterMonth' => $dataMasterMonth,
            'dataDealer' => $dataDealer['data']
        ])
        ->layout('layouts.app', ['title' => $this->pageTitle]);
    }

    public function allChecked(MonthExceptionRuleRepository $monthExceptionRuleRepository)
    {
        $datas = $monthExceptionRuleRepository->viewChecked(
            $this->viewName,
            $this->search,
            $this->sortBy,
            $this->sortDirection,
            $this->perPageSelected
        );

        $id = $monthExceptionRuleRepository->getPrimaryKey();

        // Dari Unchecked ke Checked
        if($this->allChecked == true) {
            foreach($datas as $data) {
                if(!in_array($data->$id, $this->checked)) {
                    array_push($this->checked, (string) $data->$id);
                }
            }
        } else {
            // Checked ke Unchecked
            $this->checked = [];
        }
    }

    public function addForm()
    {
        $this->isEdit = false;
        $this->resetForm();
        $this->emit('openModal');
    }

    public function addProcess(
        MonthExceptionRuleRepository $monthExceptionRuleRepository,
        ApiDealerRepository $apiDealerRepository
    ) {
        $this->validate();

        $data = array(
            'id_month' => $this->bind['id_month'],
            'month_id_to' => $this->bind['month_id_to'],
            'date_input_lock_month_start' => date('Y-m-d', strtotime($this->bind['date_input_lock_month_start'])),
            'date_input_lock_month_end' => date('Y-m-d', strtotime($this->bind['date_input_lock_month_end'])),
            'id_dealer' => (int) $this->bind['id_dealer'],
            'nama_dealer' => $this->getNamaDealer($this->bind['id_dealer'], $apiDealerRepository),
            'flag_open_colour' => $this->bind['flag_open_colour'],
            'flag_open_volume' => $this->bind['flag_open_volume'],
        );

        $where = array(
            'id_month' => $this->bind['id_month'],
            'month_id_to' => $this->bind['month_id_to'],
            'id_dealer' => $this->bind['id_dealer'],
        );

        $count = $monthExceptionRuleRepository->findDuplicate($where);

        if($count <= 0) {
            $insert = $monthExceptionRuleRepository->create($data);

            if($insert) {
                $this->resetForm();
                $this->deleteCache();
                $this->emit('closeModal');

                session()->flash('action_message', '<div class="alert alert-success">Insert Data Success!</div>');
            } else {
                session()->flash('action_message', '<div class="alert alert-danger">Insert Data Failed!</div>');
            }
        } else {
            session()->flash('message_duplicate', '<div class="alert alert-warning">Rule Already Exists!</div>');
        }
    }

    private function getNamaDealer($idDealer, $apiDealerRepository)
    {
        $dataDealer = $apiDealerRepository->getById($idDealer);

        if($idDealer != '0') {
            return $dataDealer['data']['nm_dealer'];
        } else {
            return 'All Dealer';
        }

    }

    public function editForm(MonthExceptionRuleRepository $monthExceptionRuleRepository)
    {
        $this->isEdit = true;

        $data = $monthExceptionRuleRepository->getByID($this->checked[0]);
        $this->bind['id_rule_month'] = $data->id_rule_month;
        $this->bind['id_month'] = $data->id_month;
        $this->bind['month_id_to'] = $data->month_id_to;
        $this->bind['date_input_lock_month_start'] = $data->date_input_lock_month_start;
        $this->bind['date_input_lock_month_end'] = $data->date_input_lock_month_end;
        $this->bind['id_dealer'] = $data->id_dealer;
        $this->bind['flag_open_colour'] = $data->flag_open_colour;
        $this->bind['flag_open_volume'] = $data->flag_open_volume;

        $this->emit('openModal');
    }

    public function editProcess(
        MonthExceptionRuleRepository $monthExceptionRuleRepository,
        ApiDealerRepository $apiDealerRepository
    ) {
        $this->validate();

        $data = array(
            'id_month' => $this->bind['id_month'],
            'month_id_to' => $this->bind['month_id_to'],
            'date_input_lock_month_start' => date('Y-m-d', strtotime($this->bind['date_input_lock_month_start'])),
            'date_input_lock_month_end' => date('Y-m-d', strtotime($this->bind['date_input_lock_month_end'])),
            'id_dealer' => (int) $this->bind['id_dealer'],
            'nama_dealer' => $this->getNamaDealer($this->bind['id_dealer'], $apiDealerRepository),
            'flag_open_colour' => $this->bind['flag_open_colour'],
            'flag_open_volume' => $this->bind['flag_open_volume'],
        );

        $where = array(
            'id_month' => $this->bind['id_month'],
            'month_id_to' => $this->bind['month_id_to'],
            'id_dealer' => $this->bind['id_dealer'],
        );

        $count = $monthExceptionRuleRepository->findDuplicateEdit($where, $this->bind['id_rule_month']);

        if($count >= 1) {
            session()->flash('message_duplicate', '<div class="alert alert-warning">Rule Already Exists!</div>');
        } else {
            $update = $monthExceptionRuleRepository->update($this->bind['id_rule_month'], $data);

            if($update) {
                $this->isEdit = false;
                $this->resetForm();
                $this->deleteCache();
                $this->emit('closeModal');

                session()->flash('action_message', '<div class="alert alert-success">Update Data Success!</div>');
            } else {
                session()->flash('action_message', '<div class="alert alert-danger">Update Data Failed!</div>');
            }
        }
    }

    public function deleteProcess(MonthExceptionRuleRepository $monthExceptionRuleRepository)
    {
        $delete = $monthExceptionRuleRepository->massDelete($this->checked);

        if($delete) {
            $this->resetForm();
            $this->checked = [];
            $this->deleteCache();
            $deleteStatus = 'success';
        } else {
            $deleteStatus = 'failed';
        }

        $this->emit('deleted', $deleteStatus);
    }
}
