<?php

namespace App\Http\Livewire\SalesOrdering\UserRole;

use App\Exports\UserRoleExport;
use App\Repository\Eloquent\CacheRepository;
use App\Repository\Eloquent\UserGroupRepository;
use App\Repository\Eloquent\UserRepository;
use App\Traits\WithDeleteCache;
use App\Traits\WithSorting;
use Livewire\Component;
use Livewire\WithPagination;
use App\Traits\WithPaginationAttribute;
use Illuminate\Support\Facades\Cache;
use Maatwebsite\Excel\Facades\Excel;

class UserRoleIndex extends Component
{
    use WithPagination;
    use WithPaginationAttribute;
    use WithSorting;
    use WithDeleteCache;

    /**
     * Page Attributes
     */
    public string $pageTitle = "User Role";
    public bool $isEdit = false, $allChecked = false;
    public array $checked = [];
    protected string $view = 'view_user';
    public $tableName = 'whitelist';

    protected $queryString = [
        'search' => ['except' => ''],
        'page' => ['except' => 1]
    ];

    public $remarkBlacklist = '';
    public $remarkWhitelist = '';

    public $bindReport = [
        'status' => 'all'
    ];

    public function mount()
    {
        $this->sortBy = 'username';
        $this->fill(request()->only('search', 'page'));
    }

    public function updated($propertyName)
    {
        //$this->validateOnly($propertyName);
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function resetForm()
    {
        $this->reset(['bind']);
    }

    public function render(
        UserRepository $userRepository,
        UserGroupRepository $userGroupRepository,
        CacheRepository $cacheRepository
    ) {
        $cache_name = 'userRole-index-page-'.$this->page.'-pageselected-'.$this->perPageSelected.'-search-'.$this->search;
        $cache_name .= '-sortby-'.$this->sortBy.'-sortdirection-'.$this->sortDirection.'-user-'.session()->get('user')['id_user'];
        $cache_name .= '-tableName-'.$this->tableName;

        $dataUser = $userRepository->viewPaginationdealerBM(
            $this->view,
            $this->search,
            $this->sortBy,
            $this->sortDirection,
            $this->perPageSelected,
            $this->tableName
        );

        return view('livewire.sales-ordering.user-role.index',
        [
            'dataUser' => $dataUser,
            'dataUserGroup' => $userGroupRepository->allActive()
        ])->layout('layouts.app', ['title' => $this->pageTitle]);
    }

    public function allChecked(UserRepository $userRepository)
    {
        $datas = $userRepository->viewChecked(
            $this->view,
            $this->search,
            $this->sortBy,
            $this->sortDirection,
            $this->perPageSelected
        );

        $id = $userRepository->getPrimaryKey();

        // Dari Unchecked ke Checked
        if($this->allChecked == true) {
            foreach($datas as $data) {
                if(!in_array($data->$id, $this->checked)) {
                    array_push($this->checked, (string) $data->$id);
                }
            }
        } else {
            // Checked ke Unchecked
            $this->checked = [];
        }
    }

    public function blacklist(UserRepository $userRepository)
    {
        $update = $userRepository->blacklist($this->checked, $this->remarkBlacklist);

        if($update) {
            $this->checked = [];
            $this->deleteCache();
            session()->flash('action_message', '<div class="alert alert-success">Blacklist Success!</div>');
        }
    }

    public function whitelist(UserRepository $userRepository)
    {
        $update = $userRepository->whitelist($this->checked, $this->remarkWhitelist);

        if($update) {
            $this->checked = [];
            $this->deleteCache();
            session()->flash('action_message', '<div class="alert alert-success">Whitelist Success!</div>');
        }
    }

    public function exportExcel()
    {
        $excelName = 'user_role.xlsx';
        return Excel::download(new UserRoleExport($this->bindReport['status']), $excelName);
    }

}
