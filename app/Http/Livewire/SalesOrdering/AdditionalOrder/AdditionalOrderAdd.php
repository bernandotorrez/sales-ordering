<?php

namespace App\Http\Livewire\SalesOrdering\AdditionalOrder;

use App\Repository\Api\ApiColorRepository;
use App\Repository\Api\ApiModelColorRepository;
use App\Repository\Api\ApiModelRepository;
use App\Repository\Api\ApiTypeModelRepository;
use App\Repository\Eloquent\ColourMazdaRepository;
use App\Repository\Eloquent\MasterAdditionalOrderRepository;
use App\Repository\Eloquent\MazdaModelRepository;
use App\Repository\Eloquent\ModelColourMazdaRepository;
use App\Repository\Eloquent\TypeModelMazdaRepository;
use App\Traits\WithDeleteCache;
use App\Traits\WithGoTo;
use App\Traits\WithWrsApi;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Livewire\Component;

class AdditionalOrderAdd extends Component
{
    use WithWrsApi;
    use WithGoTo;
    use WithDeleteCache;

    public $pageTitle = 'Additional Order - Add';
    public array $detailData = [];
    public $totalQty = 0;
    public $foo = 0;
    public $second = 0;
    public $third = 0;

    public array $bind = [
        'order_number_dealer' => ''
    ];

    protected $rules = [
        'bind.order_number_dealer' => 'required|min:1|max:100',
        'detailData.*.id_model' => 'required',
        'detailData.*.id_type' => 'required',
        'detailData.*.id_colour' => 'required',
        'detailData.*.qty' => 'required|numeric|min:1|max:99999',
        'detailData.*.year_production' => 'required',
    ];

    protected $messages = [
        'bind.order_number_dealer.required' => 'Please fill Order Number Dealer',
        'bind.order_number_dealer.min' => 'Please fill Order Number Minimal :min Character',
        'bind.order_number_dealer.max' => 'Please fill Order Number Maximal :max Characters',
        'detailData.*.id_model.required' => 'Please Choose Model Name!',
        'detailData.*.id_type.required' => 'Please Choose Type Name!',
        'detailData.*.id_colour.required' => 'Please Choose Colour!',
        'detailData.*.qty.required' => 'Quantity cant be Empty!',
        'detailData.*.qty.min' => 'Please input Quantity at Least :min',
        'detailData.*.qty.max' => 'Please input Quantity at Max :max',
        'detailData.*.year_production.required' => 'Please Choose Year Production!',
    ];

    public function mount()
    {
        $data = array(
            'id_model' => '',
            'model_name' => '',
            'id_type' => '',
            'type_name' => '',
            'id_colour' => '',
            'colour_name' => '',
            'qty' => 0,
            'year_production' => '',
            'data_type' => [],
            'data_colour' => []
        );

        array_push($this->detailData, $data);
    }

    public function addDetail()
    {
        $data = array(
            'id_model' => '',
            'model_name' => '',
            'id_type' => '',
            'type_name' => '',
            'id_colour' => '',
            'colour_name' => '',
            'qty' => 0,
            'year_production' => '',
            'data_type' => [],
            'data_colour' => []
        );

        array_push($this->detailData, $data);
    }

    public function deleteDetail($key)
    {
        unset($this->detailData[$key]);
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
        $this->sumTotalQty();
    }

    private function sumTotalQty()
    {
        $totalQty = 0;
        foreach($this->detailData as $key => $detailData)
        {
            $totalQty += $this->detailData[$key]['qty'] ? $this->detailData[$key]['qty'] : 0;
        }

        $this->totalQty = $totalQty;
    }

    public function updateDataType($key, $value,
        TypeModelMazdaRepository $typeModelMazdaRepository,
        ModelColourMazdaRepository $modelColourMazdaRepository
    ) {
        if($this->detailData[$key]['id_model'] != '') {
            $dataType = Cache::remember('data-type-with-id-model-'.$value, 30, function () use($value, $typeModelMazdaRepository) {
                return $typeModelMazdaRepository->getByIdModelAdditional($value);
            });
            $dataType = json_decode($dataType, true);

            $this->detailData[$key]['data_type'] = $dataType ? $dataType : [];
        }

        $this->updateDataColour($key, $value, $modelColourMazdaRepository);

    }

    public function updateDataColour($key, $value, $modelColourMazdaRepository)
    {
        if($this->detailData[$key]['id_model'] != '') {
            $dataColor = Cache::remember('data-color-with-id-model-'.$value, 30, function () use($value, $modelColourMazdaRepository) {
                return $modelColourMazdaRepository->getByIdModelAdditional($value);
            });
            $dataColor = json_decode($dataColor, true);

            $this->detailData[$key]['data_colour'] = $dataColor ? $dataColor : [];
        }

    }

    public function render(MazdaModelRepository $mazdaModelRepository)
    {
        $dealerName = session()->get('dealer')['nm_dealer'] ? session()->get('dealer')['nm_dealer'] : 'Admin';

        $dataModel = Cache::remember('data-model', 30, function () use($mazdaModelRepository) {
            return $mazdaModelRepository->getAvailableModelAdditional()->toArray();
        });

        return view('livewire.sales-ordering.additional-order.additional-order-add', [
            'dealerName' => $dealerName,
            'dataModel' => $dataModel,
        ])
        ->layout('layouts.app', ['title' => $this->pageTitle]);
    }

    public function addProcess(
        MasterAdditionalOrderRepository $masterAdditionalOrderRepository,
        MazdaModelRepository $mazdaModelRepository,
        TypeModelMazdaRepository $typeModelMazdaRepository,
        ColourMazdaRepository $colourMazdaRepository
    ) {
        $this->validate();

        $dataMaster = array(
            'no_order_atpm' => '',
            'no_order_dealer' => $this->bind['order_number_dealer'],
            'date_save_order' => Carbon::now(),
            'id_dealer' => session()->get('user')['id_dealer'],
            'nama_dealer' => session()->get('dealer')['nm_dealer'],
            'id_user' => session()->get('user')['id_user'],
            'user_order' => session()->get('user')['nama_user'],
            'month_order' => Carbon::now()->month,
            'year_order' => Carbon::now()->year,
            'total_qty' => $this->totalQty,
            'email_user_order' => session()->get('user')['email'],
            'status' => '1'
        );

        $this->updateModelTypeColour($mazdaModelRepository, $typeModelMazdaRepository, $colourMazdaRepository);

        $where = array('no_order_dealer' => $this->bind['order_number_dealer']);

        $count = $masterAdditionalOrderRepository->findDuplicate($where);

        if($count > 0) {
            session()->flash('action_message',
            '<div class="alert alert-warning" role="alert">No Order Dealer : <strong>'.$this->bind['order_number_dealer'].'</strong> is Exists!</div>');
        } else {
            $checkDuplicate = $this->checkDuplicate();

            if($checkDuplicate) {
                session()->flash('action_message_detail', '<div class="alert alert-warning" role="alert">
                <strong>Model, Year Prod, Type, Colour are Duplicated!</strong> <p> Please check your Order, ensure if your Order with no Duplicate! </p></div>');
            } else {
                $insert = $masterAdditionalOrderRepository->createDealerOrder($dataMaster, $this->detailData);

                if($insert) {
                    $this->deleteCache();
                    session()->flash('action_message', '<div class="alert alert-primary" role="alert">Insert Data Success!</div>');
                    return redirect()->to(route('additional-order.index'));
                } else {
                    session()->flash('action_message', '<div class="alert alert-danger" role="alert">Insert Data Failed!</div>');
                }
            }

        }

    }

    /**
     * Check Duplicated Model, Year Prod, Type, Colour in Additional Order
     * @author Bernand
     * @return bool true | false
     */
    private function checkDuplicate()
    {
        $arrayModel = array_column($this->detailData, 'id_model');
        $arrayYear = array_column($this->detailData, 'year_production');
        $arrayType = array_column($this->detailData, 'id_type');
        $arrayColour = array_column($this->detailData, 'id_colour');

        $checkDuplicate = $arrayModel != array_unique($arrayModel) && $arrayYear != array_unique($arrayYear)
        && $arrayType != array_unique($arrayType) && $arrayColour != array_unique($arrayColour);

        return $checkDuplicate;
    }

    private function updateModelTypeColour($mazdaModelRepository, $typeModelMazdaRepository, $colourMazdaRepository)
    {
        foreach($this->detailData as $key => $detailData) {
            // Model Name
            $cacheModel = 'data-model-getById-'.$this->detailData[$key]['id_model'];
            $dataModel = Cache::remember($cacheModel, 10, function () use($mazdaModelRepository, $key) {
                return $mazdaModelRepository->getByIdModel($this->detailData[$key]['id_model'])->toArray();
            });

            //$dataModel = $apiModelRepository->getById($this->detailData[$key]['id_model']);
            $this->detailData[$key]['model_name'] = $dataModel['name_model'];

            // Type Model Name
            $cacheType = 'data-type-model-getById-'.$this->detailData[$key]['id_type'];
            $dataTypeModel = Cache::remember($cacheType, 10, function () use($typeModelMazdaRepository, $key) {
                return $typeModelMazdaRepository->geByIdTypeModel($this->detailData[$key]['id_type'])->toArray();
            });

            //$dataTypeModel = $apiTypeModelRepository->getById($this->detailData[$key]['id_type']);

            $this->detailData[$key]['type_name'] = $dataTypeModel['name_type'].' - '.$dataTypeModel['msc_code'];

            // Model Colour
            $cacheModelColor = 'data-model-color-getById-'.$this->detailData[$key]['id_colour'];
            $dataModelColour = Cache::remember($cacheModelColor, 10, function () use($colourMazdaRepository, $key) {
                return $colourMazdaRepository->getByIdColour($this->detailData[$key]['id_colour'])->toArray();
            });
            //$dataModelColour = $apiColorRepository->getById($this->detailData[$key]['id_colour']);

            $this->detailData[$key]['colour_name'] = $dataModelColour['name_colour_global'];

        }
    }
}
