<?php

namespace App\Http\Livewire\SalesOrdering\ApprovedBM;

use App\Repository\Eloquent\CancelStatusRepository;
use Livewire\Component;
use App\Traits\WithGoTo;

class ApprovedBMIndex extends Component
{

    use WithGoTo;

    public function render(CancelStatusRepository $cancelStatusRepository)
    {
        $dataCancelStatus = $cancelStatusRepository->allActive();
        return view('livewire.sales-ordering.approved-b-m.approved-b-m-index', [
            'dataCancelStatus' => $dataCancelStatus
        ])->layout('layouts.app', ['title' => 'Approved BM']);
    }
}
