<?php

namespace App\Http\Livewire\SalesOrdering\TypeModelMazda;

use App\Repository\Api\ApiTypeModelRepository;
use App\Repository\Eloquent\CacheRepository;
use App\Repository\Eloquent\MazdaModelRepository;
use App\Repository\Eloquent\TypeModelMazdaRepository;
use App\Traits\WithDeleteCache;
use App\Traits\WithPaginationAttribute;
use App\Traits\WithSorting;
use Illuminate\Support\Facades\Cache;
use Livewire\Component;
use Livewire\WithPagination;

class TypeModelMazdaIndex extends Component
{
    use WithPagination;
    use WithPaginationAttribute;
    use WithSorting;
    use WithDeleteCache;

    /**
     * Page Attributes
     */
    public string $pageTitle = "Type Model Mazda";
    public bool $isEdit = false, $allChecked = false;
    public array $checked = [];
    protected string $viewName = 'view_model_type_mazda';

    protected $queryString = [
        'search' => ['except' => ''],
        'page' => ['except' => 1]
    ];

    public $bind = [
        'id_type_model_mazda' => '0',
        'id_type_model' => '',
        'id_model' => '',
        'name_model' => '',
        'name_type' => '',
        'flag_show_additional' => '',
        'flag_show_fix' => '',
    ];

    /**
     * Validation Attributes
     */
    protected $rules = [
        'bind.id_type_model_mazda' => 'required',
        'bind.id_type_model' => 'required',
        'bind.id_model' => 'required',
        'bind.name_type' => 'required|min:3|max:100',
        'bind.flag_show_additional' => 'required|in:0,1',
        'bind.flag_show_fix' => 'required|in:0,1',
    ];

    protected $messages = [
        'bind.id_type_model_mazda.required' => 'Please fill ID Type Model Mazda field!',
        'bind.id_type_model.required' => 'Please fill ID Type Model field!',
        'bind.id_model.required' => 'Please fill ID Model field!',
        'bind.name_type.required' => 'Please fill Model Mazda field!',
        'bind.name_type.min' => 'Please fill Model Mazda field with Minimal :min Characters',
        'bind.name_type.max' => 'Please fill Model Mazda field with Maximal :max Characters',
        'bind.flag_show_additional.required' => 'Please fill Flag Show Additional field!',
        'bind.flag_show_additional.in' => 'Please fill Flag Show Additional field with 0 or 1',
        'bind.flag_show_fix.required' => 'Please fill Flag Show Fix field!',
        'bind.flag_show_fix.in' => 'Please fill Flag Flag Show Fix field with 0 or 1',
    ];

    public function mount()
    {
        $this->sortBy = 'id_type_model';
        $this->fill(request()->only('search', 'page'));
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function resetForm()
    {
        $this->reset(['bind']);
    }

    public function render(
        TypeModelMazdaRepository $typeModelMazdaRepository,
        MazdaModelRepository $mazdaModelRepository,
        CacheRepository $cacheRepository
    ) {
        $dataModelMazda = $mazdaModelRepository->getAvailableModel();

        $mazdaTypeModelCache = 'typemazdamodel-menu-index-page-'.$this->page.'-pageselected-'.$this->perPageSelected.'-search-'.$this->search;
        $mazdaTypeModelCache .= '-sortby-'.$this->sortBy.'-sortdirection-'.$this->sortDirection.'-user-'.session()->get('user')['id_user'];

        $dataTypeModelMazda = Cache::remember($mazdaTypeModelCache, 15, function () use($typeModelMazdaRepository, $cacheRepository, $mazdaTypeModelCache) {
            $cacheRepository->firstOrCreate(['cache_name' => $mazdaTypeModelCache, 'id_user' => session()->get('user')['id_user']]);

            return $typeModelMazdaRepository->getAvailableTypeModelPagination(
                $this->viewName,
                $this->search,
                $this->sortBy,
                $this->sortDirection,
                $this->perPageSelected
            );
        });

        return view('livewire.sales-ordering.type-model-mazda.type-model-mazda-index', [
            'dataTypeModelMazda' => $dataTypeModelMazda,
            'dataModelMazda' => $dataModelMazda
        ])
        ->layout('layouts.app', ['title' => $this->pageTitle]);
    }

    public function allChecked(TypeModelMazdaRepository $typeModelMazdaRepository)
    {
        $datas = $typeModelMazdaRepository->getAvailableTypeModelChecked(
            $this->viewName,
            $this->search,
            $this->sortBy,
            $this->sortDirection,
            $this->perPageSelected
        );

        $id = $typeModelMazdaRepository->getPrimaryKey();

        // Dari Unchecked ke Checked
        if($this->allChecked == true) {
            foreach($datas as $data) {
                if(!in_array($data->$id, $this->checked)) {
                    array_push($this->checked, (string) $data->$id);
                }
            }
        } else {
            // Checked ke Unchecked
            $this->checked = [];
        }
    }

    public function addForm()
    {
        $this->isEdit = false;
        $this->resetForm();
        $this->emit('openModal');
    }

    public function addProcess(TypeModelMazdaRepository $typeModelMazdaRepository)
    {
        $this->validate();

        $data = array(
            'id_type_model' => $this->bind['id_type_model'],
            'id_model' => $this->bind['id_model'],
            'name_type' => $this->bind['name_type'],
            'flag_show_additional' => $this->bind['flag_show_additional'],
            'flag_show_fix' => $this->bind['flag_show_fix'],
        );

        $where = array(
            'id_model' => $this->bind['id_model'],
            'name_type' => $this->bind['name_type'],
        );

        $count = $typeModelMazdaRepository->findDuplicate($where);

        if($count <= 0) {
            $insert = $typeModelMazdaRepository->create($data);

            if($insert) {
                $this->resetForm();
                $this->deleteCache();
                $this->emit('closeModal');

                session()->flash('action_message', '<div class="alert alert-success">Insert Data Success!</div>');
            } else {
                session()->flash('action_message', '<div class="alert alert-danger">Insert Data Failed!</div>');
            }
        } else {
            session()->flash('message_duplicate', '<div class="alert alert-warning"><strong>'.$this->bind['name_type'].'</strong> Already Exists!</div>');
        }
    }

    public function editForm(
        TypeModelMazdaRepository $typeModelMazdaRepository,
        MazdaModelRepository $mazdaModelRepository
    ) {
        $this->isEdit = true;

        $data = $typeModelMazdaRepository->getByID($this->checked[0]);
        $this->bind['id_type_model_mazda'] = $data->id_type_model_mazda;
        $this->bind['id_type_model'] = $data->id_type_model;
        $this->bind['id_model'] = $data->id_model;
        $this->bind['name_type'] = $data->name_type;
        $this->bind['flag_show_additional'] = $data->flag_show_additional;
        $this->bind['flag_show_fix'] = $data->flag_show_fix;

        $dataModel = $mazdaModelRepository->getByIdModel($data->id_model);
        $this->bind['name_model'] = $dataModel->name_model;

        $this->emit('openModal');
    }

    public function editProcess(TypeModelMazdaRepository $typeModelMazdaRepository)
    {
        $this->validate();

        $data = array(
            'id_type_model_mazda' => $this->bind['id_type_model_mazda'],
            'id_type_model' => $this->bind['id_type_model'],
            'id_model' => $this->bind['id_model'],
            'name_type' => $this->bind['name_type'],
            'flag_show_additional' => $this->bind['flag_show_additional'],
            'flag_show_fix' => $this->bind['flag_show_fix'],
        );

        $where = array(
            'id_model' => $this->bind['id_model'],
            'name_type' => $this->bind['name_type'],
        );
        $count = $typeModelMazdaRepository->findDuplicateEdit($where, $this->bind['id_type_model_mazda']);

        if($count >= 1) {
            session()->flash('message_duplicate', '<div class="alert alert-warning"><strong>'.$this->bind['name_type'].'</strong> Already Exists!</div>');
        } else {
            $update = $typeModelMazdaRepository->update($this->bind['id_type_model_mazda'], $data);

            if($update) {
                $this->isEdit = false;
                $this->resetForm();
                $this->deleteCache();
                $this->emit('closeModal');

                session()->flash('action_message', '<div class="alert alert-success">Update Data Success!</div>');
            } else {
                session()->flash('action_message', '<div class="alert alert-danger">Update Data Failed!</div>');
            }
        }
    }

    public function deleteProcess(TypeModelMazdaRepository $typeModelMazdaRepository)
    {
        $delete = $typeModelMazdaRepository->massDelete($this->checked);

        if($delete) {
            $this->resetForm();
            $this->checked = [];
            $this->deleteCache();
            $deleteStatus = 'success';
        } else {
            $deleteStatus = 'failed';
        }

        $this->emit('deleted', $deleteStatus);
    }

    public function syncFromWRS(
        ApiTypeModelRepository $apiTypeModelRepository,
        MazdaModelRepository $mazdaModelRepository,
        TypeModelMazdaRepository $typeModelMazdaRepository,
        CacheRepository $cacheRepository
    ) {
        $dataTypeModel = $apiTypeModelRepository->all();

        foreach($dataTypeModel['data'] as $typeModel) {
            $checkDuplicate = Cache::remember('syncFromWrs-TypeModel-kd_type-'.$typeModel['kd_type'], 30, function () use($typeModelMazdaRepository, $typeModel) {
                $where = array(
                    'id_type_model' => $typeModel['kd_type']
                );
                return $typeModelMazdaRepository->findDuplicate($where);
            });

            if($checkDuplicate == '0') {
                $data = array(
                    'id_type_model' => $typeModel['kd_type'],
                    'id_model' => $typeModel['fk_model'],
                    'name_type' => $typeModel['nm_type'],
                    'cylinder' => $typeModel['isi_silinder'],
                    'msc_code' => $typeModel['msc_code'],
                    'price' => $typeModel['harga'],
                    'tpt' => $typeModel['tpt'],
                    'sut' => $typeModel['sut'],
                    'flag_show_additional' => '1',
                    'flag_show_fix' => '1',
                    'status' => '1'
                );
                $insert = $typeModelMazdaRepository->create($data);
            } else {
                $data = array(
                    'id_type_model' => $typeModel['kd_type'],
                    'id_model' => $typeModel['fk_model'],
                    'name_type' => $typeModel['nm_type'],
                    'cylinder' => $typeModel['isi_silinder'],
                    'msc_code' => $typeModel['msc_code'],
                    'price' => $typeModel['harga'],
                    'tpt' => $typeModel['tpt'],
                    'sut' => $typeModel['sut'],
                    'flag_show_additional' => '1',
                    'flag_show_fix' => '1',
                    'status' => '1'
                );
                $update = $typeModelMazdaRepository->update($data['id_type_model'], $data);
            }
        }

        $this->deleteCache();
        $this->render($typeModelMazdaRepository, $mazdaModelRepository, $cacheRepository);
    }
}
