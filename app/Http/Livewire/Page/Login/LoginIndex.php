<?php

namespace App\Http\Livewire\Page\Login;

use App\Models\User;
use App\Repository\Api\ApiAtpmUserRepository;
use App\Repository\Api\ApiDealerUserRepository;
use App\Repository\Eloquent\UserRepository;
use App\Traits\WithWrsApi;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Illuminate\Support\Facades\Http;

class LoginIndex extends Component
{

    use WithWrsApi;

    protected $pageTitle = 'Login Into DMS';
    public $username, $password, $loginAs;

    protected $rules = [
        'username'  => 'required|min:3|max:50',
        'password'  => 'required|min:3|max:50',
        'loginAs'   => 'required'
    ];

    protected $messages = [
        'username.required' => 'Please fill the Username Field',
        'username.min'      => 'Please fill the Username Field with Minimal 3 Characters',
        'username.max'      => 'Please fill the Username Field with Maximal 50 Characters',
        'password.required' => 'Please fill the Password Field',
        'password.min'      => 'Please fill the Password Field with Minimal 3 Characters',
        'password.max'      => 'Please fill the Username Field with Maximal 50 Characters',
        'loginAs.required'  => 'Please choose the Login As Field',
    ];

    public function render()
    {
        $data = array('title' => $this->pageTitle);
        return view('livewire.page.login.login-index')->layout('layouts.app', $data);
    }

    public function mount()
    {
        $this->resetForm();
    }

    public function resetForm()
    {
        $this->reset(['username', 'password', 'loginAs']);
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function login(
        ApiDealerUserRepository $apiDealerUserRepository,
        ApiAtpmUserRepository $apiAtpmUserRepository
    ) {
        $this->validate();

        // Start Check if Local Admin
        //$admin = User::whereIn('level_access', ['1', '2'])->get(['username']);
        $admin = User::where('level_access', '1')->get(['username']);
        $listAdmin = array();

        foreach($admin as $data) {
            array_push($listAdmin, $data->username);
        }

        if(in_array($this->username, $listAdmin)) {
            $login = User::where(['username' => $this->username, 'password' => md5($this->password)])->first();

            $response = $apiAtpmUserRepository->login('tampan', 't@mp@n');
            $token = $response->header('X-Auth-Token');

            if(!$login) {
                session()->flash('login_failed', 'Username or Password is wrong!');
            } else {
                session([
                    'user' => $login->toArray(),
                    'dealer' => array('nm_dealer' => 'Admin'),
                    'level_access' => $login->level_access,
                    'token' => $token
                ]);
                return redirect(url('home'));
            }
        // End Check if Local Admin
        } else {
            if($this->loginAs == 'atpm') {
                $response = $apiAtpmUserRepository->login($this->username, $this->password);

                $status_atpm = 'atpm';
                $dataDealer = array('nm_dealer' => 'ATPM');
            } else if($this->loginAs == 'dealer') {
                $response = $apiDealerUserRepository->login($this->username, $this->password);

                $status_atpm = 'dealer';
                $dataDealer = ($response['message'] == 'success') ? $response['data']['dealer'] : array('nm_dealer' => 'Dealer');
            } else if($this->loginAs == 'atpm-aftersales') {
                $response = $apiAtpmUserRepository->loginAfterSales($this->username, $this->password);

                $status_atpm = 'atpm';
                $dataDealer = array('nm_dealer' => 'ATPM');
            } else if($this->loginAs == 'dealer-aftersales') {
                $response = $apiDealerUserRepository->loginAfterSales($this->username, $this->password);

                $status_atpm = 'dealer';
                $dataDealer = ($response['message'] == 'success') ? $response['data']['dealer'] : array('nm_dealer' => 'Dealer');
            }

            //dd($response);

            $token = $response->header('X-Auth-Token');

            if($response['message'] != 'success') {
                session()->flash('login_failed', 'Username or Password is wrong!');
            } else {
                $login = User::where(['username' => $this->username])->count();

                if($login == 0) {
                    if($status_atpm == 'atpm') {
                        User::create([
                            'kd_user_wrs' => $response['data']['kd_atpm_user'],
                            'nama_user' => $response['data']['nm_atpm_user'],
                            'username' => $response['data']['username'],
                            'email' => $response['data']['email'],
                            'id_user_group' => 2,
                            'status_atpm' => 'atpm',
                            'is_from_wrs' => '1'
                        ]);
                    } else {
                        if($response['data']['fk_dealer_level'] == 'BM') {
                            $idUserGroup = 5;
                        } else if($response['data']['fk_dealer_level'] == 'Adm') {
                            $idUserGroup = 4;
                        } else {
                            $idUserGroup = 3;
                        }
                        User::create([
                            'kd_user_wrs' => $response['data']['kd_dealer_user'],
                            'nama_user' => $response['data']['nm_dealer_user'],
                            'username' => $response['data']['username'],
                            'email' => $response['data']['email'],
                            'id_user_group' => $idUserGroup,
                            'id_dealer' => $response['data']['fk_dealer'],
                            'id_dealer_level' => $response['data']['fk_dealer_level'],
                            'status_atpm' => 'dealer',
                            'is_from_wrs' => '1'
                        ]);
                    }
                }

                $loginData = User::where(['username' => $this->username])->first();

                if($loginData->flag_blacklist == 1) {
                    session()->flash('login_failed', 'This User has been Blacklisted!');
                } else {
                    session([
                        'user' => $loginData->toArray(),
                        'dealer' => $dataDealer,
                        'level_access' => $loginData->level_access,
                        'token' => $token,
                        'area_manager' => $response['data']['area_manager'] ?? '',
                        'atpm_level' => $response['data']['atpm_level']['nm_atpm_level'] ?? '',

                    ]);
                    return redirect(url('home'));
                }


            }
        }



    }
}
