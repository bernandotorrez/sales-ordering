<?php

namespace App\Http\Livewire\Page\Artisan;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Livewire\Component;

class ArtisanIndex extends Component
{
    public $pageTitle = 'Artisan';

    public function render()
    {
        return view('livewire.page.artisan.artisan-index')->layout('layouts.app');
    }

    public function configCache()
    {
        Config::set('constants.api_token', session()->get('token'));
        return Artisan::call('config:cache');
    }

    public function cacheClear()
    {
        return Artisan::call('cache:clear');
    }

    public function migrateFresh()
    {
        Config::set('constants.api_token', session()->get('token'));

        return Artisan::call('migrate:fresh');
    }

    public function changeAPIDev()
    {
        Config::set('constants.wrs_api', 'http://172.17.0.149/lumensphp/api/v1');
        Config::set('constants.wrs_aftersales_api', 'http://172.17.0.149/lumensphp/aftersales/api/v1');

        return true;
    }
}
