<?php

namespace App\Http\Livewire\AfterSales\Warranty\Convert;

use App\Imports\ConvertExcelImport;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;
use Maatwebsite\Excel\Facades\Excel;

class ConvertIndex extends Component
{
    use WithFileUploads;

    public $file;
    public $storageFile;
    public $resultConvert;
    public string $pageTitle = "User";

    public function render() {
        return view('livewire.after-sales.warranty.convert.convert-index')
        ->layout('layouts.app', ['title' => $this->pageTitle]);
    }

    public function downloadTemplate()
    {
        $documentLocation = storage_path();
        $documentName = 'sample_data.xlsx';

        $mime = mime_content_type($documentLocation.'/'.$documentName);
        $headers = array(
            "Content-Type: $mime",
            );
        return response()->download($documentLocation.'/'.$documentName, $documentName, $headers);
    }

    public function downloadResult()
    {
        $documentLocation = storage_path('app/result_convert');
        $documentName = 'result_convert.txt';

        $mime = mime_content_type($documentLocation.'/'.$documentName);
        $headers = array(
            "Content-Type: $mime",
            );
        return response()->download($documentLocation.'/'.$documentName, $documentName, $headers);
    }

    public function save()
    {
        $this->validate([
            'file' => 'required|mimes:xls,xlsx|max:100000', // 1MB Max
        ]);

        $this->storageFile = $this->file->store('upload_convert_excel');

        $this->resultConvert = Excel::toArray(new ConvertExcelImport, $this->storageFile);

        $this->writeFile($this->resultConvert);
    }

    public function writeFile($data) {
        $perArray = ['A3010', 'A3070', 'A3080', 'A3090'];
        $lengthLimit = 591;

        if(!is_dir(storage_path('app/result_convert'))) {
            mkdir(storage_path('app/result_convert'));
        }

        $myfile = fopen(storage_path('app/result_convert/result_convert.txt'), "w") or die("Unable to open file!");

        foreach($data[0] as $row) {
            foreach($perArray as $column) {

                $fileIdentifier = $column;

                if($fileIdentifier == 'A3010') {
                    $claimId = str_pad('', 14);
                    $distributorCode = trim($row['distributor_code']);
                    $dealerCode = trim($row['dealer']);
                    $dealerCode = str_replace('D', '', trim($dealerCode));
                    $claimNumber = trim($row['dealer_claim_number']);
                    $claimNumber = $claimNumber.str_repeat(' ', 10 - strlen($claimNumber));
                    $vin = trim($row['vin']).'1';
                    $claimSubmitted = str_pad('', 14);
                    $odometer = trim($row['kilometers']);
                    $odometer = str_repeat('0', 7 - strlen($odometer)).$odometer.' ';
                    $repairDate = trim($row['failure_date']);
                    $repairDate = $repairDate ? date('Ymd', strtotime(trim($repairDate))) : '';
                    $vehicleRegistrationNo = str_pad('', 15);
                    $repairOrderNo = $row['sys_claim'];
                    $repairOrderNo = $repairOrderNo ? $repairOrderNo : '';
                    $repairOrderNo = $repairOrderNo.str_repeat(' ', 11 - strlen($repairOrderNo));
                    $symptomCode = trim($row['symptom_code']);
                    $symptomCode = $symptomCode.str_repeat(' ', 3 - strlen($symptomCode)).' ';
                    $damageCode = trim($row['damage_code']);
                    $damageCode = $damageCode.str_repeat(' ', 3 - strlen($damageCode));
                    $processNumber = trim($row['process_number']);
                    $processNumber = $processNumber ? $processNumber : '';

                    $result = $fileIdentifier.$claimId.$distributorCode.$dealerCode.$claimNumber.$vin.$claimSubmitted.$odometer.$repairDate.$vehicleRegistrationNo.$repairOrderNo.$symptomCode.$damageCode.$processNumber;
                    $spasiFinal = str_pad('', ($lengthLimit - strlen($result)-1));
                    $finalResult = $result.$spasiFinal."\n";

                    fwrite($myfile, $finalResult);

                } else if($fileIdentifier == 'A3070') {
                    $claimId = str_pad('', 14);
                    $distributorCode = trim($row['distributor_code']);
                    $dealerCode = trim($row['dealer']);
                    $dealerCode = str_replace('D', '', trim($dealerCode));
                    $claimNumber = trim($row['dealer_claim_number']);
                    $claimNumber = $claimNumber.str_repeat(' ', 10 - strlen($claimNumber));
                    $vin = trim($row['vin']).'1';

                    $causalPartNo = trim($row['causal_part']);
                    $stLenCausal = strlen($causalPartNo);
                    if($stLenCausal > 12) {
                        $causalPartNo = substr($causalPartNo, ($stLenCausal-12));
                    }
                    $causalPartNo = $causalPartNo.str_repeat(' ', 12 - strlen($causalPartNo));

                    $causalPartQty = $row['causal_quantity'];
                    if($causalPartQty == '') {
                        $causalPartQty = '';
                    } else {
                        $causalPartQty = $causalPartQty ? floatval($causalPartQty) : '';
                        $explodecausalPartQty = explode('.', $causalPartQty);
                        $explodecausalPartQty[0] = intval($explodecausalPartQty[0]);
                        $decimal = $explodecausalPartQty[1] ?? 0;
                        $dot = '.';
                        $causalPartQty = '+'.str_repeat(0, 3 - strlen($explodecausalPartQty[0])).$explodecausalPartQty[0].$dot.$decimal;
                    }
                    $proRate = str_pad('', 5);

                    $result = $fileIdentifier.$claimId.$distributorCode.$dealerCode.$claimNumber.$vin.$causalPartNo.$causalPartQty.$proRate;
                    $spasiFinal = str_pad('', ($lengthLimit - strlen($result)-1));
                    $finalResult = $result.$spasiFinal."\n";

                    fwrite($myfile, $finalResult);
                } else if($fileIdentifier == 'A3080') {
                    $claimId = str_pad('', 14);
                    $distributorCode = trim($row['distributor_code']);
                    $dealerCode = trim($row['dealer']);
                    $dealerCode = str_replace('D', '', trim($dealerCode));
                    $claimNumber = trim($row['dealer_claim_number']);
                    $claimNumber = $claimNumber.str_repeat(' ', 10 - strlen($claimNumber));
                    $vin = trim($row['vin']).'1';
                    $seqNo = '001';

                    $relatedPartNo = trim($row['realated_part']);
                    $strLenRelated = strlen($relatedPartNo);
                    if($strLenRelated > 12) {
                        $relatedPartNo = substr($strLenRelated, ($strLenRelated-12));
                    }
                    $relatedPartNo = $relatedPartNo.str_repeat(' ', 12 - strlen($relatedPartNo));

                    $relatedPartQty = ''; // TODO: diubah kalau ada kesekapakatan dari EMI related qty di tambahkan column di excel
                    if($relatedPartQty == '' || $relatedPartNo == '') {
                        $relatedPartQty = '';
                    } else {
                        $relatedPartQty = $relatedPartQty ? floatval($relatedPartQty) : '';
                        $explodeRelatedPartQty = explode('.', $relatedPartQty);
                        $explodeRelatedPartQty[0] = intval($explodeRelatedPartQty[0]);
                        $decimal = $explodeRelatedPartQty[1] ?? 0;
                        $dot = '.';
                        $relatedPartQty = '+'.str_repeat(0, 3 - strlen($explodeRelatedPartQty[0])).$explodeRelatedPartQty[0].$dot.$decimal;
                    }
                    $proRate = str_pad('', 5);
                    $result = $fileIdentifier.$claimId.$distributorCode.$dealerCode.$claimNumber.$vin.$seqNo.$relatedPartNo.$relatedPartQty;
                    $spasiFinal = str_pad('', ($lengthLimit - strlen($result)-1));
                    $finalResult = $result.$spasiFinal."\n";

                    fwrite($myfile, $finalResult);
                } else if($fileIdentifier == 'A3090') {
                    $claimId = str_pad('', 14);
                    $distributorCode = trim($row['distributor_code']);
                    $dealerCode = trim($row['dealer']);
                    $dealerCode = str_replace('D', '', trim($dealerCode));
                    $claimNumber = trim($row['dealer_claim_number']);
                    $claimNumber = $claimNumber.str_repeat(' ', 10 - strlen($claimNumber));
                    $vin = trim($row['vin']).'1';
                    $seqNo = '001';

                    $operationNo = trim($row['primary_operation_code']);
                    $strLenOperation = strlen($operationNo);
                    if($strLenOperation > 8) {
                        $operationNo = substr($strLenOperation, ($strLenOperation-8));
                    }
                    $operationNo = $operationNo.str_repeat(' ', 8 - strlen($operationNo));

                    $hoursClaimed = trim($row['primary_operation_hours']);
                    if($hoursClaimed == '' || $operationNo == '') {
                        $hoursClaimed = '';
                    } else {
                        $hoursClaimed = $hoursClaimed ? floatval($hoursClaimed) : '';
                        $explodeHoursClaimed = explode('.', $hoursClaimed);
                        $explodeHoursClaimed[0] = intval($explodeHoursClaimed[0]);
                        $decimal = $explodeHoursClaimed[1] ?? 0;
                        $dot = '.';
                        $hoursClaimed = '+'.str_repeat(0, 3 - strlen($explodeHoursClaimed[0])).$explodeHoursClaimed[0].$dot.$decimal;
                    }
                    $proRate = str_pad('', 5);

                    $result = $fileIdentifier.$claimId.$distributorCode.$dealerCode.$claimNumber.$vin.$seqNo.$operationNo.$hoursClaimed;
                    $spasiFinal = str_pad('', ($lengthLimit - strlen($result)-1));
                    $finalResult = $result.$spasiFinal."\n";

                    fwrite($myfile, $finalResult);
                }
            }
        }

        fclose($myfile);

    }

    public function writeFileBackup($data)
    {
        $perArray = array(
            'A3010' => array(
                'fileIdentifier' => 'A3010',
                'spasiFileIdentifier' => 14,
                'distributorCode' => 'distributor_code',
                'dealerCode' => 'dealer',
                'claimNumber' => 'dealer_claim_number',
                'spasiClaimNumber' => 20,
                'vin' => 'vin',
                'spasiVin' => 14,
                'odometer' => 'kilometers',
                'spasiOdometer' => 1,
                'repairDate' => 'failure_date',
                'spasiRepairDate' => 15,
                'sysClaim' => 'sys_claim',
                'spasiSysClaim' => 6,
                'symptomCode' => 'symptom_code',
                'spasiSymptomCode' => 2,
                'damageCode' => 'damage_code',
                'spasiDamageCode' => 1,
                'processNumber' => 'process_number'
            ),
            'A3070' => array(
                'fileIdentifier' => 'A3070',
                'spasiFileIdentifier' => 14,
                'distributorCode' => 'distributor_code',
                'dealerCode' => 'dealer',
                'claimNumber' => 'dealer_claim_number',
                'spasiClaimNumber' => 20,
                'vin' => 'vin',
                'spasiVin' => 14,
                'odometer' => 'causal_part', //Causal part
                'spasiOdometer' => 1,
                'repairDate' => 'causal_quantity', //Causal Qty
                'spasiRepairDate' => 15,
                'sysClaim' => '',
                'spasiSysClaim' => 0,
                'symptomCode' => '',
                'spasiSymptomCode' => 0,
                'damageCode' => '',
                'spasiDamageCode' => 0,
                'processNumber' => ''
            ),
            'A3080' => array(
                'fileIdentifier' => 'A3080',
                'spasiFileIdentifier' => 14,
                'distributorCode' => 'distributor_code',
                'dealerCode' => 'dealer',
                'claimNumber' => 'dealer_claim_number',
                'spasiClaimNumber' => 20,
                'vin' => 'vin',
                'spasiVin' => 14,
                'odometer' => 'realated_part', //Related part
                'spasiOdometer' => 1,
                'repairDate' => '', //Related qty
                'spasiRepairDate' => 15,
                'sysClaim' => '',
                'spasiSysClaim' => 0,
                'symptomCode' => '',
                'spasiSymptomCode' => 0,
                'damageCode' => '',
                'spasiDamageCode' => 0,
                'processNumber' => ''
            ),
            'A3090' => array(
                'fileIdentifier' => 'A3090',
                'spasiFileIdentifier' => 14,
                'distributorCode' => 'distributor_code',
                'dealerCode' => 'dealer',
                'claimNumber' => 'dealer_claim_number',
                'spasiClaimNumber' => 20,
                'vin' => 'vin',
                'spasiVin' => 14,
                'odometer' => 'primary_operation_code', //Primary Operation Code
                'spasiOdometer' => 1,
                'repairDate' => 'primary_operation_hours', //Primary Operation Hour
                'spasiRepairDate' => 15,
                'sysClaim' => '',
                'spasiSysClaim' => 0,
                'symptomCode' => '',
                'spasiSymptomCode' => 0,
                'damageCode' => '',
                'spasiDamageCode' => 0,
                'processNumber' => ''
            ),
        );

        $lengthLimit = 591;

        if(!is_dir(storage_path('app/result_convert'))) {
            mkdir(storage_path('app/result_convert'));
        }

        $myfile = fopen(storage_path('app/result_convert/result_convert.txt'), "w") or die("Unable to open file!");
        foreach($data[0] as $row) {
            foreach($perArray as $column) {
                //echo $column['spasiFileIdentifier'];
                $fileIdentifier = trim($column['fileIdentifier']);
                $spasiFileIdentifier = str_pad('', $column['spasiFileIdentifier']);
                $distributorCode = trim($row[$column['distributorCode']]);
                $dealerCode = str_replace('D', '', trim($row[$column['dealerCode']]));
                $claimNumber = trim($row[$column['claimNumber']]);
                $spasiClaimNumber = str_pad('', ($column['spasiClaimNumber'] - strlen($distributorCode.$dealerCode.$claimNumber)));
                $vin = trim($row[$column['vin']]).'1';
                $spasiVin = str_pad('', $column['spasiVin']);
                $odometer = trim($row[$column['odometer']]);
                $odometer = ($fileIdentifier == 'A3010') ? str_repeat('0', 7 - strlen($odometer)).$odometer : $odometer;
                $spasiOdometer = str_pad('', $column['spasiOdometer']);
                $spasiRepairDate = $column['spasiRepairDate'] ? str_pad('', $column['spasiRepairDate']) : '';
                $sysClaim = $column['sysClaim'] ? trim($row[$column['sysClaim']]) : '';
                $spasiSysClaim = $column['spasiSysClaim'] ? str_pad('', $column['spasiSysClaim']) : '';
                $symptomCode = $column['symptomCode'] ? trim($row[$column['symptomCode']]) : '';
                $spasiSymptomCode = $column['spasiSymptomCode'] ? str_pad('', $column['spasiSymptomCode']) : '';
                $damageCode = $column['damageCode'] ? trim($row[$column['damageCode']]) : '';
                $spasiDamageCode = $column['spasiDamageCode'] ? str_pad('', $column['spasiDamageCode']) : '';
                $processNumber = $column['processNumber'] ? trim($row[$column['processNumber']]) : '';

                if($fileIdentifier == 'A3070') {
                    $spasiVin = str_pad('', 0);
                    $spasiOdometer = str_pad('', 3);
                } elseif($fileIdentifier == 'A3080') {
                    $spasiVin = str_pad('', 3);
                    $spasiOdometer = str_pad('', 12 - strlen($odometer));
                } elseif($fileIdentifier == 'A3090') {
                    $spasiVin = '001';
                    $spasiOdometer = str_pad('', 0);
                }

                $strLenClaimNumber = strlen($claimNumber);
                if($strLenClaimNumber > 10) {
                    $claimNumber = substr($claimNumber, ($strLenClaimNumber-10));
                }

                // For Causal, Related Part, Labout
                $strLenOdometer = strlen($odometer);
                if($fileIdentifier == 'A3070') {
                    if($strLenOdometer > 12) {
                        $odometer = substr($odometer, ($strLenOdometer-12));
                    }
                } elseif($fileIdentifier == 'A3090') {
                    if($strLenOdometer > 8) {
                        $odometer = substr($odometer, ($strLenOdometer-8));
                    }
                }


                //Different (using if)
                if($fileIdentifier == 'A3010') {
                    $repairDate = $column['repairDate'] ? date('Ymd', strtotime(trim($row[$column['repairDate']]))) : '';
                } else {

                    if($column['repairDate'] == '') {
                        $repairDate = '';
                    } else {
                        $repairDate = $column['repairDate'] ? trim($row[$column['repairDate']]) : '';
                        $repairDate = number_format((float)$repairDate, 1, '.', '');
                        $explodeRepairDate = explode('.', $repairDate);
                        $repairDate = '+'.str_repeat('0', 3 - strlen($explodeRepairDate[0])).$explodeRepairDate[0].'.'.$explodeRepairDate[1];
                    }

                }

                $result = $fileIdentifier.$spasiFileIdentifier.$distributorCode.$dealerCode.$claimNumber.$spasiClaimNumber.$vin.$spasiVin.$odometer.$spasiOdometer.$repairDate.$spasiRepairDate.$sysClaim.$spasiSysClaim.$symptomCode.$spasiSymptomCode.$damageCode.$spasiDamageCode.$processNumber;
                $spasiFinal = str_pad('', ($lengthLimit - strlen($result)-1));
                $finalResult = $result.$spasiFinal."\n";

                // if(strlen($contoh) > $lengthLimit) {
                //     echo 'Jumlah karakter melebihi '.$lengthLimit.' karakter';die;
                // }

                fwrite($myfile, $finalResult);
                //Storage::disk('local')->put('result_convert/result_convert.txt', $contoh);
            }
        }
        fclose($myfile);
    }
}
