<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckAdminAtpmSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(!$request->session()->exists('user')) {
            return redirect('/');
        }

        if($request->session()->get('level_access') != '2') {
            return redirect('/home');
        }

        return $next($request);
    }
}
