<?php

namespace App\Repository\Api;

use App\Traits\WithValidateToken;
use App\Traits\WithWrsApi;
use Illuminate\Support\Facades\Http;

class ApiDealerUserRepository
{
    use WithWrsApi;
    use WithValidateToken;

    public function all()
    {
        $data = Http::withHeaders([
            'X-Auth-Token' => session()->get('token')
        ])
        ->get($this->wrsApi.'/dealer-user/')
        ->json();

        return($this->validateToken($data));
    }

    public function getById($id)
    {
        $data = Http::withHeaders([
            'X-Auth-Token' => session()->get('token')
        ])
        ->get($this->wrsApi.'/dealer-user/get?id='.$id)
        ->json();

        return($this->validateToken($data));
    }

    public function login($username, $password)
    {
        return Http::post($this->wrsApi.'/dealer-user/login', [
            'username' => $username,
            'password' => $password
        ]);
    }

    public function loginAfterSales($username, $password)
    {
        return Http::post($this->wrsAfterSalesApi.'/atpm-user/login', [
            'username' => $username,
            'password' => $password
        ]);
    }

    public function getByIdDealer($id)
    {
        $data = Http::withHeaders([
            'X-Auth-Token' => session()->get('token')
        ])
        ->get($this->wrsApi.'/dealer-user/get/fk_dealer/'.$id)
        ->json();

        return($this->validateToken($data));
    }

    public function allWithPagination()
    {
        $data = Http::withHeaders([
            'X-Auth-Token' => session()->get('token')
        ])
        ->get($this->wrsApi.'/dealer-user/pagination/')
        ->json();

        return($this->validateToken($data));
    }
}
