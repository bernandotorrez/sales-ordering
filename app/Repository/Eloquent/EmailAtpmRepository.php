<?php

namespace App\Repository\Eloquent;

use App\Models\SalesOrdering\EmailAtpm;

class EmailAtpmRepository extends BaseRepository
{
    public function __construct(EmailAtpm $model)
    {
        parent::__construct($model);
    }
}
