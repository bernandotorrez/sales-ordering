<?php

namespace App\Repository\Eloquent;

use App\Models\User;
use Illuminate\Support\Facades\DB;

class UserRepository extends BaseRepository
{
    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    /**
     * @param array $id
     */
    public function massDeleteUser(array $id)
    {
        return $this->model->where('is_from_wrs', '!=', '1')->whereIn($this->primaryKey, $id)->delete();
    }

    public function blacklist(array $arrayId, string $remark)
    {
        return $this->model->whereIn('id_user', $arrayId)->update([
                'flag_blacklist' => '1',
                'date_blacklist' => date('Y-m-d H:i:s'),
                'blacklist_by' => session()->get('user')['username'],
                'remarks_blacklist' => $remark
            ]
        );
    }

    public function whitelist(array $arrayId, string $remark)
    {
        return $this->model->whereIn('id_user', $arrayId)->update([
            'flag_blacklist' => '0',
            'date_whitelist' => date('Y-m-d H:i:s'),
            'whitelist_by' => session()->get('user')['username'],
            'remarks_whitelist' => $remark
        ]
    );
    }

    public function viewPaginationdealerBM(
        string $viewName,
        string $search = '',
        string $sortBy,
        string $sortDirection = 'asc',
        int $perPage,
        string $tableName
    ) {
        $arrayField = $this->searchableColumn;
        $countField = count($arrayField);
        $flagBlacklist = ($tableName == 'whitelist') ? 0 : 1;

        $data = DB::table($viewName);
        $data = $data->where(function ($query) use ($arrayField, $countField, $search) {
            if ($countField >= 1) {
                for ($i = 0; $i <= $countField - 1; $i++) {
                    if ($i == 0) {
                        $query = $query->where($arrayField[$i], 'like', '%' . $search . '%');
                    } else {
                        $query = $query->orWhere($arrayField[$i], 'like', '%' . $search . '%');
                    }
                }
            }
        });
        $data = $data->where('status', '1');
        $data = $data->where('id_dealer_level', 'BM');
        $data = $data->where('status_atpm', 'dealer');
        $data = $data->where('flag_blacklist', $flagBlacklist);
        $data = $data->orderBy($sortBy, $sortDirection);
        $data = $data->paginate($perPage);

        return $data;
    }

    public function getByKodeWRS($id)
    {
        return $this->model->where('kd_user_wrs', $id)->first();
    }

}
