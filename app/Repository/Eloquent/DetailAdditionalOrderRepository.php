<?php

namespace App\Repository\Eloquent;

use App\Models\SalesOrdering\DetailAdditionalOrderUnit;
use App\Models\SalesOrdering\MasterAdditionalOrderUnit;
use Illuminate\Support\Facades\DB;

class DetailAdditionalOrderRepository extends BaseRepository
{
    protected $tableMaster;
    protected $tableDetail;

    public function __construct(
        DetailAdditionalOrderUnit $model,
        MasterAdditionalOrderUnit $tableMaster
    ) {
        parent::__construct($model);
        $this->tableDetail = (new $model)->getTable();
        $this->tableMaster = (new $tableMaster)->getTable();
    }

    public function getByIdMaster($id)
    {
        return $this->model->where(['status' => '1', 'id_master_additional_order_unit' => $id])->get();
    }

    public function allocate($detail)
    {
        foreach($detail as $data) {
            $update = $this->model->where('id_detail_additional_order_unit', $data['id_detail'])
            ->update([
                'qty_input' => $data['qty_input'],
                'qty_diff' => $data['qty_diff'],
            ]);
        }

        return $update;
    }

    public function getTopOrderedModel($statusAtpm, $idDealer, $bind)
    {
        $month = ($bind['month'] < 10) ? '0' . $bind['month'] : $bind['month'];
        $year = $bind['year'];

        if ($statusAtpm == 'atpm') {
            if ($month != '' && $year != '') {
                return DB::select("SELECT tdaou.id_model, tdaou.model_name, tdaou.id_type, tdaou.type_name, tdaou.year_production, tmaou.date_allocation_atpm, SUM(tdaou.qty) AS ordered_qty
                FROM $this->tableDetail tdaou
                INNER JOIN $this->tableMaster tmaou ON tmaou.id_master_additional_order_unit = tdaou.id_master_additional_order_unit
                WHERE tmaou.date_allocation_atpm LIKE '%$year-$month%'
                AND tmaou.flag_send_approval_dealer = '1'
                AND tmaou.flag_approval_dealer = '1'
                AND tmaou.flag_submit_to_atpm = '1'
                AND tmaou.flag_allocation = '1'
                GROUP BY tdaou.id_model, tdaou.year_production
                ORDER BY ordered_qty DESC LIMIT 10;");
            } else {
                return DB::select("SELECT tdaou.id_model, tdaou.model_name, tdaou.id_type, tdaou.type_name, tdaou.year_production, tmaou.date_allocation_atpm, SUM(tdaou.qty) AS ordered_qty
                FROM $this->tableDetail tdaou
                INNER JOIN $this->tableMaster tmaou ON tmaou.id_master_additional_order_unit = tdaou.id_master_additional_order_unit
                AND tmaou.flag_send_approval_dealer = '1'
                AND tmaou.flag_approval_dealer = '1'
                AND tmaou.flag_submit_to_atpm = '1'
                AND tmaou.flag_allocation = '1'
                GROUP BY tdaou.id_model, tdaou.year_production
                ORDER BY ordered_qty DESC LIMIT 10;");
            }

        } else {
            if ($month != '' && $year != '') {
                return DB::select("SELECT tdaou.id_model, tdaou.model_name, tdaou.id_type, tdaou.type_name, tdaou.year_production, tmaou.date_allocation_atpm, SUM(tdaou.qty) AS ordered_qty
                FROM $this->tableDetail tdaou
                INNER JOIN $this->tableMaster tmaou ON tmaou.id_master_additional_order_unit = tdaou.id_master_additional_order_unit
                WHERE tmaou.id_dealer = '$idDealer'
                AND tmaou.date_allocation_atpm LIKE '%$year-$month%'
                AND tmaou.flag_send_approval_dealer = '1'
                AND tmaou.flag_approval_dealer = '1'
                AND tmaou.flag_submit_to_atpm = '1'
                AND tmaou.flag_allocation = '1'
                GROUP BY tdaou.id_model, tdaou.year_production
                ORDER BY ordered_qty DESC LIMIT 10;");
            } else {
                return DB::select("SELECT tdaou.id_model, tdaou.model_name, tdaou.id_type, tdaou.type_name, tdaou.year_production, tmaou.date_allocation_atpm, SUM(tdaou.qty) AS ordered_qty
                FROM $this->tableDetail tdaou
                INNER JOIN $this->tableMaster tmaou ON tmaou.id_master_additional_order_unit = tdaou.id_master_additional_order_unit
                WHERE tmaou.id_dealer = '$idDealer'
                AND tmaou.flag_send_approval_dealer = '1'
                AND tmaou.flag_approval_dealer = '1'
                AND tmaou.flag_submit_to_atpm = '1'
                AND tmaou.flag_allocation = '1'
                GROUP BY tdaou.id_model, tdaou.year_production
                ORDER BY ordered_qty DESC LIMIT 10;");
            }

        }

    }

    public function getTopOrderedType($statusAtpm, $idDealer, $bind)
    {
        $month = ($bind['month'] < 10) ? '0' . $bind['month'] : $bind['month'];
        $year = $bind['year'];

        if ($statusAtpm == 'atpm') {
            if ($month != '' && $year != '') {
                return DB::select("SELECT tdaou.id_model, tdaou.model_name, tdaou.id_type, tdaou.type_name, tdaou.year_production, tmaou.date_allocation_atpm, SUM(tdaou.qty) AS ordered_qty
                FROM $this->tableDetail tdaou
                INNER JOIN $this->tableMaster tmaou ON tmaou.id_master_additional_order_unit = tdaou.id_master_additional_order_unit
                WHERE tmaou.date_allocation_atpm LIKE '%$year-$month%'
                AND tmaou.flag_send_approval_dealer = '1'
                AND tmaou.flag_approval_dealer = '1'
                AND tmaou.flag_submit_to_atpm = '1'
                AND tmaou.flag_allocation = '1'
                GROUP BY tdaou.id_model, tdaou.id_type, tdaou.year_production
                ORDER BY ordered_qty DESC LIMIT 10;");
            } else {
                return DB::select("SELECT tdaou.id_model, tdaou.model_name, tdaou.id_type, tdaou.type_name, tdaou.year_production, tmaou.date_allocation_atpm, SUM(tdaou.qty) AS ordered_qty
                FROM $this->tableDetail tdaou
                INNER JOIN $this->tableMaster tmaou ON tmaou.id_master_additional_order_unit = tdaou.id_master_additional_order_unit
                WHERE tmaou.flag_send_approval_dealer = '1'
                AND tmaou.flag_approval_dealer = '1'
                AND tmaou.flag_submit_to_atpm = '1'
                AND tmaou.flag_allocation = '1'
                GROUP BY tdaou.id_model, tdaou.id_type, tdaou.year_production
                ORDER BY ordered_qty DESC LIMIT 10;");
            }

        } else {
            if ($month != '' && $year != '') {
                return DB::select("SELECT tdaou.id_model, tdaou.model_name, tdaou.id_type, tdaou.type_name, tdaou.year_production, tmaou.date_allocation_atpm, SUM(tdaou.qty) AS ordered_qty
                FROM $this->tableDetail tdaou
                INNER JOIN $this->tableMaster tmaou ON tmaou.id_master_additional_order_unit = tdaou.id_master_additional_order_unit
                WHERE tmaou.id_dealer = '$idDealer'
                AND tmaou.date_allocation_atpm LIKE '%$year-$month%'
                AND tmaou.flag_send_approval_dealer = '1'
                AND tmaou.flag_approval_dealer = '1'
                AND tmaou.flag_submit_to_atpm = '1'
                AND tmaou.flag_allocation = '1'
                GROUP BY tdaou.id_model, tdaou.id_type, tdaou.year_production
                ORDER BY ordered_qty DESC LIMIT 10;");
            } else {
                return DB::select("SELECT tdaou.id_model, tdaou.model_name, tdaou.id_type, tdaou.type_name, tdaou.year_production, tmaou.date_allocation_atpm, SUM(tdaou.qty) AS ordered_qty
                FROM $this->tableDetail tdaou
                INNER JOIN $this->tableMaster tmaou ON tmaou.id_master_additional_order_unit = tdaou.id_master_additional_order_unit
                WHERE tmaou.id_dealer = '$idDealer'
                AND tmaou.flag_send_approval_dealer = '1'
                AND tmaou.flag_approval_dealer = '1'
                AND tmaou.flag_submit_to_atpm = '1'
                AND tmaou.flag_allocation = '1'
                GROUP BY tdaou.id_model, tdaou.id_type, tdaou.year_production
                ORDER BY ordered_qty DESC LIMIT 10;");
            }

        }

    }
}
