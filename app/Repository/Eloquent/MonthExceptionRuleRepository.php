<?php

namespace App\Repository\Eloquent;

use App\Models\SalesOrdering\MonthExceptionRule;
use Illuminate\Support\Facades\DB;

class MonthExceptionRuleRepository extends BaseRepository
{
    public function __construct(MonthExceptionRule $model)
    {
        parent::__construct($model);
    }

    public function getByIdDealerAndIdMonth($idDealer, $idMonth)
    {
        return $this->model->where('status', '1')
            ->where('id_month', $idMonth)
            ->whereIn('id_dealer', [$idDealer, '0'])
            ->first();
    }

    public function getByIdDealerAndIdMonthAndMonthId($idDealer, $idMonth, $monthIdTo)
    {
        return $this->model->where('status', '1')
            ->where('id_month', $idMonth)
            ->where('month_id_to', $monthIdTo)
            ->whereIn('id_dealer', [$idDealer, '0'])
            ->first();
    }

    /**
     * Get Data Pagination With Query View
     * @param string $viewName
     * @param array $arrayField
     * @param string $search
     * @param string $sortBy
     * @param string $sortDirection
     * @param int $perPage
     */
    public function viewPagination(
        string $viewName,
        string $search = '',
        string $sortBy,
        string $sortDirection = 'asc',
        int $perPage
    )
    {
        $arrayField = $this->searchableColumn;
        $countField = count($arrayField);

        $data = DB::table($viewName);
        $data = $data->where(function ($query) use ($arrayField, $countField, $search) {
            if ($countField >= 1) {
                for ($i = 0; $i <= $countField - 1; $i++) {
                    if ($i == 0) {
                        $query = $query->where($arrayField[$i], 'like', '%' . $search . '%');
                    } else {
                        $query = $query->orWhere($arrayField[$i], 'like', '%' . $search . '%');
                    }
                }
            }
        });
        //$data = $data->where('status', '1');
        $data = $data->orderBy($sortBy, $sortDirection);
        $data = $data->paginate($perPage);

        return $data;
    }
}
