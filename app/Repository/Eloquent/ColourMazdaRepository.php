<?php

namespace App\Repository\Eloquent;

use App\Models\SalesOrdering\ColourMazda;

class ColourMazdaRepository extends BaseRepository
{
    public function __construct(ColourMazda $model)
    {
        parent::__construct($model);
    }

    public function getByIdColour($id)
    {
        return $this->model->where('id_colour', $id)->first();
    }

    public function getAvailableColour()
    {
        return $this->model->where(function($query) {
            $query = $query->where('flag_show_additional', '1');
            $query = $query->orWhere('flag_show_fix', '1');
        })->where('status', '1')->get();
    }
}
