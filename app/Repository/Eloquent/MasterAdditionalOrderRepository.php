<?php

namespace App\Repository\Eloquent;

use App\Models\SalesOrdering\DetailAdditionalOrderUnit;
use App\Models\SalesOrdering\MasterAdditionalOrderUnit;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class MasterAdditionalOrderRepository extends BaseRepository
{
    public function __construct(MasterAdditionalOrderUnit $model)
    {
        parent::__construct($model);
    }

    public function getByIdDealer($id)
    {
        return $this->model->where(['status' => '1', 'id_dealer' => $id])->get();
    }

    public function createDealerOrder($dataMaster, $dataDetail)
    {
        $insert = DB::transaction(function () use ($dataMaster, $dataDetail) {
            $insertMaster = $this->model->create($dataMaster);
            if ($insertMaster) {
                $insertDetail = $insertMaster->detailAdditionalOrderUnit()->createMany($dataDetail);
            }

            return $insertMaster;
        }, 5);

        return $insert;
    }

    public function updateDealerOrder($idMaster, $dataMaster, $dataDetail, $deleteIdDetail)
    {
        $update = DB::transaction(function () use ($idMaster, $dataMaster, $dataDetail, $deleteIdDetail) {
            $updateMaster = MasterAdditionalOrderUnit::where($this->primaryKey, $idMaster)
                ->update($dataMaster);
            if ($updateMaster) {
                foreach ($dataDetail as $key => $detail) {
                    $updateData = array(
                        'id_master_additional_order_unit' => $idMaster,
                        'id_model' => $dataDetail[$key]['id_model'],
                        'model_name' => $dataDetail[$key]['model_name'],
                        'id_colour' => $dataDetail[$key]['id_colour'],
                        'colour_name' => $dataDetail[$key]['colour_name'],
                        'id_type' => $dataDetail[$key]['id_type'],
                        'type_name' => $dataDetail[$key]['type_name'],
                        'qty' => $dataDetail[$key]['qty'],
                        'year_production' => $dataDetail[$key]['year_production'],
                    );
                    if ($dataDetail[$key]['id_detail_additional_order_unit'] == '') {
                        $insertDetail = DetailAdditionalOrderUnit::create($updateData);
                    } else {
                        $updateDetail = DetailAdditionalOrderUnit::where('id_detail_additional_order_unit',
                            $dataDetail[$key]['id_detail_additional_order_unit'])
                            ->update($updateData);
                    }
                }

                // Delete From Edit Page
                foreach ($deleteIdDetail as $idDetail) {
                    $deleteDetailData = DetailAdditionalOrderUnit::where('id_detail_additional_order_unit', $idDetail)
                        ->update(['status' => '0']);

                }
            }

            return $updateMaster;
        }, 5);

        return $update;
    }

    public function updateSubmitAtpm(array $arrayId, $kodeTahunRepository)
    {
        foreach ($arrayId as $id) {
            $orderSequence = $kodeTahunRepository->getOrderSequence($id);
            $data = array(
                'flag_submit_to_atpm' => '1',
                'date_submit_atpm_order' => Carbon::now(),
                'no_order_atpm' => $orderSequence,
                'submitted_by' => session()->get('user')['username'],
                'nama_submitted_by' => session()->get('user')['nama_user']
            );

            $update = DB::transaction(function () use ($id, $data) {
                return $this->model->where($this->primaryKey, $id)->update($data);
            });

        }

        return $update;
    }


    // Tab Draft
    public function getDraft($idDealer)
    {
        if (session()->get('user')['status_atpm'] == 'atpm') {
            return $this->model
                ->whereIn('flag_send_approval_dealer', ['0', '2'])
                ->where('flag_approval_dealer', '0')
                ->where('flag_submit_to_atpm', '0')
                ->where('flag_am_confirmation', '0')
                ->where('flag_allocation', '0')
                ->where('status', '1')
                ->get();
        } else {
            return $this->model
                ->whereIn('flag_send_approval_dealer', ['0', '2'])
                ->where('flag_approval_dealer', '0')
                ->where('flag_submit_to_atpm', '0')
                ->where('flag_am_confirmation', '0')
                ->where('flag_allocation', '0')
                ->where('status', '1')
                ->where('id_dealer', $idDealer)
                ->get();
        }

    }

    // Tab Waiting Approval
    public function getWaitingApprovalDealerPrinciple($idDealer)
    {
        if (session()->get('user')['status_atpm'] == 'atpm') {
            return $this->model
                ->where('flag_send_approval_dealer', '1')
                ->where('flag_approval_dealer', '0')
                ->where('flag_submit_to_atpm', '0')
                ->where('flag_am_confirmation', '0')
                ->where('flag_allocation', '0')
                ->where('status', '1')
                ->get();
        } else {
            return $this->model
                ->where('flag_send_approval_dealer', '1')
                ->where('flag_approval_dealer', '0')
                ->where('flag_submit_to_atpm', '0')
                ->where('flag_am_confirmation', '0')
                ->where('flag_allocation', '0')
                ->where('status', '1')
                ->where('id_dealer', $idDealer)
                ->get();
        }

    }

    // Tab Approved
    public function getApprovalDealerPrinciple($idDealer)
    {
        if (session()->get('user')['status_atpm'] == 'atpm') {
            return $this->model
                ->where('flag_send_approval_dealer', '1')
                ->where('flag_approval_dealer', '1')
                ->where('flag_submit_to_atpm', '0')
                ->where('flag_am_confirmation', '0')
                ->where('flag_allocation', '0')
                ->where('status', '1')
                ->get();
        } else {
            return $this->model
                ->where('flag_send_approval_dealer', '1')
                ->where('flag_approval_dealer', '1')
                ->where('flag_submit_to_atpm', '0')
                ->where('flag_am_confirmation', '0')
                ->where('flag_allocation', '0')
                ->where('status', '1')
                ->where('id_dealer', $idDealer)
                ->get();
        }

    }

    // Tab Submitted
    public function getSubmittedATPM($idDealer, $dealers)
    {
        if (session()->get('user')['status_atpm'] == 'atpm') {
            return $this->model
                ->whereIn('id_dealer', $dealers)
                ->where('flag_send_approval_dealer', '1')
                ->where('flag_approval_dealer', '1')
                ->where('flag_submit_to_atpm', '1')
                ->where('flag_am_confirmation', '0')
                ->where('flag_allocation', '0')
                ->where('status', '1')
                ->get();
        } else {
            return $this->model
                ->where('flag_send_approval_dealer', '1')
                ->where('flag_approval_dealer', '1')
                ->where('flag_submit_to_atpm', '1')
                ->where('flag_am_confirmation', '0')
                ->where('flag_allocation', '0')
                ->where('status', '1')
                ->where('id_dealer', $idDealer)
                ->get();
        }

    }

    // Tab AM Confirmation
    public function getAMConfirmation($idDealer)
    {
        if (session()->get('user')['status_atpm'] == 'atpm') {
            return $this->model
                //->whereIn('id_dealer', $dealers)
                ->where('flag_send_approval_dealer', '1')
                ->where('flag_approval_dealer', '1')
                ->where('flag_submit_to_atpm', '1')
                ->where('flag_am_confirmation', '1')
                ->where('flag_allocation', '0')
                ->where('status', '1')
                ->get();
        } else {
            return $this->model
                ->where('flag_send_approval_dealer', '1')
                ->where('flag_approval_dealer', '1')
                ->where('flag_submit_to_atpm', '1')
                ->where('flag_am_confirmation', '1')
                ->where('flag_allocation', '0')
                ->where('status', '1')
                ->where('id_dealer', $idDealer)
                ->get();
        }

    }

    public function getATPMAllocation($idDealer)
    {
        if (session()->get('user')['status_atpm'] == 'atpm') {
            return $this->model
                ->where('flag_send_approval_dealer', '1')
                ->where('flag_approval_dealer', '1')
                ->where('flag_submit_to_atpm', '1')
                ->where('flag_am_confirmation', '1')
                ->where('flag_allocation', '1')
                ->where('status', '1')
                ->get();
        } else {
            return $this->model
                ->where('flag_send_approval_dealer', '1')
                ->where('flag_approval_dealer', '1')
                ->where('flag_submit_to_atpm', '1')
                ->where('flag_allocation', '1')
                ->where('flag_am_confirmation', '1')
                ->where('status', '1')
                ->where('id_dealer', $idDealer)
                ->get();
        }

    }

    // TODO: tambahkan query where
    public function getCanceledAdditionalOrder($idDealer, $idCancel)
    {
        if (session()->get('user')['status_atpm'] == 'atpm') {
            if ($idCancel) {
                return $this->model
                    ->where('status', '0')
                    ->where('id_cancel_status', $idCancel)
                    ->with('cancelStatus')
                    ->get();
            } else {
                return $this->model
                    ->where('status', '0')
                    ->with('cancelStatus')
                    ->get();
            }
        } else {
            if ($idCancel) {
                return $this->model
                    ->where('status', '0')
                    ->where('id_dealer', $idDealer)
                    ->where('id_cancel_status', $idCancel)
                    ->with('cancelStatus')
                    ->get();
            } else {
                return $this->model
                    ->where('status', '0')
                    ->where('id_dealer', $idDealer)
                    ->with('cancelStatus')
                    ->get();
            }
        }

    }

    public function getReportAdditionalOrder($idDealer, $kdDealer, $dateStart, $dateEnd, $statusOrder)
    {
        if (session()->get('user')['status_atpm'] == 'atpm') {
            $data = $this->model;
            $data = $data->where('status', '1');

            if ($kdDealer != '-') {
                $data = $data->where('id_dealer', $kdDealer);
            }

            if ($dateStart != '' && $dateEnd != '') {
                $data = $data->where('date_save_order', '>=', $dateStart . ' 00:00:00')
                    ->where('date_save_order', '<=', $dateEnd . ' 23:59:59');
            }

            if ($statusOrder != '-') {
                if ($statusOrder == 'draft') {
                    $data = $data->whereIn('flag_send_approval_dealer', ['0', '2'])
                        ->where('flag_approval_dealer', '0')
                        ->where('flag_submit_to_atpm', '0')
                        ->where('flag_am_confirmation', '0')
                        ->where('flag_allocation', '0');
                } else if ($statusOrder == 'waiting-approval') {
                    $data = $data->where('flag_send_approval_dealer', '1')
                        ->where('flag_approval_dealer', '0')
                        ->where('flag_submit_to_atpm', '0')
                        ->where('flag_am_confirmation', '0')
                        ->where('flag_allocation', '0');
                } else if ($statusOrder == 'approved') {
                    $data = $data->where('flag_send_approval_dealer', '1')
                        ->where('flag_approval_dealer', '1')
                        ->where('flag_submit_to_atpm', '0')
                        ->where('flag_am_confirmation', '0')
                        ->where('flag_allocation', '0');
                } else if ($statusOrder == 'submitted') {
                    $data = $data->where('flag_send_approval_dealer', '1')
                        ->where('flag_approval_dealer', '1')
                        ->where('flag_submit_to_atpm', '1')
                        ->where('flag_am_confirmation', '0')
                        ->where('flag_allocation', '0');
                } else if ($statusOrder == 'confirmed') {
                    $data = $data->where('flag_send_approval_dealer', '1')
                        ->where('flag_approval_dealer', '1')
                        ->where('flag_submit_to_atpm', '1')
                        ->where('flag_am_confirmation', '1')
                        ->where('flag_allocation', '0');
                } else if ($statusOrder == 'allocated') {
                    $data = $data->where('flag_send_approval_dealer', '1')
                        ->where('flag_approval_dealer', '1')
                        ->where('flag_submit_to_atpm', '1')
                        ->where('flag_am_confirmation', '1')
                        ->where('flag_allocation', '1');
                }
            }

            return $data->get();
        } else {
            $data = $this->model;
            $data = $data->where('status', '1');
            $data = $data->where('id_dealer', $idDealer);

            if ($kdDealer != '-') {
                $data = $data->where('id_dealer', $kdDealer);
            }

            if ($dateStart != '' && $dateEnd != '') {
                $data = $data->where('date_save_order', '>=', $dateStart . ' 00:00:00')
                    ->where('date_save_order', '<=', $dateEnd . ' 23:59:59');
            }

            if ($statusOrder != '-') {
                if ($statusOrder == 'draft') {
                    $data = $data->whereIn('flag_send_approval_dealer', ['0', '2'])
                        ->where('flag_approval_dealer', '0')
                        ->where('flag_submit_to_atpm', '0')
                        ->where('flag_allocation', '0');
                } else if ($statusOrder == 'waiting-approval') {
                    $data = $data->where('flag_send_approval_dealer', '1')
                        ->where('flag_approval_dealer', '0')
                        ->where('flag_submit_to_atpm', '0')
                        ->where('flag_allocation', '0');
                } else if ($statusOrder == 'approved') {
                    $data = $data->where('flag_send_approval_dealer', '1')
                        ->where('flag_approval_dealer', '1')
                        ->where('flag_submit_to_atpm', '0')
                        ->where('flag_allocation', '0');
                } else if ($statusOrder == 'submitted') {
                    $data = $data->where('flag_send_approval_dealer', '1')
                        ->where('flag_approval_dealer', '1')
                        ->where('flag_submit_to_atpm', '1')
                        ->where('flag_allocation', '0');
                } else if ($statusOrder == 'allocated') {
                    $data = $data->where('flag_send_approval_dealer', '1')
                        ->where('flag_approval_dealer', '1')
                        ->where('flag_submit_to_atpm', '1')
                        ->where('flag_allocation', '1');
                }
            }

            return $data->get();
        }

    }
}
