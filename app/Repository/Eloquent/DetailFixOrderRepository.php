<?php

namespace App\Repository\Eloquent;

use App\Models\SalesOrdering\DetailFixOrderUnit;
use App\Models\SalesOrdering\MasterFixOrderUnit;
use Illuminate\Support\Facades\DB;

class DetailFixOrderRepository extends BaseRepository
{
    protected $tableMaster;
    protected $tableDetail;

    public function __construct(
        DetailFixOrderUnit $model,
        MasterFixOrderUnit $tableMaster
    ) {
        parent::__construct($model);
        $this->tableDetail = (new $model)->getTable();
        $this->tableMaster = (new $tableMaster)->getTable();
    }

    public function getByIdMaster($id)
    {
        return $this->model->where(['status' => '1', 'id_master_fix_order_unit' => $id])->get();
    }

    public function getByIdForAllocation(int $id)
    {
        return DB::table('view_fix_order')->where('id_master_fix_order_unit', $id)->get();
    }

    public function getTopOrderedModel($statusAtpm, $idDealer, $bind)
    {
        $month = ($bind['month'] < 10) ? '0' . $bind['month'] : $bind['month'];
        $year = $bind['year'];

        if ($statusAtpm == 'atpm') {
            if ($month != '' && $year != '') {
                return DB::select("SELECT tdfou.id_model, tdfou.model_name, tdfou.total_qty, tdfou.year_production, tmfou.id_dealer,
                tmfou.date_allocation_atpm, SUM(total_qty) AS ordered_qty
                FROM $this->tableDetail tdfou
                INNER JOIN $this->tableMaster tmfou ON tmfou.id_master_fix_order_unit = tdfou.id_master_fix_order_unit
                WHERE tmfou.date_allocation_atpm LIKE '%$year-$month%'
                AND tmfou.flag_send_approval_dealer = '1'
                AND tmfou.flag_approval_dealer = '1'
                AND tmfou.flag_submit_to_atpm = '1'
                AND tmfou.flag_allocation = '1'
                GROUP BY tdfou.id_model, tdfou.year_production
                ORDER BY ordered_qty DESC
                LIMIT 10;");
            } else {
                return DB::select("SELECT tdfou.id_model, tdfou.model_name, tdfou.total_qty, tdfou.year_production, tmfou.id_dealer,
                tmfou.date_allocation_atpm, SUM(total_qty) AS ordered_qty
                FROM $this->tableDetail tdfou
                INNER JOIN $this->tableMaster tmfou ON tmfou.id_master_fix_order_unit = tdfou.id_master_fix_order_unit
                WHERE tmfou.flag_send_approval_dealer = '1'
                AND tmfou.flag_approval_dealer = '1'
                AND tmfou.flag_submit_to_atpm = '1'
                AND tmfou.flag_allocation = '1'
                GROUP BY tdfou.id_model, tdfou.year_production
                ORDER BY ordered_qty DESC
                LIMIT 10;");
            }

        } else {
            if ($month != '' && $year != '') {
                return DB::select("SELECT tdfou.id_model, tdfou.model_name, tdfou.total_qty, tdfou.year_production, tmfou.id_dealer,
                tmfou.date_allocation_atpm, SUM(total_qty) AS ordered_qty
                FROM $this->tableDetail tdfou
                INNER JOIN $this->tableMaster tmfou ON tmfou.id_master_fix_order_unit = tdfou.id_master_fix_order_unit
                WHERE tmfou.id_dealer = '$idDealer'
                AND tmfou.date_allocation_atpm LIKE '%$year-$month%'
                AND tmfou.flag_send_approval_dealer = '1'
                AND tmfou.flag_approval_dealer = '1'
                AND tmfou.flag_submit_to_atpm = '1'
                AND tmfou.flag_allocation = '1'
                GROUP BY tdfou.id_model, tdfou.year_production
                ORDER BY ordered_qty DESC
                LIMIT 10;");
            } else {
                return DB::select("SELECT tdfou.id_model, tdfou.model_name, tdfou.total_qty, tdfou.year_production, tmfou.id_dealer,
                tmfou.date_allocation_atpm, SUM(total_qty) AS ordered_qty
                FROM $this->tableDetail tdfou
                INNER JOIN $this->tableMaster tmfou ON tmfou.id_master_fix_order_unit = tdfou.id_master_fix_order_unit
                WHERE tmfou.id_dealer = '$idDealer'
                AND tmfou.flag_send_approval_dealer = '1'
                AND tmfou.flag_approval_dealer = '1'
                AND tmfou.flag_submit_to_atpm = '1'
                AND tmfou.flag_allocation = '1'
                GROUP BY tdfou.id_model, tdfou.year_production
                ORDER BY ordered_qty DESC
                LIMIT 10;");
            }

        }

    }

    public function getTopOrderedType($statusAtpm, $idDealer, $bind)
    {
        $month = ($bind['month'] < 10) ? '0' . $bind['month'] : $bind['month'];
        $year = $bind['year'];

        if ($statusAtpm == 'atpm') {
            if ($month != '' && $year != '') {
                return DB::select("SELECT tdfou.id_model, tdfou.model_name, tdfou.total_qty, tdfou.type_name, tdfou.year_production, tmfou.id_dealer,
                tmfou.date_allocation_atpm, SUM(total_qty) AS ordered_qty
                FROM $this->tableDetail tdfou
                INNER JOIN $this->tableMaster tmfou ON tmfou.id_master_fix_order_unit = tdfou.id_master_fix_order_unit
                WHERE tmfou.date_allocation_atpm LIKE '%$year-$month%'
                AND tmfou.flag_send_approval_dealer = '1'
                AND tmfou.flag_approval_dealer = '1'
                AND tmfou.flag_submit_to_atpm = '1'
                AND tmfou.flag_allocation = '1'
                GROUP BY tdfou.id_model, tdfou.id_type, tdfou.year_production
                ORDER BY ordered_qty DESC
                LIMIT 10;");
            } else {
                return DB::select("SELECT tdfou.id_model, tdfou.model_name, tdfou.total_qty, tdfou.type_name, tdfou.year_production, tmfou.id_dealer,
                tmfou.date_allocation_atpm, SUM(total_qty) AS ordered_qty
                FROM $this->tableDetail tdfou
                INNER JOIN $this->tableMaster tmfou ON tmfou.id_master_fix_order_unit = tdfou.id_master_fix_order_unit
                WHERE tmfou.flag_send_approval_dealer = '1'
                AND tmfou.flag_approval_dealer = '1'
                AND tmfou.flag_submit_to_atpm = '1'
                AND tmfou.flag_allocation = '1'
                GROUP BY tdfou.id_model, tdfou.id_type, tdfou.year_production
                ORDER BY ordered_qty DESC
                LIMIT 10;");
            }

        } else {
            if ($month != '' && $year != '') {
                return DB::select("SELECT tdfou.id_model, tdfou.model_name, tdfou.total_qty, tdfou.type_name, tdfou.year_production, tmfou.id_dealer,
                tmfou.date_allocation_atpm, SUM(total_qty) AS ordered_qty
                FROM $this->tableDetail tdfou
                INNER JOIN $this->tableMaster tmfou ON tmfou.id_master_fix_order_unit = tdfou.id_master_fix_order_unit
                WHERE tmfou.id_dealer = '$idDealer'
                AND tmfou.date_allocation_atpm LIKE '%$year-$month%'
                AND tmfou.flag_send_approval_dealer = '1'
                AND tmfou.flag_approval_dealer = '1'
                AND tmfou.flag_submit_to_atpm = '1'
                AND tmfou.flag_allocation = '1'
                GROUP BY tdfou.id_model, tdfou.id_type, tdfou.year_production
                ORDER BY ordered_qty DESC
                LIMIT 10;");
            } else {
                return DB::select("SELECT tdfou.id_model, tdfou.model_name, tdfou.total_qty, tdfou.type_name, tdfou.year_production, tmfou.id_dealer,
                tmfou.date_allocation_atpm, SUM(total_qty) AS ordered_qty
                FROM $this->tableDetail tdfou
                INNER JOIN $this->tableMaster tmfou ON tmfou.id_master_fix_order_unit = tdfou.id_master_fix_order_unit
                WHERE tmfou.id_dealer = '$idDealer'
                AND tmfou.flag_send_approval_dealer = '1'
                AND tmfou.flag_approval_dealer = '1'
                AND tmfou.flag_submit_to_atpm = '1'
                AND tmfou.flag_allocation = '1'
                GROUP BY tdfou.id_model, tdfou.id_type, tdfou.year_production
                ORDER BY ordered_qty DESC
                LIMIT 10;");
            }

        }

    }
}
