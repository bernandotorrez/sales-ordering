<?php

namespace App\Repository\Eloquent;

use App\Models\SalesOrdering\MazdaModel;

class MazdaModelRepository extends BaseRepository
{
    public function __construct(MazdaModel $model)
    {
        parent::__construct($model);
    }

    public function getAvailableModel()
    {
        return $this->model->where(function($query) {
            $query = $query->where('flag_show_additional', '1');
            $query = $query->orWhere('flag_show_fix', '1');
        })->where('status', '1')->get();
    }

    public function getAvailableModelAdditional()
    {
        return $this->model->where('flag_show_additional', '1')->where('status', '1')->get();
    }

    public function getAvailableModelFix()
    {
        return $this->model->where('flag_show_fix', '1')->where('status', '1')->get();
    }

    public function getByIdModel($id)
    {
        return $this->model->where('id_model', $id)->first();
    }
}
