<?php

namespace App\Repository\Eloquent;

use App\Models\SalesOrdering\DetailColourFixOrderUnit;

class DetailColourFixOrderRepository extends BaseRepository
{
    public function __construct(DetailColourFixOrderUnit $model)
    {
        parent::__construct($model);
    }

    public function getByIdDetail($id)
    {
        return $this->model->where(['status' => '1', 'id_detail_fix_order_unit' => $id])->get();
    }

    public function allocate($detail)
    {
        foreach($detail as $data) {
            $update = $this->model->where('id_detail_color_fix_order_unit', $data['id_detail'])
            ->update([
                'qty_input' => $data['qty_input'],
                'qty_diff' => $data['qty_diff'],
            ]);
        }

        return $update;
    }
}
