<?php

namespace App\Repository\Eloquent;

use App\Models\SalesOrdering\ModelColourMazda;
use Illuminate\Support\Facades\DB;

class ModelColourMazdaRepository extends BaseRepository
{
    public function __construct(ModelColourMazda $model)
    {
        parent::__construct($model);
    }

    /**
     * Get Data Pagination With Query View
     * @param string $viewName
     * @param array $arrayField
     * @param string $search
     * @param string $sortBy
     * @param string $sortDirection
     * @param int $perPage
     */
    public function getAvailableModelColourPagination(
        string $viewName,
        string $search = '',
        string $sortBy,
        string $sortDirection = 'asc',
        int $perPage
    ) {
        $arrayField = $this->searchableColumn;
        $countField = count($arrayField);

        $data = DB::table($viewName);
        $data = $data->where(function ($query) use ($arrayField, $countField, $search) {
            if ($countField >= 1) {
                for ($i = 0; $i <= $countField - 1; $i++) {
                    if ($i == 0) {
                        $query = $query->where($arrayField[$i], 'like', '%' . $search . '%');
                    } else {
                        $query = $query->orWhere($arrayField[$i], 'like', '%' . $search . '%');
                    }
                }
            }
        });
        $data = $data->where(function($query) {
            $query = $query->where('flag_show_additional_model', '1');
            $query = $query->orWhere('flag_show_fix_model', '1');
        });
        $data = $data->where('status', '1');
        $data = $data->orderBy($sortBy, $sortDirection);
        $data = $data->paginate($perPage);

        return $data;
    }

    /**
     * Get Data Checked With Query View
     * @param string $viewName
     * @param array $arrayField
     * @param string $search
     * @param string $sortBy
     * @param string $sortDirection
     * @param int $perPage
     */
    public function getAvailableModelColourChecked(
        string $viewName,
        string $search = '',
        string $sortBy,
        string $sortDirection = 'asc',
        int $perPage
    ) {
        $arrayField = $this->searchableColumn;
        $countField = count($arrayField);

        $data = DB::table($viewName);
        $data = $data->select($this->primaryKey);
        $data = $data->where(function ($query) use ($arrayField, $countField, $search) {
            if ($countField >= 1) {
                for ($i = 0; $i <= $countField - 1; $i++) {
                    if ($i == 0) {
                        $query = $query->where($arrayField[$i], 'like', '%' . $search . '%');
                    } else {
                        $query = $query->orWhere($arrayField[$i], 'like', '%' . $search . '%');
                    }
                }
            }
        });
        $data = $data->where(function($query) {
            $query = $query->where('flag_show_additional_model', '1');
            $query = $query->orWhere('flag_show_fix_model', '1');
        });
        $data = $data->where('status', '1');
        $data = $data->orderBy($sortBy, $sortDirection);
        $data = $data->paginate($perPage);

        return $data;
    }

    public function getByIdModelAdditional($id)
    {
        //return $this->model->where('id_model', $id)->where('status', '1')->where('flag_show_additional', '1')->get();

        return DB::table('view_model_colour_mazda')
        ->where('id_model', $id)
        ->where('status', '1')
        ->where('flag_show_additional_model', '1')
        ->where('flag_show_additional_model_colour', '1')
        ->get();
    }

    public function getByIdModelFix($id)
    {
        //return $this->model->where('id_model', $id)->where('status', '1')->where('flag_show_additional', '1')->get();

        return DB::table('view_model_colour_mazda')
        ->where('id_model', $id)
        ->where('status', '1')
        ->where('flag_show_fix_model', '1')
        ->where('flag_show_fix_model_colour', '1')
        ->get();
    }
}
