<?php

use App\Models\SalesOrdering\MasterDealer;
use App\Repository\Eloquent\BaseRepository;

class MasterDealerRepository extends BaseRepository {

    public function __construct(MasterDealer $model)
    {
        parent::__construct($model);
    }

}
