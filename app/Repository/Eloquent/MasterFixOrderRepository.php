<?php

namespace App\Repository\Eloquent;

use App\Models\SalesOrdering\DetailColourFixOrderUnit;
use App\Models\SalesOrdering\DetailFixOrderUnit;
use App\Models\SalesOrdering\MasterFixOrderUnit;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class MasterFixOrderRepository extends BaseRepository
{
    public function __construct(MasterFixOrderUnit $model)
    {
        parent::__construct($model);
    }

    public function getByIdDealer($idDealer)
    {
        return $this->model->where(['status' => '1', 'id_dealer' => $idDealer])->get();
    }

    public function getByIdDealerAndMonth($idDealer, $month)
    {
        return $this->model->where('status', '1')
            ->where('id_dealer', $idDealer)
            ->where('id_month', $month)
            ->get();
    }

    public function getByIdDealerAndMonthApprovalBM($idDealer, $month)
    {

        return $this->model->where('status', '1')
            ->where('id_dealer', $idDealer)
            ->where('id_month', $month)
            ->where('flag_send_approval_dealer', '1')
            ->whereIn('flag_approval_dealer', ['0', '1'])
            ->get();
    }

    public function getByIdDealerAndMonthConfirmationAtpm($idDealer, $month, $dealers)
    {

        return $this->model->where('status', '1')
            ->whereIn('id_dealer', $dealers)
            ->where('id_month', $month)
            ->where('flag_send_approval_dealer', '1')
            ->where('flag_approval_dealer', '1')
            ->where('flag_submit_to_atpm', '1')
            ->get();
    }

    public function getByIdDealerAndMonthAllocationAtpm($idDealer, $month, $dealers)
    {

        return $this->model->where('status', '1')
            //->whereIn('id_dealer', $dealers)
            ->where('id_month', $month)
            ->where('flag_send_approval_dealer', '1')
            ->where('flag_approval_dealer', '1')
            ->where('flag_submit_to_atpm', '1')
            ->where('flag_am_confirmation', '1')
            ->get();
    }

    public function getReportFixOrder($idDealer, $kdDealer, $dateStart, $dateEnd, $statusOrder)
    {
        if (session()->get('user')['status_atpm'] == 'atpm') {
            $data = $this->model;
            $data = $data->where('status', '1');

            if ($kdDealer != '-') {
                $data = $data->where('id_dealer', $kdDealer);
            }

            if ($dateStart != '' && $dateEnd != '') {
                $data = $data->where('date_save_order', '>=', $dateStart . ' 00:00:00')
                    ->where('date_save_order', '<=', $dateEnd . ' 23:59:59');
            }

            if ($statusOrder != '-') {
                if ($statusOrder == 'draft') {
                    $data = $data->whereIn('flag_send_approval_dealer', ['0', '2'])
                        ->where('flag_approval_dealer', '0')
                        ->where('flag_submit_to_atpm', '0')
                        ->where('flag_allocation', '0');
                } else if ($statusOrder == 'waiting-approval') {
                    $data = $data->where('flag_send_approval_dealer', '1')
                        ->where('flag_approval_dealer', '0')
                        ->where('flag_submit_to_atpm', '0')
                        ->where('flag_allocation', '0');
                } else if ($statusOrder == 'approved') {
                    $data = $data->where('flag_send_approval_dealer', '1')
                        ->where('flag_approval_dealer', '1')
                        ->where('flag_submit_to_atpm', '0')
                        ->where('flag_allocation', '0');
                } else if ($statusOrder == 'submitted') {
                    $data = $data->where('flag_send_approval_dealer', '1')
                        ->where('flag_approval_dealer', '1')
                        ->where('flag_submit_to_atpm', '1')
                        ->where('flag_allocation', '0');
                } else if ($statusOrder == 'allocated') {
                    $data = $data->where('flag_send_approval_dealer', '1')
                        ->where('flag_approval_dealer', '1')
                        ->where('flag_submit_to_atpm', '1')
                        ->where('flag_allocation', '1');
                }
            }

            return $data->get();
        } else {
            $data = $this->model;
            $data = $data->where('status', '1');
            $data = $data->where('id_dealer', $idDealer);

            if ($kdDealer != '-') {
                $data = $data->where('id_dealer', $kdDealer);
            }

            if ($dateStart != '' && $dateEnd != '') {
                $data = $data->where('date_save_order', '>=', $dateStart . ' 00:00:00')
                    ->where('date_save_order', '<=', $dateEnd . ' 23:59:59');
            }

            if ($statusOrder != '-') {
                if ($statusOrder == 'draft') {
                    $data = $data->whereIn('flag_send_approval_dealer', ['0', '2'])
                        ->where('flag_approval_dealer', '0')
                        ->where('flag_submit_to_atpm', '0')
                        ->where('flag_allocation', '0');
                } else if ($statusOrder == 'waiting-approval') {
                    $data = $data->where('flag_send_approval_dealer', '1')
                        ->where('flag_approval_dealer', '0')
                        ->where('flag_submit_to_atpm', '0')
                        ->where('flag_allocation', '0');
                } else if ($statusOrder == 'approved') {
                    $data = $data->where('flag_send_approval_dealer', '1')
                        ->where('flag_approval_dealer', '1')
                        ->where('flag_submit_to_atpm', '0')
                        ->where('flag_allocation', '0');
                } else if ($statusOrder == 'submitted') {
                    $data = $data->where('flag_send_approval_dealer', '1')
                        ->where('flag_approval_dealer', '1')
                        ->where('flag_submit_to_atpm', '1')
                        ->where('flag_allocation', '0');
                } else if ($statusOrder == 'allocated') {
                    $data = $data->where('flag_send_approval_dealer', '1')
                        ->where('flag_approval_dealer', '1')
                        ->where('flag_submit_to_atpm', '1')
                        ->where('flag_allocation', '1');
                }
            }

            return $data->get();
        }
    }

    public function createDealerOrder($dataMaster, $dataDetail, $idMonth)
    {
        $idDealer = session()->get('user')['id_dealer'];
        $where = array(
            'status' => '1',
            'id_dealer' => $idDealer,
            'id_month' => $idMonth
        );

        $countOrder = $this->model->where($where)->where('status', '1')->count();

        if ($countOrder == 0) {
            $insert = DB::transaction(function () use ($dataMaster, $dataDetail) {
                $insertMaster = DB::table('tbl_master_fix_order_unit')->insertGetId($dataMaster);

                foreach ($dataDetail as $detail) {
                    $dataInsertDetail = array(
                        'id_master_fix_order_unit' => $insertMaster,
                        'id_model' => $detail['id_model'],
                        'model_name' => $detail['model_name'],
                        'id_type' => $detail['id_type'],
                        'type_name' => $detail['type_name'],
                        'total_qty' => $detail['total_qty'],
                        'year_production' => $detail['year_production'],
                    );

                    $insertDetail = DB::table('tbl_detail_fix_order_unit')->insertGetId($dataInsertDetail);

                    foreach ($detail['selected_colour'] as $selectedColour) {
                        $dataInsertDetailColour = array(
                            'id_detail_fix_order_unit' => $insertDetail,
                            'id_colour' => $selectedColour['id_colour'],
                            'colour_name' => $selectedColour['colour_name'],
                            'qty' => $selectedColour['qty'],
                        );
                        $insertDetailColour = DB::table('tbl_detail_color_fix_order_unit')->insertGetId($dataInsertDetailColour);
                    }
                }

                return $insertMaster;
            }, 5);

            return $insert;
        } else {
            return true;
        }

    }

    public function updateDealerOrder($dataMaster, $dataDetail, $deleteIdDetail, $deleteIdDetailColour)
    {
        $update = DB::transaction(function () use ($dataMaster, $dataDetail, $deleteIdDetail, $deleteIdDetailColour) {
            $updateMaster = $this->model
                ->where('id_master_fix_order_unit', $dataMaster['id_master_fix_order_unit'])
                ->update($dataMaster);

            foreach ($dataDetail as $detail) {
                $insertDetail = '';
                if ($detail['id_detail_fix_order_unit']) {
                    $dateUpdateDetail = array(
                        'id_model' => $detail['id_model'],
                        'model_name' => $detail['model_name'],
                        'id_type' => $detail['id_type'],
                        'type_name' => $detail['type_name'],
                        'total_qty' => $detail['total_qty'],
                        'year_production' => $detail['year_production'],
                    );
                    $updateDetail = DetailFixOrderUnit::where('id_detail_fix_order_unit', $detail['id_detail_fix_order_unit'])
                        ->update($dateUpdateDetail);
                } else {
                    $dataInsertDetail = array(
                        'id_master_fix_order_unit' => $dataMaster['id_master_fix_order_unit'],
                        'id_model' => $detail['id_model'],
                        'model_name' => $detail['model_name'],
                        'id_type' => $detail['id_type'],
                        'type_name' => $detail['type_name'],
                        'total_qty' => $detail['total_qty'],
                        'year_production' => $detail['year_production'],
                    );

                    $insertDetail = DB::table('tbl_detail_fix_order_unit')->insertGetId($dataInsertDetail);
                }

                foreach ($detail['selected_colour'] as $selectedColour) {

                    if ($selectedColour['id_detail_color_fix_order_unit']) {
                        $dataUpdateDetailColour = array(
                            'id_colour' => $selectedColour['id_colour'],
                            'colour_name' => $selectedColour['colour_name'],
                            'qty' => (int)$selectedColour['qty'],
                        );
                        $updateDetailColour = DetailColourFixOrderUnit::where('id_detail_color_fix_order_unit', $selectedColour['id_detail_color_fix_order_unit'])
                            ->update($dataUpdateDetailColour);
                    } else {
                        $dataInsertDetailColour = array(
                            'id_detail_fix_order_unit' => $insertDetail ? $insertDetail : $detail['id_detail_fix_order_unit'],
                            'id_colour' => $selectedColour['id_colour'],
                            'colour_name' => $selectedColour['colour_name'],
                            'qty' => (int)$selectedColour['qty'],
                        );
                        $insertDetailColour = DB::table('tbl_detail_color_fix_order_unit')->insertGetId($dataInsertDetailColour);
                    }

                }
            }

            // Delete Detail from Edit Page
            foreach ($deleteIdDetail as $idDetail) {
                $deleteDetail = DetailFixOrderUnit::where('id_detail_fix_order_unit', $idDetail)
                    ->update(['status' => '0']);
            }

            // Delete Detail Colour from Edit Page
            foreach ($deleteIdDetailColour as $idDetailColour) {
                $deleteDetailColour = DetailColourFixOrderUnit::where('id_detail_color_fix_order_unit', $idDetailColour)
                    ->update(['status' => '0']);
            }

            return $updateMaster;
        }, 5);

        return $update;
    }

    public function updateSubmitAtpm(array $arrayId, $kodeTahunRepository)
    {
        foreach ($arrayId as $id) {
            $orderSequence = $kodeTahunRepository->getOrderSequenceFixOrder($id);
            $data = array(
                'flag_submit_to_atpm' => '1',
                'date_submit_atpm_order' => Carbon::now(),
                'no_order_atpm' => $orderSequence,
                'submitted_by' => session()->get('user')['username'],
                'nama_submitted_by' => session()->get('user')['nama_user']
            );

            $update = DB::transaction(function () use ($id, $data) {
                return $this->model->where($this->primaryKey, $id)->update($data);
            });

        }

        return $update;
    }
}
