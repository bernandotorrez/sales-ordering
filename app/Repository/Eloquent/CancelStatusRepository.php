<?php

namespace App\Repository\Eloquent;

use App\Models\SalesOrdering\CancelStatus;

class CancelStatusRepository extends BaseRepository
{
    public function __construct(CancelStatus $model)
    {
        parent::__construct($model);
    }
}
