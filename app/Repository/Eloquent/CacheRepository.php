<?php


namespace App\Repository\Eloquent;


use App\Models\Cache;

class CacheRepository extends BaseRepository
{
    public function __construct(Cache $model)
    {
        parent::__construct($model);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function firstOrCreate(array $data)
    {
        return $this->model->firstOrCreate($data);
    }
}
