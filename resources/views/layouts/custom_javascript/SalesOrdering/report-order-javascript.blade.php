<script id="details-template-additional" type="text/x-handlebars-template" data-turbolinks-track="reload">
    <h5 class="mt-2 text-center">Detail @{{no_order_dealer}} Order</h5>
    <table class="table table-hover details-table" id="detail-additional-@{{id_master_additional_order_unit}}">

    </table>
</script>

<script id="details-template-fix" type="text/x-handlebars-template" data-turbolinks-track="reload">
    <h5 class="mt-2 text-center">Detail @{{no_order_dealer}} Order</h5>
    <table class="table table-hover details-table" id="detail-fix-@{{id_master_fix_order_unit}}">

    </table>
</script>

<script id="sub-details-template-fix" type="text/x-handlebars-template" data-turbolinks-track="reload">
    <h5 class="mt-2 text-center">List @{{model_name}} Colour</h5>
    <table class="table table-hover details-table" id="sub-detail-fix-@{{id_detail_fix_order_unit}}">
        <thead>

        </thead>
    </table>
</script>

<script data-turbolinks-track="reload">
document.addEventListener('livewire:load', function () {
    //resetData()
    //var month = document.getElementById('id_month').value
    var url = window.location.href
    if (url.includes('report-order')) {
        var dateStart = document.getElementById('date_start').value
        var dateEnd = document.getElementById('date_end').value
        var statusOrder = document.getElementById('status_order').value
        var kdDealer = document.getElementById('kd_dealer').value
        var whichTable = document.getElementById('which-table').value

        var data = {
            dateStart: dateStart.split("-").reverse().join("-"),
            dateEnd: dateEnd.split("-").reverse().join("-"),
            statusOrder: statusOrder,
            kdDealer: kdDealer
        }

        showTable('additional', data)
    }

    flatpickr(document.getElementById('date_start'), {
        dateFormat: 'Y-m-d'
    });

    flatpickr(document.getElementById('date_end'), {
        dateFormat: 'Y-m-d'
    });
})

function search() {
    var dateStart = document.getElementById('date_start').value
    var dateEnd = document.getElementById('date_end').value
    var statusOrder = document.getElementById('status_order').value
    var kdDealer = document.getElementById('kd_dealer').value
    var whichTable = document.getElementById('which-table').value

    var data = {
        dateStart: dateStart.split("-").reverse().join("-"),
        dateEnd: dateEnd.split("-").reverse().join("-"),
        statusOrder: statusOrder,
        kdDealer: kdDealer
    }

    showTableTab(whichTable, data)
}

function getUrlAjax(status) {
    if (status == 'additional') {
        return "{{ url('datatable/additionalOrderJsonReport') }}"
    } else {
        return "{{ url('datatable/fixOrderJsonReport') }}"
    }
}

function getColumn(status) {
    if (status == 'additional') {
        return [{
                className: 'details-control-additional',
                data: null,
                searchable: false,
                orderable: false,
                defaultContent: ''
            },
            {
                data: 'id_master_additional_order_unit',
                data: 'id_master_additional_order_unit',
                title: 'ID',
            },
            {
                data: 'no_order_dealer',
                name: 'no_order_dealer',
                title: 'No Order Dealer'
            },
            {
                data: 'no_order_atpm',
                name: 'no_order_atpm',
                title: 'Order Sequence'
            },
            {
                data: 'nama_dealer',
                name: 'nama_dealer',
                title: 'Nama Dealer'
            },
            {
                data: 'user_order',
                name: 'user_order',
                title: 'User Order'
            },
            {
                data: 'month_order',
                name: 'month_order',
                title: 'Month Order'
            },
            {
                data: 'year_order',
                name: 'year_order',
                title: 'Year Order'
            },
            {
                data: 'status_progress',
                name: 'status_progress',
                title: 'Status Order'
            },
            {
                data: 'total_qty',
                name: 'total_qty',
                title: 'Total Qty'
            },
            {
                data: 'date_save_order',
                name: 'date_save_order',
                title: 'Date Draft',
            },
            {
                data: 'date_send_approval',
                name: 'date_send_approval',
                title: 'Date Send Approval',
            },
            {
                data: 'date_approval',
                name: 'date_approval',
                title: 'Date Approval',
            },
            {
                data: 'date_submit_atpm_order',
                name: 'date_submit_atpm_order',
                title: 'Date Submit ATPM',
            },
            {
                data: 'date_allocation_atpm',
                name: 'date_allocation_atpm',
                title: 'Date Allocation',
            },
        ]
    } else {
        return [{
                className: 'details-control-fix',
                data: null,
                searchable: false,
                orderable: false,
                defaultContent: ''
            },
            {
                data: 'id_master_fix_order_unit',
                data: 'id_master_fix_order_unit',
                title: 'ID',
            },
            {
                data: 'no_order_dealer',
                name: 'no_order_dealer',
                title: 'No Order Dealer'
            },
            {
                data: 'no_order_atpm',
                name: 'no_order_atpm',
                title: 'Order Sequence'
            },
            {
                data: 'nama_dealer',
                name: 'nama_dealer',
                title: 'Nama Dealer'
            },
            {
                data: 'user_order',
                name: 'user_order',
                title: 'User Order'
            },
            {
                data: 'id_month',
                name: 'id_month',
                title: 'Month Order'
            },
            {
                data: 'year_order',
                name: 'year_order',
                title: 'Year Order'
            },
            {
                data: 'status_progress',
                name: 'status_progress',
                title: 'Status Order'
            },
            {
                data: 'grand_total_qty',
                name: 'grand_total_qty',
                title: 'Grand Total Qty'
            },
            {
                data: 'date_save_order',
                name: 'date_save_order',
                title: 'Date Draft',
            },
            {
                data: 'date_send_approval',
                name: 'date_send_approval',
                title: 'Date Send Approval',
            },
            {
                data: 'date_approval',
                name: 'date_approval',
                title: 'Date Approval',
            },
            {
                data: 'date_submit_atpm_order',
                name: 'date_submit_atpm_order',
                title: 'Date Submit ATPM',
            },
            {
                data: 'date_allocation_atpm',
                name: 'date_allocation_atpm',
                title: 'Date Allocation',
            },
        ]
    }
}

function showTable(status, data) {
    // showHideButton(status)
    // disableButton()
    document.getElementById('which-table').value = status
    var templateAdditional = Handlebars.compile($("#details-template-additional").html());
    var templateFix = Handlebars.compile($("#details-template-fix").html());
    $.fn.dataTable.ext.errMode = 'throw';
    var table = $('#master-report-table').DataTable({
        "oLanguage": {
            "oPaginate": {
                "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
            },
            "sInfo": "Showing page _PAGE_ of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Search...",
            "sLengthMenu": "Results :  _MENU_",
        },
        "stripeClasses": [],
        processing: true,
        serverSide: true,
        destroy: true,
        ajax: {
            url: getUrlAjax(status),
            data: data
        },
        columnDefs: [{
            "visible": false,
            "targets": 1
        }],
        columns: getColumn(status)
    });

    // Add event listener for opening and closing details
    if (status == 'additional') {
        $('#master-report-table tbody').off('click', 'td.details-control-additional');
        $('#master-report-table tbody').on('click', 'td.details-control-additional', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var tableId = 'detail-additional-' + row.data().id_master_additional_order_unit;

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child(templateAdditional(row.data())).show();
                initTableAdditional(tableId, row.data());
                tr.addClass('shown');
                tr.next().find('td').addClass('no-padding bg-gray');
            }
        });
    } else {
        $('#master-report-table tbody').off('click', 'td.details-control-fix');
        $('#master-report-table tbody').on('click', 'td.details-control-fix', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var tableId = 'detail-fix-' + row.data().id_master_fix_order_unit;

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child(templateFix(row.data())).show();
                initTableFix(tableId, row.data());
                tr.addClass('shown');
                tr.next().find('td').addClass('no-padding bg-gray');
            }
        });
    }


}

function showTableTab(status, data) {
    // showHideButton(status)
    // disableButton()
    $('#master-fixorder-table').DataTable().destroy();
    $('#master-fixorder-table').html('');
    document.getElementById('which-table').value = status
    var templateAdditional = Handlebars.compile($("#details-template-additional").html());
    var templateFix = Handlebars.compile($("#details-template-fix").html());
    $.fn.dataTable.ext.errMode = 'throw';
    var table = $('#master-report-table').DataTable({
        "oLanguage": {
            "oPaginate": {
                "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
            },
            "sInfo": "Showing page _PAGE_ of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Search...",
            "sLengthMenu": "Results :  _MENU_",
        },
        "stripeClasses": [],
        processing: true,
        serverSide: true,
        destroy: true,
        ajax: {
            url: getUrlAjax(status),
            data: data ? data : {
                dateStart: '',
                dateEnd: '',
                statusOrder: '-',
                kdDealer: '-'
            }
        },
        columnDefs: [{
            "visible": false,
            "targets": 1
        }],
        columns: getColumn(status)
    });

    // Add event listener for opening and closing details
    if (status == 'additional') {
        $('#master-report-table tbody').off('click', 'td.details-control-additional');
        $('#master-report-table tbody').on('click', 'td.details-control-additional', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var tableId = 'detail-additional-' + row.data().id_master_additional_order_unit;

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child(templateAdditional(row.data())).show();
                initTableAdditional(tableId, row.data());
                tr.addClass('shown');
                tr.next().find('td').addClass('no-padding bg-gray');
            }
        });
    } else {
        $('#master-report-table tbody').off('click', 'td.details-control-fix');
        $('#master-report-table tbody').on('click', 'td.details-control-fix', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var tableId = 'detail-fix-' + row.data().id_master_fix_order_unit;

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child(templateFix(row.data())).show();
                initTableFix(tableId, row.data());
                tr.addClass('shown');
                tr.next().find('td').addClass('no-padding bg-gray');
            }
        });

    }


}

function initTableAdditional(tableId, data) {
    $('#' + tableId).DataTable({
        paging: false,
        "stripeClasses": [],
        processing: true,
        serverSide: true,
        searching: false,
        "ordering": true,
        "info": false,
        destroy: true,
        ajax: data.details_url,
        columns: [{
                data: 'model_name',
                name: 'model_name',
                title: 'Model Name',
            },
            {
                data: 'type_name',
                name: 'type_name',
                title: 'Type Name',
            },
            {
                data: 'colour_name',
                name: 'colour_name',
                title: 'Colour Name',
            },
            {
                data: 'year_production',
                name: 'year_production',
                title: 'Year production',
            },
            {
                data: 'qty',
                name: 'qty',
                title: 'Qty Order',
            },
            {
                data: 'qty_input',
                name: 'qty_input',
                title: 'Qty Input',
            },
            {
                data: 'qty_diff',
                name: 'qty_diff',
                title: 'Qty Remains',
            },
        ]
    })
}

function initTableFix(tableId, data) {
    var template = Handlebars.compile($("#sub-details-template-fix").html());
    var table = $('#' + tableId).DataTable({
        paging: false,
        "stripeClasses": [],
        processing: true,
        serverSide: true,
        searching: false,
        "ordering": true,
        "info": false,
        destroy: true,
        ajax: data.details_url,
        columns: [{
                className: 'sub-details-control-fix',
                data: null,
                searchable: false,
                orderable: false,
                defaultContent: ''
            },
            {
                data: 'model_name',
                name: 'model_name',
                title: 'Model Name'
            },
            {
                data: 'type_name',
                name: 'type_name',
                title: 'Type Name'
            },
            {
                data: 'total_qty',
                name: 'total_qty',
                title: 'Total Qty'
            },
        ]
    })

    // Add event listener for opening and closing details
    $('#detail-fix-' + data.id_master_fix_order_unit + ' tbody').off('click', 'td.sub-details-control-fix');
    $('#detail-fix-' + data.id_master_fix_order_unit + ' tbody').on('click', 'td.sub-details-control-fix', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var tableId = 'sub-detail-fix-' + row.data().id_detail_fix_order_unit;

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child(template(row.data())).show();
            initSubTableFix(tableId, row.data());
            tr.addClass('shown');
            tr.next().find('td').addClass('no-padding bg-gray');
        }
    });
}

function initSubTableFix(tableId, data) {
    $('#' + tableId).DataTable({
        paging: false,
        "stripeClasses": [],
        processing: true,
        serverSide: true,
        searching: false,
        "ordering": true,
        "info": false,
        destroy: true,
        ajax: data.details_url,
        columns: [{
                data: 'colour_name',
                name: 'colour_name',
                title: 'Colour Name'
            },
            {
                data: 'qty',
                name: 'qty',
                title: 'Qty Order'
            },
            {
                data: 'qty_input',
                name: 'qty_input',
                title: 'Qty Input',
            },
            {
                data: 'qty_diff',
                name: 'qty_diff',
                title: 'Qty Remains',
            },
        ]
    })
}
</script>
