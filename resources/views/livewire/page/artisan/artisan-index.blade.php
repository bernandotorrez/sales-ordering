<div class="row layout-top-spacing">

    <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
        <div class="widget-content-area br-4">

            <button type="button"
            class="btn btn-primary mr-4"
            id="configCache"
            wire:click.prevent="configCache"
            wire:loading.attr="disabled"> Config Cache
            </button>

            <button type="button"
            class="btn btn-primary mr-4"
            id="cacheClear"
            wire:click.prevent="cacheClear"
            wire:loading.attr="disabled"> Cache Clear
            </button>

            <button type="button"
            class="btn btn-primary mr-4"
            id="migrateFresh"
            wire:click.prevent="migrateFresh"
            wire:loading.attr="disabled"> Migrate Fresh
            </button>

            <button type="button"
            class="btn btn-primary mr-4"
            id="changeAPIDev"
            wire:click.prevent="changeAPIDev"
            wire:loading.attr="disabled"> Change API to DEV
            </button>

                <div class="text-center">
                    <p class=""></p>

                    <img alt="logo" class="img-fluid mb-4" style="max-height: 15%; max-width: 15%;" src="{{ asset('assets/img/mazda-logo.png') }}">


                </div>


        </div>
    </div>

</div>
