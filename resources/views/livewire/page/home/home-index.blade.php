<div class="row layout-top-spacing">

    <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
        <div class="widget-content-area br-4">

                <p class="" style="font-size: 16px;">
                    <strong>Welcome,</strong> {{session()->get('user')['nama_user']}} - {{session()->get('dealer')['nm_dealer']}}
                </p>

                <div class="text-center">
                    <p class=""></p>

                    <img alt="logo" class="img-fluid mb-4" style="max-height: 15%; max-width: 15%;" src="{{ asset('assets/img/mazda-logo.png') }}">


                </div>


        </div>
    </div>

</div>
