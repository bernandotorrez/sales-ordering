<div class="row layout-top-spacing">

    <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
        <div class="widget-content-area br-4">
            <div class="">

                @if(session()->has('action_message'))
                    {!! session('action_message') !!}
                @endif

                <button type="button"
                class="btn btn-primary mr-4"
                wire:click.prevent="downloadTemplate"> Download Template Excel
                </button>

                @if ($file)
                {{-- @dump($resultConvert) --}}
                @endif

                <form wire:submit.prevent="save">
                    <div class="row layout-top-spacing col-6 text-center offset-3">
                        <div wire:loading wire:target="file">Uploading...</div>
                    @error('file') <span class="error">{{ $message }}</span> @enderror
                    <div class="input-group mb-4">
                        <input type="file" class="form-control" wire:model="file">
                        <div class="input-group-append">
                          <button class="btn btn-primary" type="submit" wire:loading.attr="disabled">Upload</button>
                        </div>
                      </div>
                    </div>
                </form>

                @if($storageFile)
                <div class="text-center">
                    <button class="btn btn-primary" type="button" wire:click.prevent="downloadResult">Download Result</button>
                </div>
                @endif

            </div>
        </div>
    </div>

</div>

@push('scripts')
<script>
    Livewire.on('triggerDelete', function () {

        Swal.fire({
            icon: 'question',
            title: 'Are You Sure?',
            text: 'this Record will be deleted!',
            type: "warning",
            showCancelButton: true,
            confirmButtonText: 'Delete!'
        }).then((result) => {
            if (result.isConfirmed) {
                // call function deleteProcess() in Livewire Controller
                @this.deleteProcess()
            }
        });
    });
</script>
@endpush
