<div class="row layout-top-spacing">

    <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
        <div class="widget-content-area br-4">
            <div class="">

                <h4 class="title">{{ $pageTitle }}</h4>

                <br>

                @if(session()->has('action_message'))
                {!! session('action_message') !!}
                @endif

                @if(session()->get('user')['level_access'] == '1')
                <button type="button" class="btn btn-primary mr-4" id="addButton" wire:click.prevent="addForm"> Add
                </button>
                @endif

                <button type="button" class="btn btn-success mr-4" id="editButton" wire:click.prevent="editForm"
                    @if(count($checked) !=1) disabled @endif> Edit
                </button>

                @if(session()->get('user')['level_access'] == '1')
                <button type="button" class="btn btn-danger mr-4" id="deleteButton"
                    wire:click.prevent="$emit('triggerDelete')" @if(count($checked) <=0 ) disabled @endif> Delete
                </button>
                @endif

                @if(session()->get('user')['level_access'] == '1')
                <button type="button" class="btn btn-info" id="syncWrsButton"
                    wire:click.prevent="syncFromWRS" wire:loading.attr="disabled"> Sync From WRS
                </button>
                @endif

                <!-- Modal -->
                <div wire:ignore.self class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">{{ $pageTitle }}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                        viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                        stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                                        <line x1="18" y1="6" x2="6" y2="18"></line>
                                        <line x1="6" y1="6" x2="18" y2="18"></line>
                                    </svg>
                                </button>
                            </div>
                            <div class="modal-body">

                                <form>
                                    @if(session()->has('message_duplicate'))
                                    {!! session('message_duplicate') !!}
                                    @endif

                                    {{-- <div class="form-group mb-4">
                                        <label for="id_model_mazda">ID</label>
                                        <input type="text" class="form-control" id="id_model_mazda" maxlength="100"
                                            placeholder="Example : 1" wire:model.lazy="bind.id_model_mazda" readonly>
                                        @error('bind.id_model_mazda') <span class="error">{{ $message }}</span>
                                        @enderror
                                    </div> --}}

                                    <div class="form-group mb-4">
                                        <label for="id_model">ID Model</label>
                                        <input type="text" class="form-control" id="id_model" maxlength="100"
                                            placeholder="Example : 1" wire:model.lazy="bind.id_model"
                                            @if($isEdit == true && session()->get('user')['level_access'] != '1')
                                            readonly
                                            @endif
                                            >
                                        @error('bind.id_model') <span class="error">{{ $message }}</span>
                                        @enderror
                                    </div>

                                    <div class="form-group mb-4">
                                        <label for="name_model">Model Name</label>
                                        <input type="text" class="form-control" id="name_model" maxlength="100"
                                            placeholder="Example : 1" wire:model.lazy="bind.name_model"
                                            @if($isEdit == true && session()->get('user')['level_access'] != '1')
                                            readonly
                                            @endif
                                            >
                                        @error('bind.name_model') <span class="error">{{ $message }}</span>
                                        @enderror
                                    </div>

                                    <div class="form-group mb-4">
                                        <label for="flag_show_additional">Show in Additional</label>

                                        <select class="form-control" wire:model.lazy="bind.flag_show_additional">
                                            <option value="">- Choose -</option>
                                            <option value="0">0</option>
                                            <option value="1">1</option>
                                        </select>
                                        @error('bind.flag_show_additional') <span class="error">{{ $message }}</span>
                                        @enderror
                                    </div>

                                    <div class="form-group mb-4">
                                        <label for="flag_show_fix">Show in Fix</label>

                                        <select class="form-control" wire:model.lazy="bind.flag_show_fix">
                                            <option value="">- Choose -</option>
                                            <option value="0">0</option>
                                            <option value="1">1</option>
                                        </select>
                                        @error('bind.flag_show_fix') <span class="error">{{ $message }}</span>
                                        @enderror
                                    </div>

                                </form>
                            </div>
                            <div class="modal-footer">
                                <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i>
                                    Discard</button>
                                @if($isEdit)
                                <button type="button" class="btn btn-success" id="update"
                                    wire:click.prevent="editProcess" wire:offline.attr="disabled"> Update </button>
                                @else
                                <button type="button" class="btn btn-primary" id="submit"
                                    wire:click.prevent="addProcess" wire:offline.attr="disabled" @error('bind.*')
                                    disabled @enderror> Submit </button>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal -->

                <p></p>

                <div class="table-responsive mt-4">
                    <div class="d-flex">
                        <div class="p-2 align-content-center align-items-center" class="text-center">Per Page : </div>
                        <div class="p-2">
                            <select class="form-control" wire:model.lazy="perPageSelected">
                                @foreach($perPage as $page)
                                <option value="{{ $page }}">{{ $page }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="ml-auto p-2">
                            <input type="text" class="form-control" wire:model="search" placeholder="Search...">
                        </div>
                    </div>


                    <table class="table table-striped table-bordered" id="users-table">
                        <thead>
                            <th width="5%">
                                <label class="new-control new-checkbox checkbox-outline-primary  m-auto">
                                <input type="checkbox" class="new-control-input"
                                wire:model="allChecked"
                                wire:click="allChecked">
                                <span class="new-control-indicator"></span><span style="visibility:hidden">c</span>
                                </label>
                            </th>
                            <th width="10%">No</th>
                            <th wire:click="sortBy('id_model')">
                                <a href="javascript:void(0);">ID Model
                                    @include('livewire.datatable-icon', ['field' => 'id_model'])
                                </a>
                            </th>
                            <th wire:click="sortBy('name_model')">
                                <a href="javascript:void(0);">Model Name
                                    @include('livewire.datatable-icon', ['field' => 'name_model'])
                                </a>
                            </th>
                            <th wire:click="sortBy('flag_show_additional')">
                                <a href="javascript:void(0);">Show In <br>Additional
                                    @include('livewire.datatable-icon', ['field' => 'flag_show_additional'])
                                </a>
                            </th>
                            <th wire:click="sortBy('flag_show_fix')">
                                <a href="javascript:void(0);">Show In <br>Fix
                                    @include('livewire.datatable-icon', ['field' => 'flag_show_fix'])
                                </a>
                            </th>
                        </thead>
                        <tbody>
                            @foreach($dataMazdaModel as $data)
                            <tr>
                                <td>
                                    <label class="new-control new-checkbox checkbox-outline-primary  m-auto">
                                    <input type="checkbox"
                                    value="{{ $data->id_model_mazda }}"
                                    class="new-control-input"
                                    wire:model="checked">
                                    <span class="new-control-indicator"></span><span style="visibility:hidden">c</span>
                                    </label>
                                </td>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $data->id_model }}</td>
                                <td>{{ $data->name_model }}</td>
                                <td>{{ $data->flag_show_additional }}</td>
                                <td>{{ $data->flag_show_fix }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <div class="d-flex justify-content-center">
                        {{ $dataMazdaModel->links('livewire.pagination-links') }}
                    </div>

                </div>

            </div>
        </div>
    </div>

</div>

@push('scripts')
<script>
    Livewire.on('triggerDelete', function () {

        Swal.fire({
            icon: 'question',
            title: 'Are You Sure?',
            text: 'this Record will be deleted!',
            type: "warning",
            showCancelButton: true,
            confirmButtonText: 'Delete!'
        }).then((result) => {
            if (result.isConfirmed) {
                // call function deleteProcess() in Livewire Controller
                @this.deleteProcess()
            }
        });
    });
</script>
@endpush
