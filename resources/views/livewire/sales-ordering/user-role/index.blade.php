<div class="row layout-top-spacing">

    <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
        <div class="widget-content-area br-4">
            <div class="">

                @if(session()->has('action_message'))
                    {!! session('action_message') !!}
                @endif

                <button type="button"
                class="btn btn-danger mr-4"
                id="blacklistButton "
                wire:click.prevent="$emit('triggerBlacklist')"
                wire:loading.attr="disabled"
                @if(count($checked) != 1) disabled @endif
                > Blacklist
                </button>

                <button type="button"
                class="btn btn-primary mr-4"
                id="whitelistButton"
                wire:click.prevent="$emit('triggerWhitelist')"
                wire:loading.attr="disabled"
                @if(count($checked) != 1) disabled @endif
                > Whitelist
                </button>

                <button type="button"
                class="btn btn-success mr-4"
                id="reportExcelButton"
                wire:click.prevent="$emit('openModal')"
                wire:loading.attr="disabled"> Excel
                </button>

                <p></p>

                <!-- Modal -->
                <div wire:ignore.self class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">{{ $pageTitle }}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                        viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                        stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                                        <line x1="18" y1="6" x2="6" y2="18"></line>
                                        <line x1="6" y1="6" x2="18" y2="18"></line>
                                    </svg>
                                </button>
                            </div>
                            <div class="modal-body">

                                <form>

                                    <div class="form-group mb-4">
                                        <label for="status">Status</label>
                                        <select class="form-control" wire:model="bindReport.status">
                                            <option value="all">- Choose Status -</option>
                                            <option value="1">Blacklisted</option>
                                            <option value="0">Whitelisted</option>
                                        </select>
                                    </div>

                                </form>
                            </div>
                            <div class="modal-footer">
                                <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i>
                                    Discard</button>
                                <button type="button" class="btn btn-primary" id="submit"
                                    wire:click.prevent="exportExcel"
                                    wire:offline.attr="disabled"> Submit </button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal -->

                <div class="widget-content widget-content-area animated-underline-content">

                    <ul class="nav nav-tabs  mb-3" id="animateLine" role="tablist">
                        <li class="nav-item" wire:click.prevent="$set('tableName', 'whitelist')">
                            <a class="nav-link @if($tableName == 'whitelist') active @endif" id="animated-underline-home-tab" data-toggle="tab"
                                href="#animated-underline-home" role="tab" aria-controls="animated-underline-home"
                                aria-selected="true">
                                <i class="far fa-edit"></i> Whitelist</a>
                        </li>
                        <li class="nav-item" wire:click.prevent="$set('tableName', 'blacklist')">
                            <a class="nav-link @if($tableName == 'blacklist') active @endif" id="animated-underline-profile-tab" data-toggle="tab"
                                href="#animated-underline-profile" role="tab" aria-controls="animated-underline-profile"
                                aria-selected="false">
                                <i class="fas fa-user-clock"></i> Blacklist</a>
                        </li>
                    </ul>

                    <div class="table-responsive mt-4">
                        <div class="d-flex">
                            <div class="p-2 align-content-center align-items-center" class="text-center">Per Page : </div>
                            <div class="p-2">
                                <select class="form-control" wire:model.lazy="perPageSelected">
                                    @foreach($perPage as $page)
                                    <option value="{{ $page }}">{{ $page }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="ml-auto p-2 text-center alert alert-info" wire:loading
                                wire:target="car_model_paginate">Loading ... </div>
                            <div class="ml-auto p-2">
                                <input type="text" class="form-control" wire:model.debounce.500ms="search" placeholder="Search...">
                            </div>
                        </div>
                        <table class="table table-striped table-bordered" id="users-table">
                            <thead>
                                <th width="5%">
                                    <label class="new-control new-checkbox checkbox-outline-primary  m-auto">
                                    <input type="checkbox" class="new-control-input"
                                    wire:model="allChecked"
                                    wire:click="allChecked">
                                    <span class="new-control-indicator"></span><span style="visibility:hidden">c</span>
                                    </label>
                                </th>
                                <th width="10%">No</th>
                                <th wire:click="sortBy('id_dealer')">
                                    <a href="javascript:void(0);">Dealer Code
                                        @include('livewire.datatable-icon', ['field' => 'id_dealer'])
                                    </a>
                                </th>
                                <th wire:click="sortBy('nama_user')">
                                    <a href="javascript:void(0);">Nama User
                                        @include('livewire.datatable-icon', ['field' => 'nama_user'])
                                    </a>
                                </th>
                                <th wire:click="sortBy('username')">
                                    <a href="javascript:void(0);">Username
                                        @include('livewire.datatable-icon', ['field' => 'username'])
                                    </a>
                                </th>
                                <th wire:click="sortBy('id_dealer_level')">
                                    <a href="javascript:void(0);">Level
                                        @include('livewire.datatable-icon', ['field' => 'id_dealer_level'])
                                    </a>
                                </th>
                                <th wire:click="sortBy('email')">
                                    <a href="javascript:void(0);">Email
                                        @include('livewire.datatable-icon', ['field' => 'email'])
                                    </a>
                                </th>

                                <th width="10%">
                                    <a href="javascript:void(0);" class="text-center">Status Blacklist</a>
                                </th>

                                @if ($tableName == 'whitelist')
                                <th width="10%">
                                    <a href="javascript:void(0);" class="text-center">Remarks Whitelist</a>
                                </th>
                                <th width="10%">
                                    <a href="javascript:void(0);" class="text-center">Date Whitelist</a>
                                </th>
                                <th width="10%">
                                    <a href="javascript:void(0);" class="text-center">Whitelist By</a>
                                </th>
                                @else
                                <th width="10%">
                                    <a href="javascript:void(0);" class="text-center">Remarks Blacklist</a>
                                </th>
                                <th width="10%">
                                    <a href="javascript:void(0);" class="text-center">Date Blacklist</a>
                                </th>
                                <th width="10%">
                                    <a href="javascript:void(0);" class="text-center">Blacklist By</a>
                                </th>
                                @endif
                            </thead>
                            <tbody>
                                @foreach($dataUser as $data)
                                <tr>
                                    <td>
                                        <label class="new-control new-checkbox checkbox-outline-primary  m-auto">
                                        <input type="checkbox"
                                        value="{{ $data->id_user }}"
                                        class="new-control-input"
                                        wire:model="checked">
                                        <span class="new-control-indicator"></span><span style="visibility:hidden">c</span>
                                        </label>
                                    </td>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $data->id_dealer }}</td>
                                    <td>{{ $data->nama_user }}</td>
                                    <td>{{ $data->username }}</td>
                                    <td>{{ $data->id_dealer_level }}</td>
                                    <td>{{ $data->email }}</td>
                                    <td>
                                        @if($data->flag_blacklist == 0)
                                        <div class="text-center"><span class="badge badge-primary"> Whitelisted </span></i></div>
                                        @else
                                        <div class="text-center"><span class="badge badge-danger"> Blacklisted </span></i></div>
                                        @endif
                                    </td>
                                    @if ($tableName == 'whitelist')
                                    <td>
                                        @if($data->remarks_whitelist)
                                        <div class="text-center"><span class="badge badge-warning"> {{ $data->remarks_whitelist }} </span></i></div>
                                        @endif
                                    </td>
                                    <td>{{ ($data->date_whitelist ? date('d-m-Y H:i:s', strtotime($data->date_whitelist)) : '' ) }}</td>
                                    <td>{{ $data->whitelist_by }}</td>
                                    @else
                                    <td>
                                        @if($data->remarks_blacklist)
                                        <div class="text-center"><span class="badge badge-warning"> {{ $data->remarks_blacklist }} </span></i></div>
                                        @endif
                                    </td>
                                    <td>{{ ($data->date_blacklist ? date('d-m-Y H:i:s', strtotime($data->date_blacklist)) : '' ) }}</td>
                                    <td>{{ $data->blacklist_by }}</td>
                                    @endif

                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                        <div class="d-flex justify-content-center">
                            {{ $dataUser->links('livewire.pagination-links') }}
                        </div>

                    </div>
                </div>


            </div>
        </div>
    </div>

</div>

@push('scripts')
<script>
    Livewire.on('triggerDelete', function () {

        Swal.fire({
            icon: 'question',
            title: 'Are You Sure?',
            text: 'this Record will be deleted!',
            type: "warning",
            showCancelButton: true,
            confirmButtonText: 'Delete!'
        }).then((result) => {
            if (result.isConfirmed) {
                // call function deleteProcess() in Livewire Controller
                @this.deleteProcess()
            }
        });
    });

    Livewire.on('triggerBlacklist', function() {
        Swal.fire({
            title: "Blacklist this User (BM)?",
            text: "Please ensure and then confirm!",
            type: "info",
            icon: 'question',
            input: 'text',
            inputPlaceholder: 'Enter your Blacklist Reason',
            showCancelButton: true,
            reverseButtons: false,
            showLoaderOnConfirm: true,
            preConfirm: (remark) => {
                @this.remarkBlacklist = remark
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
            if (result.isConfirmed) {
                // call function blacklist() in Livewire Controller
                @this.blacklist()
            }
        })
    })

    Livewire.on('triggerWhitelist', function() {
        Swal.fire({
            title: "Whitelist this User (BM)?",
            text: "Please ensure and then confirm!",
            type: "info",
            icon: 'question',
            input: 'text',
            inputPlaceholder: 'Enter your Whitelist Reason',
            showCancelButton: true,
            reverseButtons: false,
            showLoaderOnConfirm: true,
            preConfirm: (remark) => {
                @this.remarkWhitelist = remark
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
            if (result.isConfirmed) {
                // call function whitelist() in Livewire Controller
                @this.whitelist()
            }
        })
    })
</script>
@endpush
