@push('css')
<link href="{{ asset('assets/css/dashboard/dash_1.css') }}" rel="stylesheet" type="text/css" />
@endpush

<div class="layout-px-spacing">

    <div class="page-header">
        <div class="page-title">
            <h3>Sales Dashboard</h3>
        </div>
    </div>

    <!-- Model -->
    <div class="row layout-top-spacing">

        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="widget">
                <form>
                    <div class="col-md-12">
                        <div class="form-row mb-4">
                            <div class="form-group col-md-2">
                                <label for="month">Month</label>
                                <select class="form-control" name="month" id="month" wire:model="bind.month">
                                    <option value="">- Choose Month -</option>
                                    @foreach($dataMasterMonth as $month)
                                    <option value="{{$month->id_month}}">{{$month->month}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label for="year">Year</label>
                                <select class="form-control" wire:model.lazy="bind.year">
                                    <option value="" selected>- Choose Year -</option>

                                    @for($i = 0;$i <= 2;$i++) 
                                    <option value="{{(date('Y')-$i)}}"> {{(date('Y')-$i)}} </option>
                                    @endfor
                                </select>
                            </div>

                            <!-- <div class="form-group col-md-1 text-center">
                                <label for="searchs" style="color: #0e1726;">aadsa</label>
                                <button type="button" class="btn btn-primary"
                                    style="padding: 0.58rem 1.25rem; font-size: 16px;" id="searchs">Search</button>
                            </div> -->
                        </div>

                    </div>

                </form>
            </div>
        </div>


        <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="widget widget-table-three">

                <div class="widget-heading">
                    <h5 class="">Top 10 Ordered Model (Additional Order)</h5>
                </div>

                <div class="widget-content">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>
                                        <div class="th-content">No</div>
                                    </th>

                                    <th>
                                        <div class="th-content">Model Name</div>
                                    </th>

                                    <th>
                                        <div class="th-content">Year Production</div>
                                    </th>

                                    <th>
                                        <div class="th-content">Total Ordered Qty</div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($dataTopOrderedModelAdditional as $modelAdditional)
                                <tr>
                                    <td>
                                        <div class="td-content product-name">{{$loop->iteration}}</div>
                                    </td>
                                    <td>
                                        <div class="td-content product-name">{{$modelAdditional->model_name}}</div>
                                    </td>
                                    <td>
                                        <div class="td-content product-name">{{$modelAdditional->year_production}}</div>
                                    </td>
                                    <td>
                                        <div class="td-content product-name">{{$modelAdditional->ordered_qty}}</div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="widget widget-table-three">

                <div class="widget-heading">
                    <h5 class="">Top 10 Ordered Type (Additional Order)</h5>
                </div>

                <div class="widget-content">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>
                                        <div class="th-content">No</div>
                                    </th>

                                    <th>
                                        <div class="th-content">Model Name</div>
                                    </th>

                                    <th>
                                        <div class="th-content">Type Name</div>
                                    </th>

                                    <th>
                                        <div class="th-content">Year Production</div>
                                    </th>

                                    <th>
                                        <div class="th-content">Total Ordered Qty</div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($dataTopOrderedTypeAdditional as $typeAdditional)
                                <tr>
                                    <td>
                                        <div class="td-content product-name">{{$loop->iteration}}</div>
                                    </td>
                                    <td>
                                        <div class="td-content product-name">{{$typeAdditional->model_name}}</div>
                                    </td>
                                    <td>
                                        <div class="td-content product-name">{{$typeAdditional->type_name}}</div>
                                    </td>
                                    <td>
                                        <div class="td-content product-name">{{$typeAdditional->year_production}}</div>
                                    </td>
                                    <td>
                                        <div class="td-content product-name">{{$typeAdditional->ordered_qty}}</div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <!-- Type -->
    <div class="row layout-top-spacing">


        <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="widget widget-table-three">

                <div class="widget-heading">
                    <h5 class="">Top 10 Ordered Model (Fix Order)</h5>
                </div>

                <div class="widget-content">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>
                                        <div class="th-content">No</div>
                                    </th>

                                    <th>
                                        <div class="th-content">Model Name</div>
                                    </th>

                                    <th>
                                        <div class="th-content">Year Production</div>
                                    </th>

                                    <th>
                                        <div class="th-content">Total Ordered Qty</div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($dataTopOrderedModelFix as $modelFix)
                                <tr>
                                    <td>
                                        <div class="td-content product-name">{{$loop->iteration}}</div>
                                    </td>
                                    <td>
                                        <div class="td-content product-name">{{$modelFix->model_name}}</div>
                                    </td>
                                    <td>
                                        <div class="td-content product-name">{{$modelFix->year_production}}</div>
                                    </td>
                                    <td>
                                        <div class="td-content product-name">{{$modelFix->ordered_qty}}</div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="widget widget-table-three">

                <div class="widget-heading">
                    <h5 class="">Top 10 Ordered Type (Fix Order)</h5>
                </div>

                <div class="widget-content">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>
                                        <div class="th-content">No</div>
                                    </th>

                                    <th>
                                        <div class="th-content">Model Name</div>
                                    </th>

                                    <th>
                                        <div class="th-content">Type Name</div>
                                    </th>

                                    <th>
                                        <div class="th-content">Year Production</div>
                                    </th>

                                    <th>
                                        <div class="th-content">Total Ordered Qty</div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($dataTopOrderedTypeFix as $typeFix)
                                <tr>
                                    <td>
                                        <div class="td-content product-name">{{$loop->iteration}}</div>
                                    </td>
                                    <td>
                                        <div class="td-content product-name">{{$typeFix->model_name}}</div>
                                    </td>
                                    <td>
                                        <div class="td-content product-name">{{$typeFix->type_name}}</div>
                                    </td>
                                    <td>
                                        <div class="td-content product-name">{{$typeFix->year_production}}</div>
                                    </td>
                                    <td>
                                        <div class="td-content product-name">{{$typeFix->ordered_qty}}</div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
