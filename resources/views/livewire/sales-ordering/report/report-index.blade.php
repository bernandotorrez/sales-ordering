<div class="row layout-top-spacing">

    <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
        <div class="widget-content-area br-4">
            <div class="">

                @if(session()->has('action_message'))
                {!! session('action_message') !!}
                @endif

                <!-- <button type="button" class="btn btn-primary mr-4" wire:click.prevent="addForm()"> Report Order
                </button> -->

                <!-- Modal -->
                <div wire:ignore.self class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-md" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">{{ $pageTitle }}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                        viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                        stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                                        <line x1="18" y1="6" x2="6" y2="18"></line>
                                        <line x1="6" y1="6" x2="18" y2="18"></line>
                                    </svg>
                                </button>
                            </div>
                            <div class="modal-body">

                                <form>

                                    <div class="form-group mb-4">
                                        <label for="month">Month Order</label>
                                        <select type="text" class="form-control" id="month"
                                            wire:model.lazy="bind.month">
                                            <option value="">- Choose Month Order -</option>
                                            <option value="all"> All </option>
                                            @foreach($dataMonth as $month)
                                            <option value="{{$month->id_month}}">
                                                {{$month->month}}
                                            </option>
                                            @endforeach
                                        </select>
                                        @error('bind.month') <span class="error">{{ $message }}</span>
                                        @enderror
                                    </div>

                                    <div class="form-group mb-4">
                                        <label for="year">Year Order</label>
                                        <select class="form-control" wire:model.lazy="bind.year">
                                            <option value="" selected>- Choose Year Order -</option>
                                            <option value="all"> All </option>
                                            @for($i = 0;$i <= 2;$i++) <option value="{{(date('Y')-$i)}}">
                                                {{(date('Y')-$i)}}</option>
                                                @endfor
                                        </select>
                                        @error('bind.year') <span class="error">{{ $message }}</span>
                                        @enderror
                                    </div>

                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary mr-4"
                                    wire:click.prevent="exportAdditionalOrder()"> Export Additional Order
                                </button>

                                <button type="button" class="btn btn-success mr-4"
                                    wire:click.prevent="exportFixOrder()"> Export Fix Order
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal -->

                <div class="widget-content widget-content-area animated-underline-content">

                    <ul class="nav nav-tabs  mb-3" id="animateLine" role="tablist">
                        <li class="nav-item" onclick="showTableTab('additional')">
                            <a class="nav-link active" id="animated-underline-home-tab" data-toggle="tab"
                                href="#animated-underline-home" role="tab" aria-controls="animated-underline-home"
                                aria-selected="true">
                                <i class="far fa-edit"></i> Additional</a>
                        </li>
                        <li class="nav-item" onclick="showTableTab('fix')">
                            <a class="nav-link" id="animated-underline-profile-tab" data-toggle="tab"
                                href="#animated-underline-profile" role="tab" aria-controls="animated-underline-profile"
                                aria-selected="false">
                                <i class="fas fa-user-clock"></i> Fix</a>
                        </li>
                    </ul>

                    <input type="hidden" id="which-table" />

                    <form>
                        <div class="col-md-12">
                            <div class="form-row mb-4">
                                <div class="form-group col-md-2">
                                    <label for="date_start">Date Start</label>
                                    <input type="text" class="form-control" name="date_start" id="date_start"
                                        placeholder="Date Start">
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="date_end">Date End</label>
                                    <input type="text" class="form-control" name="date_end" id="date_end"
                                        placeholder="Date End">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="kd_dealer">Dealer</label>

                                        @if(session()->get('user')['status_atpm'] == 'atpm')
                                            <select class="form-control basic" name="kd_dealer" id="kd_dealer">
                                            <option value="-">- Choose Dealer -</option>
                                            @foreach($dataDealer['data'] as $dealer)
                                            <option value="{{$dealer['kd_dealer']}}">{{$dealer['nm_dealer']}}</option>
                                            @endforeach
                                            </select>
                                        @else
                                            <input type="hidden" class="form-control" name="kd_dealer"
                                                id="kd_dealer" value="{{session()->get('user')['id_dealer']}}"/>

                                            <input type="text" class="form-control" readonly
                                                value="{{session()->get('dealer')['nm_dealer']}}"/>
                                        @endif

                                </div>
                                <div class="form-group col-md-3">
                                    <label for="status_order">Status</label>
                                    <select class="form-control" name="status_order" id="status_order">
                                        <option value="-">- Choose Status -</option>
                                        <option value="draft">Draft</option>
                                        <option value="waiting-approval">Waiting Approval</option>
                                        <option value="approved">Approved</option>
                                        <option value="submitted">Submitted</option>
                                        <option value="confirmed">Confirmed</option>
                                        <option value="allocated">Allocated</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-1 text-center">
                                    <label for="searchs" style="color: #0e1726;">aadsa</label>
                                    <button type="button" class="btn btn-primary"
                                        style="padding: 0.58rem 1.25rem; font-size: 16px;"
                                        id="searchs" onclick="search()">Search</button>
                                </div>

                                <div class="form-group col-md-1">
                                    <label for="searchs" style="color: #0e1726;">aadsa</label>
                                    <button type="button" class="btn btn-success"
                                        style="padding: 0.58rem 1.25rem; font-size: 16px;"
                                        wire:click.prevent="$emit('export')">Export</button>
                                </div>
                            </div>

                        </div>

                    </form>

                    <div class="table-responsive mt-4">
                        <table class="table table-striped table-bordered table-hover" id="master-report-table">


                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

<script data-turbolinks-track="reload">
    Livewire.on('export', function () {
    var dateStart = document.getElementById('date_start').value
    var dateEnd = document.getElementById('date_end').value
    var statusOrder = document.getElementById('status_order').value
    var kdDealer = document.getElementById('kd_dealer').value
    var whichTable = document.getElementById('which-table').value

    if(whichTable == 'additional') {
        @this.exportAdditionalOrder(dateStart.split("-").reverse().join("-"), dateEnd.split("-").reverse().join("-"), kdDealer, statusOrder)
    } else {
        @this.exportFixOrder(dateStart.split("-").reverse().join("-"), dateEnd.split("-").reverse().join("-"), kdDealer, statusOrder)
    }


})

document.addEventListener("DOMContentLoaded", () => {
    $('.basic').select2();
    Livewire.hook('message.processed', (message, component) => {
        location.reload()
    })
});

</script>
