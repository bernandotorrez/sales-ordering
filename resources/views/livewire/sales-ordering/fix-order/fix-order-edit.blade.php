<div class="row layout-top-spacing" wire:ignore.self>

    <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
        <div class="widget-content-area br-4">
            <div class="">

                @if(session()->has('action_message'))
                {!! session('action_message') !!}
                @endif

                <!-- Modal -->
                <div wire:ignore.self class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg animated slideInUp custo-slideInUp" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Add Colour</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                        viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                        stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                                        <line x1="18" y1="6" x2="6" y2="18"></line>
                                        <line x1="6" y1="6" x2="18" y2="18"></line>
                                    </svg>
                                </button>
                            </div>
                            <div class="modal-body">

                            @if ($errors->get('detailData.*.selected_colour.*.id_colour') || $errors->get('detailData.*.selected_colour.*.qty'))
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->get('detailData.*.selected_colour.*.id_colour') as $error)
                                            <li>{{$error[0]}}</li>
                                        @endforeach

                                        @foreach ($errors->get('detailData.*.selected_colour.*.qty') as $error)
                                            <li>{{$error[0]}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif


                                <div class="table-responsive" id="sub_detail">
                                    <table class="table table-striped table-bordered">

                                        <thead>
                                            <tr align="center">
                                                <th>No</th>
                                                <th>
                                                    <font class="text-danger">Colour Name *</font>
                                                </th>
                                                <th>
                                                    <font class="text-danger">Qty *</font>
                                                </th>
                                                <th>
                                                    <button type="button" class="btn btn-outline-success mb-2 mr-2" wire:click.prevent="addSubDetail">
                                                    <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor"
                                                        stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round"
                                                        class="css-i6dzq1">
                                                        <line x1="12" y1="5" x2="12" y2="19"></line>
                                                        <line x1="5" y1="12" x2="19" y2="12"></line>
                                                    </svg>
                                                    </button>
                                                </th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @if(isset($detailData[$idKey]))
                                            @foreach($detailData[$idKey]['selected_colour'] as $keySub => $dataSub)
                                            <tr align="center" wire:key="detail-{{ $keySub }}">
                                                <td>{{ $loop->iteration }} </td>
                                                <td>
                                                    <select class="form-control"
                                                        wire:model.lazy="detailData.{{$idKey}}.selected_colour.{{$keySub}}.id_colour">
                                                        <option value="" selected>- Choose Colour -</option>

                                                        @foreach($detailData[$idKey]['data_colour'] as $model)
                                                        <option value="{{$model['id_colour']}}">{{$model['name_colour_global']}}
                                                        </option>
                                                        @endforeach
                                                    </select>
                                                    @error('detailData.{{$idKey}}.selected_colour.{{$keySub}}.id_colour')
                                                    <span class="error">{{ $message }}</span>
                                                    @enderror
                                                </td>

                                                <td>
                                                    <input type="text" class="form-control text-right"
                                                    onkeypress="return isQtyKey(event)"
                                                    wire:model.lazy="detailData.{{$idKey}}.selected_colour.{{$keySub}}.qty">
                                                    @error('detailData.{{$idKey}}.selected_colour.{{$keySub}}.qty')
                                                    <span class="error">{{ $message }}</span>
                                                    @enderror
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-danger mb-2 mr-2" onclick="return confirm('Are you sure you want to Delete this?') || event.stopImmediatePropagation()"
                                                        wire:click.prevent="deleteSubDetail('{{$idKey}}', '{{$keySub}}', '{{$dataSub['id_detail_color_fix_order_unit']}}')" @if(count($detailData[$idKey]['selected_colour'])==1) disabled @endif>
                                                        <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor"
                                                            stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round"
                                                            class="css-i6dzq1">
                                                            <polyline points="3 6 5 6 21 6"></polyline>
                                                            <path
                                                                d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                                                            </path>
                                                            <line x1="10" y1="11" x2="10" y2="17"></line>
                                                            <line x1="14" y1="11" x2="14" y2="17"></line>
                                                        </svg>
                                                    </button>
                                                </td>
                                            </tr>
                                            @endforeach
                                            @endif
                                            <tr>
                                                <td colspan="2" align="right">Total Qty : </td>
                                                <td colspan="1">
                                                    <input type="text" class="form-control text-center"
                                                        id="total_qty" wire:model.lazy="detailData.{{$idKey}}.total_qty" readonly>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="text-right col-md-3 offset-9">
                                    <button class="btn btn-primary btn-block mr-4" data-dismiss="modal">
                                        OK
                                    </button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal -->

                <form id="form-add" class="section" wire:submit.prevent="editProcess">
                    <div class="info">
                        <h5 class="mb-4">{{ $pageTitle }}</h5>
                        <div class="row">
                            <div class="col-md-11 mx-auto">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="id_dealer">Dealer ID</label>
                                            <input type="text" class="form-control mb-4" id="id_dealer"
                                                placeholder="Dealer" value="{{session()->get('user')['id_dealer']}}"
                                                readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="order_no_dealer">
                                                <font class="text-danger">Order Number Dealer *</font>
                                            </label>
                                            <input type="text" class="form-control mb-4" id="order_number_dealer"
                                                placeholder="PO Number Dealer"
                                                wire:model.lazy="bind.order_number_dealer" readonly autofocus>
                                                @error('bind.order_number_dealer') <span class="error">{{ $message }}</span>
                                                @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="dealer_name">Dealer Name</label>
                                            <input type="text" class="form-control mb-4" id="dealer_name"
                                                placeholder="Dealer Name" value="{{$dealerName}}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="year_order">Year Order</label>
                                            <input type="text" class="form-control mb-4" id="year_order" placeholder=""
                                                value="{{ date('Y') }}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="id_user">User Dealer</label>
                                            <input type="text" class="form-control mb-4" id="id_user" placeholder=""
                                                value="{{ session()->get('user')['nama_user'] }}" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    @if(session()->has('volumeLockError'))
                        <div class="alert alert-danger">
                            {{session()->get('volumeLockError')}}
                            <br>
                            Volume / Qty before is {{$grandTotalQtyBefore}}
                            <br>
                            But Current Volume / Qty is {{$grandTotalQty}}
                        </div>
                    @endif

                    @if(session()->has('action_message_detail'))
                    {!! session('action_message_detail') !!}
                    @endif

                    <div class="table-responsive" id="detail">
                        <table class="table table-striped table-bordered">

                            <thead>
                                <tr align="center">
                                    <th>No</th>
                                    <th><font class="text-danger">Model Name *</font></th>
                                    <th><font class="text-danger">Type Name *</font></th>
                                    <th><font class="text-danger">Total Qty *</font></th>
                                    <th>
                                        <button type="button" class="btn btn-outline-success mb-2 mr-2" wire:click.prevent="addDetail"
                                        @if($flagOpen['flag_open_volume'] == '0') disabled @endif>
                                        <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor"
                                            stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round"
                                            class="css-i6dzq1">
                                            <line x1="12" y1="5" x2="12" y2="19"></line>
                                            <line x1="5" y1="12" x2="19" y2="12"></line>
                                        </svg>
                                        </button>
                                    </th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach($detailData as $key => $data)
                                <tr align="center" wire:key="master-{{ $key }}">
                                    <td>{{ $loop->iteration }} </td>
                                    <td>
                                        <select class="form-control" wire:model.lazy="detailData.{{$key}}.id_model"
                                            wire:change.prevent="updateDataType({{$key}}, $event.target.value)">
                                            <option value="" selected>- Choose Model -</option>

                                            @foreach($dataModel as $model)
                                            <option value="{{$model['id_model']}}">{{$model['name_model']}}</option>
                                            @endforeach
                                        </select>
                                        @error('detailData.'.$key.'.id_model') <span class="error">{{ $message }}</span>
                                        @enderror
                                    </td>

                                    <td>
                                        <select class="form-control" wire:model.lazy="detailData.{{$key}}.id_type">
                                            <option value="" selected>- Type | MSC Code -</option>

                                            @foreach($detailData[$key]['data_type'] as $type)
                                            <option value="{{$type['id_type_model']}}">
                                                {{$type['name_type']}} - {{$type['msc_code']}}
                                            </option>
                                            @endforeach
                                        </select>
                                        @error('detailData.'.$key.'.id_type') <span class="error">{{ $message }}</span>
                                        @enderror
                                    </td>


                                    <td>
                                        <input type="text" class="form-control text-center"
                                            wire:model.lazy="detailData.{{$key}}.total_qty" readonly>

                                    </td>

                                    <td>
                                        <button type="button" class="btn btn-outline-primary mb-2 mr-2"
                                            wire:click.prevent="$set('idKey', {{$key}})" data-toggle="modal" data-target="#exampleModal"
                                                data-backdrop="static" data-keyboard="false">
                                                <i class="fas fa-paint-brush text-info" style="font-size: 1.5em;"
                                                data-toggle="modal" data-target="#exampleModal"
                                                data-backdrop="static" data-keyboard="false"
                                                wire:click.prevent="$set('idKey', {{$key}})"></i>

                                        </button>

                                            <button type="button" class="btn btn-outline-danger mb-2 mr-2" onclick="return confirm('Are you sure you want to Delete this?') || event.stopImmediatePropagation()"
                                                wire:click.prevent="deleteDetail({{$key}}, '{{$data['id_detail_fix_order_unit']}}')" @if(count($detailData)==1 || $flagOpen['flag_open_volume'] == '0') disabled @endif>
                                            <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor"
                                                stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round"
                                                class="css-i6dzq1">
                                                <polyline points="3 6 5 6 21 6"></polyline>
                                                <path
                                                    d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                                                </path>
                                                <line x1="10" y1="11" x2="10" y2="17"></line>
                                                <line x1="14" y1="11" x2="14" y2="17"></line>
                                            </svg>
                                            </button>
                                    </td>
                                </tr>
                                @endforeach
                                <tr>
                                    <td colspan="3" align="right">Grand Total Order Qty : </td>
                                    <td colspan="1">
                                        <input type="text" class="form-control text-center" id="grand_total_qty"
                                            wire:model.lazy="grandTotalQty" readonly>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-11 text-left">
                        <button type="submit" class="btn btn-success mt-3 mr-2">Update</button>
                        <button class="btn btn-warning mt-3"
                            wire:click.prevent="goTo('{{route('fix-order.index')}}')">Back</a>

                    </div>

                </form>

            </div>
        </div>
    </div>

</div>
