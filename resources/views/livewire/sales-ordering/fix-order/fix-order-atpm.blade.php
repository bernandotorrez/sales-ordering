<div class="row layout-top-spacing">

    <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
        <div class="widget-content-area br-4">
            <div class="">

                @if(session()->has('action_message'))
                {!! session('action_message') !!}
                @endif

                <h6>Fix Order ATPM ( {{date('d M Y')}} )</h6>

                <!-- Modal -->
                <div wire:ignore.self class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-xl" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">{{ $pageTitle }}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                        viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                        stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                                        <line x1="18" y1="6" x2="6" y2="18"></line>
                                        <line x1="6" y1="6" x2="18" y2="18"></line>
                                    </svg>
                                </button>
                            </div>
                            <div class="modal-body">

                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped mb-4">
                                      <thead>
                                            <tr>
                                                <th>Model</th>
                                                <th>Type</th>
                                                <th>Colour</th>
                                                <th>Qty Order</th>
                                                <th>Qty Allocation</th>
                                                <th>Qty Remains</th>
                                            </tr>
                                        </thead>
                                        <tbody id="detailOrder">
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i>
                                    Discard</button>
                                <button type="button" class="btn btn-primary" id="submit" onclick="allocatedFixOrder()"> Submit </button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal -->

                <div class="widget-content widget-content-area animated-underline-content">

                    <input type="hidden" class="form-control" id="id_month" value="{{$rangeMonth[0]}}">
                    <input type="hidden" class="form-control" id="month_id_to"
                        value="{{$dataRangeMonth[0]->month_id_to}}">
                    <input type="hidden" class="form-control" id="checkBeforeOrAfter" value="{{$checkBeforeOrAfter}}">

                    <ul class="nav nav-tabs  mb-3" id="animateLine" role="tablist">

                        @foreach($dataMasterMonth as $key => $masterMonth)

                        @if(in_array($masterMonth->id_month, $rangeMonth))
                        <li class="nav-item" style="background-color: var(--green); color: #fff"
                            onclick="changeMonth({{$masterMonth->id_month}});">
                            <a class="nav-link {{($rangeMonth[0] == $key+1) ? 'active' : ''}}"
                                id="animated-underline-home-tab" data-toggle="tab" href="#animated-underline-home"
                                role="tab" aria-controls="animated-underline-home"
                                aria-selected="{{($rangeMonth[0] == $key+1) ? 'true' : 'false'}}">
                                <i class="far fa-calendar-alt"></i>
                                {{Str::substr($masterMonth->month, 0, 3)}}
                            </a>
                        </li>
                        @else
                        <li class="nav-item" style="background-color: var(--red); color: #fff"
                            onclick="showTableReadOnly({{$masterMonth->id_month}});">
                            <a class="nav-link" id="animated-underline-home-tab" data-toggle="tab"
                                href="#animated-underline-home" role="tab" aria-controls="animated-underline-home">
                                <i class="far fa-calendar-alt"></i>
                                {{Str::substr($masterMonth->month, 0, 3)}}
                            </a>
                        </li>
                        @endif

                        @endforeach
                    </ul>

                    <div id="button_ajax_load">
                        <!-- <button class="btn btn-warning mr-2" data-editableByJS="false" onclick="planningToAtpm()"
                            id="planningButtonAjaxLoad" disabled>Planning</button>

                        <button class="btn btn-danger mr-2" data-editableByJS="false" onclick="sendRevision()"
                            id="reviseButtonAjaxLoad" disabled>Revise</button>

                        <button class="btn btn-success mr-2" data-editableByJS="false" onclick="sendApproval()"
                            id="approveButtonAjaxLoad" disabled>Approve</button> -->

                        @if (session()->get('atpm_level') == 'Sales Administration Executive')
                        <button class="btn btn-primary mr-2" data-editableByJS="true" onclick="openModalAllocateFixOrder()"
                            id="allocateButtonAjaxLoad" disabled>Allocate</button>
                        @else
                        <button class="btn btn-primary mr-2" data-editableByJS="true" onclick="confirmAM()"
                            id="allocateButtonAjaxLoad" disabled>Confirm</button>
                        @endif

                    </div>

                    <div class="table-responsive mt-4">
                        <table class="table table-striped table-bordered table-hover" id="master-fixorder-atpm-table">


                        </table>
                    </div>

                </div>


            </div>
        </div>
    </div>

</div>
