<div class="row layout-top-spacing">

    <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
        <div class="widget-content-area br-4">
            <div class="">

                @if(session()->has('action_message'))
                {!! session('action_message') !!}
                @endif

                @if(session()->get('user')['level_access'] == '1')
                <button type="button" class="btn btn-primary mr-4" id="addButton" wire:click.prevent="addForm"> Add
                </button>
                @endif

                <button type="button" class="btn btn-success mr-4" id="editButton" wire:click.prevent="editForm"
                    @if(count($checked) !=1) disabled @endif> Edit
                </button>

                @if(session()->get('user')['level_access'] == '1')
                <button type="button" class="btn btn-danger" id="deleteButton"
                    wire:click.prevent="$emit('triggerDelete')" @if(count($checked) <=0 ) disabled @endif> Delete
                </button>
                @endif

                <p></p>

                <!-- Modal -->
                <div wire:ignore.self class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">{{ $pageTitle }}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                        viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                        stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                                        <line x1="18" y1="6" x2="6" y2="18"></line>
                                        <line x1="6" y1="6" x2="18" y2="18"></line>
                                    </svg>
                                </button>
                            </div>
                            <div class="modal-body">

                                <form>
                                    @if(session()->has('message_duplicate'))
                                    {!! session('message_duplicate') !!}
                                    @endif

                                    <div class="form-group mb-4">
                                        <label for="month">Month</label>

                                        @if(session()->get('user')['level_access'] == '1')
                                        <input type="text" class="form-control" id="month" maxlength="100"
                                            placeholder="Example : 1" wire:model.lazy="bind.month" >
                                        @else
                                        <input type="text" class="form-control" id="month" maxlength="100"
                                            placeholder="Example : 1" wire:model.lazy="bind.month" readonly>
                                        @endif
                                        @error('bind.month') <span class="error">{{ $message }}</span>
                                        @enderror
                                    </div>

                                    <div class="form-group mb-4">
                                        <label for="date_input_lock_start">Date Lock Start</label>
                                        <input type="text" class="form-control date" id="date_input_lock_start" maxlength="100"
                                            placeholder="Example : 1" wire:model.lazy="bind.date_input_lock_start">
                                        @error('bind.date_input_lock_start') <span class="error">{{ $message }}</span>
                                        @enderror
                                    </div>

                                    <div class="form-group mb-4">
                                        <label for="date_input_lock_end">Date Lock End</label>
                                        <input type="text" class="form-control date" id="date_input_lock_end" maxlength="100"
                                            placeholder="Example : 1" wire:model.lazy="bind.date_input_lock_end">
                                        @error('bind.date_input_lock_end') <span class="error">{{ $message }}</span>
                                        @enderror
                                    </div>

                                    <div class="form-group mb-4">
                                        <label for="operator_start">Operator Start</label>
                                        <input type="text" class="form-control date" id="operator_start" maxlength="10"
                                            placeholder="Example : 1" wire:model.lazy="bind.operator_start">
                                        @error('bind.operator_start') <span class="error">{{ $message }}</span>
                                        @enderror
                                    </div>

                                    <div class="form-group mb-4">
                                        <label for="operator_end">Operator End</label>
                                        <input type="text" class="form-control date" id="operator_end" maxlength="10"
                                            placeholder="Example : 1" wire:model.lazy="bind.operator_end">
                                        @error('bind.operator_end') <span class="error">{{ $message }}</span>
                                        @enderror
                                    </div>

                                </form>
                            </div>
                            <div class="modal-footer">
                                <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i>
                                    Discard</button>
                                @if($isEdit)
                                <button type="button" class="btn btn-success" id="update"
                                    wire:click.prevent="editProcess" wire:offline.attr="disabled"> Update </button>
                                @else
                                <button type="button" class="btn btn-primary" id="submit"
                                    wire:click.prevent="addProcess" wire:offline.attr="disabled" @error('bind.*')
                                    disabled @enderror> Submit </button>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal -->

                <p></p>

                <div class="table-responsive mt-4">
                    <div class="d-flex">
                        <div class="p-2 align-content-center align-items-center" class="text-center">Per Page : </div>
                        <div class="p-2">
                            <select class="form-control" wire:model.lazy="perPageSelected">
                                @foreach($perPage as $page)
                                <option value="{{ $page }}">{{ $page }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="ml-auto p-2 text-center alert alert-info" wire:loading
                            wire:target="car_model_paginate">Loading ... </div>
                        <div class="ml-auto p-2">
                            <input type="text" class="form-control" wire:model="search" placeholder="Search...">
                        </div>
                    </div>


                    <table class="table table-striped table-bordered" id="users-table">
                        <thead>
                            <th width="5%">
                                <label class="new-control new-checkbox checkbox-outline-primary  m-auto">
                                <input type="checkbox" class="new-control-input"
                                wire:model="allChecked"
                                wire:click="allChecked">
                                <span class="new-control-indicator"></span><span style="visibility:hidden">c</span>
                                </label>
                            </th>
                            <th width="10%">No</th>
                            <th wire:click="sortBy('month')">
                                <a href="javascript:void(0);">Month
                                    @include('livewire.datatable-icon', ['field' => 'month'])
                                </a>
                            </th>
                            <th wire:click="sortBy('date_input_lock_start')">
                                <a href="javascript:void(0);">Date Lock Start
                                    @include('livewire.datatable-icon', ['field' => 'date_input_lock_start'])
                                </a>
                            </th>
                            <th wire:click="sortBy('date_input_lock_end')">
                                <a href="javascript:void(0);">Date Lock End
                                    @include('livewire.datatable-icon', ['field' => 'date_input_lock_end'])
                                </a>
                            </th>
                            <th wire:click="sortBy('operator_start')">
                                <a href="javascript:void(0);">Operator Start
                                    @include('livewire.datatable-icon', ['field' => 'operator_start'])
                                </a>
                            </th>
                            <th wire:click="sortBy('operator_end')">
                                <a href="javascript:void(0);">Operator End
                                    @include('livewire.datatable-icon', ['field' => 'operator_end'])
                                </a>
                            </th>
                        </thead>
                        <tbody>
                            @foreach($dataMasterMonth as $data)
                            <tr>
                                <td>
                                    <label class="new-control new-checkbox checkbox-outline-primary  m-auto">
                                    <input type="checkbox"
                                    value="{{ $data->id_month }}"
                                    class="new-control-input"
                                    wire:model="checked">
                                    <span class="new-control-indicator"></span><span style="visibility:hidden">c</span>
                                    </label>
                                </td>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $data->month }}</td>
                                <td>{{ $data->date_input_lock_start }}</td>
                                <td>{{ $data->date_input_lock_end }}</td>
                                <td>{{ $data->operator_start }}</td>
                                <td>{{ $data->operator_end }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <div class="d-flex justify-content-center">
                        {{ $dataMasterMonth->links('livewire.pagination-links') }}
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>

@push('css')
<style>
    .flatpickr-calendar.open {
        display: inline-block;
        z-index: 1053 !important;
    }
</style>
@endpush

@push('scripts')
<script>
    Livewire.on('triggerDelete', function () {

        Swal.fire({
            icon: 'question',
            title: 'Are You Sure?',
            text: 'this Record will be deleted!',
            type: "warning",
            showCancelButton: true,
            confirmButtonText: 'Delete!'
        }).then((result) => {
            if (result.isConfirmed) {
                // call function deleteProcess() in Livewire Controller
                @this.deleteProcess()
            }
        });
    });

    document.addEventListener('livewire:load', function() {
        flatpickr('#date_input_lock_start', {
            dateFormat: 'Y-m-d',
        });

        flatpickr('#date_input_lock_end', {
            dateFormat: 'Y-m-d',
        });
    })
</script>
@endpush
