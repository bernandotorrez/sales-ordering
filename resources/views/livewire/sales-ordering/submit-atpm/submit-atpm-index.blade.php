<div class="row layout-top-spacing">

    <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
        <div class="widget-content-area br-4">
            <div class="">

                @if(session()->has('action_message'))
                {!! session('action_message') !!}
                @endif

                <div class="widget-content widget-content-area animated-underline-content">

                    <ul class="nav nav-tabs  mb-3" id="animateLine" role="tablist">
                        <li class="nav-item" onclick="showTableTab('draft')">
                            <a class="nav-link" id="animated-underline-home-tab" data-toggle="tab"
                                href="#animated-underline-home" role="tab" aria-controls="animated-underline-home"
                                aria-selected="false">
                                <i class="far fa-edit"></i> Draft <br> (Dealer)</a>
                        </li>

                        <!-- TODO: harus di pindahin Active nya dan aria-selected = true -->
                        <li class="nav-item" onclick="showTableTab('waiting_approval_dealer_principle')">
                            <a class="nav-link" id="animated-underline-profile-tab" data-toggle="tab"
                                href="#animated-underline-profile" role="tab" aria-controls="animated-underline-profile"
                                aria-selected="false">
                                <i class="fas fa-user-clock"></i> Waiting Approval <br> (Dealer)</a>
                        </li>
                        <li class="nav-item" onclick="showTableTab('approval_dealer_principle')">
                            <a class="nav-link" id="animated-underline-contact-tab" data-toggle="tab"
                                href="#animated-underline-contact" role="tab" aria-controls="animated-underline-contact"
                                aria-selected="false">
                                <i class="fas fa-user-check"></i> Approved <br> (Dealer)</a>
                        </li>
                        <li class="nav-item" onclick="showTableTab('submitted_atpm')">
                            <a class="nav-link active" id="animated-underline-contact-tab" data-toggle="tab"
                                href="#animated-underline-contact" role="tab" aria-controls="animated-underline-contact"
                                aria-selected="true">
                                <i class="fas fa-file-import"></i> Submitted <br> (ATPM)</a>
                        </li>
                        @if (session()->get('atpm_level') == 'Sales Administration Executive')
                        <li class="nav-item" wire:click.prevent="goTo('{{route('am-confirmation.index')}}')">
                            <a class="nav-link" id="animated-underline-contact-tab" data-toggle="tab"
                                href="#animated-underline-contact" role="tab" aria-controls="animated-underline-contact"
                                aria-selected="false">
                                <i class="fas fa-clipboard-check"></i> Allocation <br> (ATPM)</a>
                        </li>
                        @else
                        <li class="nav-item" onclick="showTableTab('am_confirmation')">
                            <a class="nav-link" id="animated-underline-contact-tab" data-toggle="tab"
                                href="#animated-underline-contact" role="tab" aria-controls="animated-underline-contact"
                                aria-selected="false">
                                <i class="fas fa-clipboard-check"></i> Allocation <br> (ATPM)</a>
                        </li>
                        @endif

                        <li class="nav-item" wire:click.prevent="goTo('{{url('sales/atpm/allocated-atpm')}}')">
                            <a class="nav-link" id="animated-underline-contact-tab" data-toggle="tab"
                                href="#animated-underline-contact" role="tab" aria-controls="animated-underline-contact"
                                aria-selected="false">
                                <i class="fas fa-shipping-fast"></i> Allocated <br> (ATPM)</a>
                        </li>
                        <li class="nav-item" onclick="showTableTab('canceled')">
                            <a class="nav-link" id="animated-underline-contact-tab" data-toggle="tab"
                                href="#animated-underline-contact" role="tab" aria-controls="animated-underline-contact"
                                aria-selected="false">
                                <i class="fas fa-user-times"></i> Canceled </a>
                        </li>
                    </ul>

                    <!-- <a class="btn btn-primary mr-2" id="addButton" href="{{route('additional-order.add')}}">Add</a> -->

                    <!-- <button type="button" class="btn btn-success mr-2" id="editButton"
                        wire:click.prevent="goTo($event.target.value)" value="" disabled>Amend</button> -->
                    @if (session()->get('atpm_level') == 'Area Manager')
                    <button type="button" class="btn btn-primary mr-2" id="sendApprovalButton"
                        onclick="sendApproval()"
                        disabled>Confirmation</button>

                    <button type="button" class="btn btn-success mr-2" id="sendReviseButton"
                        onclick="sendRevision()"
                        disabled>Revise</button>

                    <button type="button" class="btn btn-danger mr-2" id="sendCancelButton"
                        onclick="sendCancel()"
                        disabled>Cancel</button>
                    @else
                    <div style="display: none;">
                    <button type="button" class="btn btn-primary mr-2" id="sendApprovalButton"
                        onclick="sendApproval()"
                        disabled>Confirmation</button>

                    <button type="button" class="btn btn-success mr-2" id="sendReviseButton"
                        onclick="sendRevision()"
                        disabled>Revise</button>

                    <button type="button" class="btn btn-danger mr-2" id="sendCancelButton"
                        onclick="sendCancel()"
                        disabled>Cancel</button>
                    </div>
                    @endif

                    <div class="table-responsive mt-4">
                        <table class="table table-striped table-bordered table-hover" id="master-additional-table">

                            <div class="form-group col-md-3 mb-4" id="dropdown_cancel_status">
                                <label for="parent_position">Cancel Status</label>
                                <select name="cancel_status" id="cancel_status" class="form-control"
                                    onchange="showTableCancel('canceled', this.value)">
                                        <option value="">- Choose Cancel Status -</option>
                                            @foreach($dataCancelStatus as $key => $cancelStatus)
                                                <option value="{{$cancelStatus->id_cancel_status}}">
                                                    {{$cancelStatus->nama_cancel_status}}</option>
                                            @endforeach
                                </select>
                            </div>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
