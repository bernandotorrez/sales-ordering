<div class="row layout-top-spacing">

    <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
        <div class="widget-content-area br-4">
            <div class="">

                @if(session()->has('action_message'))
                {!! session('action_message') !!}
                @endif

                <!-- Modal -->
                <div wire:ignore.self class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-xl" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">{{ $pageTitle }}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                        viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                        stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                                        <line x1="18" y1="6" x2="6" y2="18"></line>
                                        <line x1="6" y1="6" x2="18" y2="18"></line>
                                    </svg>
                                </button>
                            </div>
                            <div class="modal-body">

                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped mb-4">
                                      <thead>
                                            <tr>
                                                <th>Model</th>
                                                <th>Type</th>
                                                <th>Colour</th>
                                                <th>Qty Order</th>
                                                <th>Qty Allocation</th>
                                                <th>Qty Remains</th>
                                            </tr>
                                        </thead>
                                        <tbody id="detailOrder">
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i>
                                    Discard</button>
                                <button type="button" class="btn btn-primary" id="submit" onclick="allocated()"> Submit </button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal -->

                <div class="widget-content widget-content-area animated-underline-content">

                    <ul class="nav nav-tabs  mb-3" id="animateLine" role="tablist">
                        <li class="nav-item" onclick="showTableTab('draft')">
                            <a class="nav-link" id="animated-underline-home-tab" data-toggle="tab"
                                href="#animated-underline-home" role="tab" aria-controls="animated-underline-home"
                                aria-selected="false">
                                <i class="far fa-edit"></i> Draft <br> (Dealer)</a>
                        </li>

                        <!-- TODO: harus di pindahin Active nya dan aria-selected = true -->
                        <li class="nav-item" onclick="showTableTab('waiting_approval_dealer_principle')">
                            <a class="nav-link" id="animated-underline-profile-tab" data-toggle="tab"
                                href="#animated-underline-profile" role="tab" aria-controls="animated-underline-profile"
                                aria-selected="false">
                                <i class="fas fa-user-clock"></i> Waiting Approval <br> (Dealer)</a>
                        </li>
                        <li class="nav-item" onclick="showTableTab('approval_dealer_principle')">
                            <a class="nav-link" id="animated-underline-contact-tab" data-toggle="tab"
                                href="#animated-underline-contact" role="tab" aria-controls="animated-underline-contact"
                                aria-selected="false">
                                <i class="fas fa-user-check"></i> Approved <br> (Dealer)</a>
                        </li>
                        <li class="nav-item" wire:click.prevent="goTo('{{route('submit-atpm.index')}}')">
                            <a class="nav-link" id="animated-underline-contact-tab" data-toggle="tab"
                                href="#animated-underline-contact" role="tab" aria-controls="animated-underline-contact"
                                aria-selected="true">
                                <i class="fas fa-file-import"></i> Submitted <br> (ATPM)</a>
                        </li>
                        @if (session()->get('atpm_level') == 'Sales Administration Executive')
                        <li class="nav-item" wire:click.prevent="goTo('{{route('am-confirmation.index')}}')">
                            <a class="nav-link active" id="animated-underline-contact-tab" data-toggle="tab"
                                href="#animated-underline-contact" role="tab" aria-controls="animated-underline-contact"
                                aria-selected="false">
                                <i class="fas fa-clipboard-check"></i> Allocation <br> (ATPM)</a>
                        </li>
                        @else
                        <li class="nav-item" onclick="showTableTab('am_confirmation')">
                            <a class="nav-link active" id="animated-underline-contact-tab" data-toggle="tab"
                                href="#animated-underline-contact" role="tab" aria-controls="animated-underline-contact"
                                aria-selected="false">
                                <i class="fas fa-clipboard-check"></i> Allocation <br> (ATPM)</a>
                        </li>
                        @endif
                        <li class="nav-item" wire:click.prevent="goTo('{{url('sales/atpm/allocated-atpm')}}')">
                            <a class="nav-link" id="animated-underline-contact-tab" data-toggle="tab"
                                href="#animated-underline-contact" role="tab" aria-controls="animated-underline-contact"
                                aria-selected="false">
                                <i class="fas fa-shipping-fast"></i> Allocated <br> (ATPM)</a>
                        </li>
                        <li class="nav-item" onclick="showTableTab('canceled')">
                            <a class="nav-link" id="animated-underline-contact-tab" data-toggle="tab"
                                href="#animated-underline-contact" role="tab" aria-controls="animated-underline-contact"
                                aria-selected="false">
                                <i class="fas fa-user-times"></i> Canceled </a>
                        </li>
                    </ul>

                    <!-- <a class="btn btn-primary mr-2" id="addButton" href="{{route('additional-order.add')}}">Add</a> -->

                    <!-- <button type="button" class="btn btn-success mr-2" id="editButton"
                        wire:click.prevent="goTo($event.target.value)" value="" disabled>Amend</button> -->

                    <button type="button" class="btn btn-primary mr-2" id="sendApprovalButton"
                        onclick="allocation()"
                        disabled>Allocate</button>

                    {{-- <button type="button" class="btn btn-danger mr-2" id="sendCancelButton"
                        onclick="sendCancel()"
                        disabled>Cancel</button> --}}

                    <button type="button" class="btn btn-success mr-2" id="sendReviseButton"
                        onclick="sendRevision()"
                        disabled>Revise</button>

                    <button type="button" class="btn btn-danger mr-2" id="sendCancelButton"
                        onclick="sendCancel()"
                        disabled>Cancel</button>

                    <!-- <button type="button" class="btn btn-danger mr-2" id="deleteButton" onclick="deleteProcess()"
                        disabled>Delete</button> -->

                    <div class="table-responsive mt-4">
                        <table class="table table-striped table-bordered table-hover" id="master-additional-table">

                            <div class="form-group col-md-3 mb-4" id="dropdown_cancel_status">
                                <label for="parent_position">Cancel Status</label>
                                <select name="cancel_status" id="cancel_status" class="form-control"
                                    onchange="showTableCancel('canceled', this.value)">
                                        <option value="">- Choose Cancel Status -</option>
                                            @foreach($dataCancelStatus as $key => $cancelStatus)
                                                <option value="{{$cancelStatus->id_cancel_status}}">
                                                    {{$cancelStatus->nama_cancel_status}}</option>
                                            @endforeach
                                </select>
                            </div>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
