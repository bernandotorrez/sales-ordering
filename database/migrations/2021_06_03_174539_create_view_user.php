<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Staudenmeir\LaravelMigrationViews\Facades\Schema;

class CreateViewUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $query = 'SELECT tu.*, tug.nama_group , tmd.dealer_name, tmd.address, tmd.phone, tmd.fax, tmd.email AS email_dealer
        FROM tbl_user tu
        INNER JOIN tbl_user_group tug ON tug.id_user_group = tu.id_user_group
        LEFT JOIN tbl_master_dealer tmd ON tmd.id_dealer = tu.id_dealer
        WHERE tu.status = "1"';

        Schema::createOrReplaceView('view_user', $query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropViewIfExists('view_user');
    }
}
