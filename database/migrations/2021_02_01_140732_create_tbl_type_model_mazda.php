<?php

use App\Models\SalesOrdering\TypeModelMazda;
use App\Traits\WithWrsApi;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Schema;

class CreateTblTypeModelMazda extends Migration
{
    use WithWrsApi;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tbl_type_model_mazda')) {
            Schema::create('tbl_type_model_mazda', function (Blueprint $table) {
                $table->id('id_type_model_mazda');
                $table->string('id_type_model', 100);
                $table->string('id_model', 25);
                $table->string('name_type', 250);
                $table->integer('cylinder')->nullable();
                $table->string('msc_code', 25)->nullable();
                $table->integer('price')->nullable();
                $table->string('tpt', 100)->nullable();
                $table->string('sut', 100)->nullable();
                $table->enum('status', ['0', '1'])->default('1');
                $table->enum('flag_show_additional', ['0', '1'])->default('0');
                $table->enum('flag_show_fix', ['0', '1'])->default('0');
                $table->timestamps();
            });
        }

        $this->insertData();
    }

    private function insertData()
    {
        $data = Http::withHeaders([
            'X-Auth-Token' => Config::get('constants.api_token')
        ])
        ->get($this->wrsApi.'/type-model')
        ->json();

        foreach($data['data'] as $model)
        {
            $checkDuplicate = TypeModelMazda::firstWhere('id_type_model', $model['kd_type']);

            if(!$checkDuplicate) {
                TypeModelMazda::create([
                    'id_type_model' => $model['kd_type'],
                    'id_model' => $model['fk_model'],
                    'name_type' => $model['nm_type'],
                    'cylinder' => $model['isi_silinder'],
                    'msc_code' => $model['msc_code'],
                    'price' => $model['harga'],
                    'tpt' => $model['tpt'],
                    'sut' => $model['sut'],
                    'flag_show_additional' => '1',
                    'flag_show_fix' => '1',
                    'status' => '1'
                ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_type_model_mazda');
    }
}
