<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Staudenmeir\LaravelMigrationViews\Facades\Schema;

class CreateViewChildMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $query = 'SELECT tcm.*, tpm.nama_parent_menu, tug.nama_group
        FROM tbl_child_menu tcm
        INNER JOIN tbl_parent_menu tpm ON tpm.id_parent_menu = tcm.id_parent_menu
        INNER JOIN tbl_user_group tug ON tug.id_user_group = tcm.id_user_group
        WHERE tcm.status = "1"';

        Schema::createOrReplaceView('view_child_menu', $query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropViewIfExists('view_child_menu');
    }
}
