<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Staudenmeir\LaravelMigrationViews\Facades\Schema;

class AlterViewMasterAdditonalOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $query = 'SELECT maou.*, tdaou.id_model, tdaou.model_name, tdaou.id_colour, tdaou.colour_name, tdaou.id_type, tdaou.type_name,
        tdaou.qty, tdaou.year_production, tdaou.qty_input, tdaou.qty_diff
        FROM tbl_master_additional_order_unit maou
        INNER JOIN tbl_detail_additional_order_unit tdaou
        WHERE maou.status = "1" AND tdaou.status = "1"';
        Schema::createOrReplaceView('view_additional_order', $query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropViewIfExists('view_additional_order');
    }
}
