<?php

use App\Models\UserGroup;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblUserGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('tbl_user_group')) {
            Schema::create('tbl_user_group', function (Blueprint $table) {
                $table->id('id_user_group');
                $table->string('nama_group', 100);
                $table->enum('status', ['0', '1'])->default(1);
                $table->timestamps();
            });

            $this->insertUserGroup();
        }

        
    }

    public function insertUserGroup()
    {
        UserGroup::create([
            'id_user_group' => 1,
            'nama_group' => 'admin001',
        ]);
        UserGroup::create([
            'id_user_group' => 2,
            'nama_group' => 'atpm001',
        ]);
        UserGroup::create([
            'id_user_group' => 3,
            'nama_group' => 'dealer001',
        ]);
        UserGroup::create([
            'id_user_group' => 4,
            'nama_group' => 'dealeradmin001',
        ]);
        UserGroup::create([
            'id_user_group' => 5,
            'nama_group' => 'dealerbm001',
        ]);
        UserGroup::create([
            'id_user_group' => 6,
            'nama_group' => 'adminatpm001',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_user_group');
    }
}
