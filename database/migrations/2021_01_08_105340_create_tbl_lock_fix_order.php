<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblLockFixOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tbl_lock_fix_order')) {
            Schema::create('tbl_lock_fix_order', function (Blueprint $table) {
                $table->id('id_lock_fix_order');
                $table->string('input_month', 2)->comment('Bulan saat input Order');  
                $table->string('month_id_to', 2)->comment('Bulan yang di Lock');
                $table->bigInteger('id_month')->comment('Bulan berjalan');
                $table->string('id_dealer', 10);
                $table->tinyInteger('flag_open_colour')->default('1');
                $table->tinyInteger('flag_open_volume')->default('1');
                $table->enum('status', ['0', '1'])->default('1');
                $table->timestamps();
            });
        }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_lock_fix_order');
    }
}
