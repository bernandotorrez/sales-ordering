<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Staudenmeir\LaravelMigrationViews\Facades\Schema;

class CreateViewModelColourMazda extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $query = 'SELECT tmm.id_model_mazda, tmm.id_model, tmm.name_model, tmm.kind_vehicle,
        tmm.model_year, tmm.engine_type, tmm.vehicle_type, tmm.wmi, tmm.model_moc,
        tmm.flag_show_additional AS flag_show_additional_model,
        tmm.flag_show_fix AS flag_show_fix_model,
        tmcm.id_model_colour_mazda, tmcm.id_model_colour, tmcm.id_colour,
        tmcm.flag_show_additional AS flag_show_additional_model_colour,
        tmcm.flag_show_fix AS flag_show_fix_model_colour,
        tcm.id_colour_mazda, tcm.name_colour, tcm.name_colour_global,
        tmcm.status
        FROM tbl_model_colour_mazda tmcm
        INNER JOIN tbl_model_mazda tmm ON tmm.id_model = tmcm.id_model
        INNER JOIN tbl_colour_mazda tcm ON tcm.id_colour = tmcm.id_colour
        WHERE tmm.status = "1" AND tcm.status = "1" AND tmcm.status = "1"';

        Schema::createOrReplaceView('view_model_colour_mazda', $query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropViewIfExists('view_model_colour_mazda');
    }
}
