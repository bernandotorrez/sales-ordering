<?php

use App\Models\SalesOrdering\MasterDealer;
use App\Traits\WithWrsApi;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Schema;

class CreateTblMasterDeler extends Migration
{
    use WithWrsApi;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tbl_master_dealer')) {
            Schema::create('tbl_master_dealer', function (Blueprint $table) {
                $table->id('id_master_dealer');
                $table->bigInteger('id_dealer');
                $table->string('dealer_name', 200);
                $table->text('address')->nullable();
                $table->string('phone', 50)->nullable();
                $table->string('fax', 25)->nullable();
                $table->string('email', 200)->nullable();
                $table->enum('status', ['0', '1'])->default('1');
                $table->timestamps();
            });
        }

        $this->inserData();

    }

    private function inserData()
    {
        $data = Http::withHeaders([
            'X-Auth-Token' => Config::get('constants.api_token')
        ])
        ->get($this->wrsApi.'/dealer')
        ->json();

        foreach($data['data'] as $dealer) {
            $checkDuplicate = MasterDealer::firstWhere('id_dealer', $dealer['kd_dealer']);

            if(!$checkDuplicate) {
                MasterDealer::create([
                    'id_dealer' => $dealer['kd_dealer'],
                    'dealer_name' => $dealer['nm_dealer'],
                    'address' => $dealer['address'],
                    'phone' => $dealer['telp'],
                    'fax' => $dealer['fax'],
                    'email' => $dealer['email1'],
                    'status' => '1'
                ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_master_dealer');
    }
}
