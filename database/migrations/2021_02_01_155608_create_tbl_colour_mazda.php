<?php

use App\Models\SalesOrdering\ColourMazda;
use App\Traits\WithWrsApi;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Schema;

class CreateTblColourMazda extends Migration
{
    use WithWrsApi;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tbl_colour_mazda')) {
            Schema::create('tbl_colour_mazda', function (Blueprint $table) {
                $table->id('id_colour_mazda');
                $table->string('id_colour', 25);
                $table->string('name_colour', 100);
                $table->string('name_colour_global', 100);
                $table->enum('flag_show_additional', ['0', '1'])->default('0');
                $table->enum('flag_show_fix', ['0', '1'])->default('0');
                $table->enum('status', ['0', '1'])->default('1');
                $table->timestamps();
            });
        }

        $this->insertData();
    }

    private function insertData()
    {
        $data = Http::withHeaders([
            'X-Auth-Token' => Config::get('constants.api_token')
        ])
        ->get($this->wrsApi.'/color/')
        ->json();

        foreach($data['data'] as $model)
        {
            $checkDuplicate = ColourMazda::firstWhere('id_colour', $model['kd_color']);

            if(!$checkDuplicate) {
                ColourMazda::create([
                    'id_colour' => $model['kd_color'],
                    'name_colour' => $model['nm_color_id'],
                    'name_colour_global' => $model['nm_color_global'],
                    'flag_show_additional' => '1',
                    'flag_show_fix' => '1',
                ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_colour_mazda');
    }
}
