<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Staudenmeir\LaravelMigrationViews\Facades\Schema;

class CreateViewParentMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $query = ' SELECT tpm.*, tug.nama_group
        FROM tbl_parent_menu tpm
        INNER JOIN tbl_user_group tug ON tug.id_user_group = tpm.id_user_group
        WHERE tpm.status = "1"';

        Schema::createOrReplaceView('view_parent_menu', $query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropViewIfExists('view_parent_menu');
    }
}
