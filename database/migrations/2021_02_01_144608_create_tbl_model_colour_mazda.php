<?php

use App\Models\SalesOrdering\ModelColourMazda;
use App\Traits\WithWrsApi;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Schema;

class CreateTblModelColourMazda extends Migration
{
    use WithWrsApi;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tbl_model_colour_mazda')) {
            Schema::create('tbl_model_colour_mazda', function (Blueprint $table) {
                $table->id('id_model_colour_mazda');
                $table->string('id_model_colour', 100);
                $table->string('id_model', 25);
                $table->string('id_colour', 25);
                $table->enum('flag_show_additional', ['0', '1'])->default('0');
                $table->enum('flag_show_fix', ['0', '1'])->default('0');
                $table->enum('status', ['0', '1'])->default('1');
                $table->timestamps();
            });
        }

        $this->insertData();

    }

    private function insertData()
    {
        $data = Http::withHeaders([
            'X-Auth-Token' => Config::get('constants.api_token')
        ])
        ->get($this->wrsApi.'/model-color/')
        ->json();

        foreach($data['data'] as $model)
        {
            $checkDuplicate = ModelColourMazda::firstWhere('id_model_colour', $model['kd_model_color']);

            if(!$checkDuplicate) {
                ModelColourMazda::create([
                    'id_model_colour' => $model['kd_model_color'],
                    'id_model' => $model['fk_model'],
                    'id_colour' => $model['fk_color'],
                    'flag_show_additional' => '1',
                    'flag_show_fix' => '1',
                    'status' => '1'
                ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_model_colour_mazda');
    }
}
