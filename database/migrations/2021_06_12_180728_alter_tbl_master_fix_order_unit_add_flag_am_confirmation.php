<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTblMasterFixOrderUnitAddFlagAmConfirmation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_master_fix_order_unit', function (Blueprint $table) {
            $table->tinyInteger('flag_am_confirmation')->default('0')->after('flag_submit_to_atpm');
            $table->dateTime('date_am_confirmation')->after('date_submit_atpm_order')->nullable();
            $table->string('am_confirmation_by', 200)->after('date_submit_atpm_order')->nullable();
            $table->string('nama_am_confirmation_by', 200)->after('am_confirmation_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_master_fix_order_unit', function (Blueprint $table) {
            $table->dropColumn('flag_am_confirmation');
            $table->dropColumn('date_am_confirmation');
            $table->dropColumn('am_confirmation_by');
            $table->dropColumn('nama_am_confirmation_by');
        });
    }
}
