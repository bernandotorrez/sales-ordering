<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Staudenmeir\LaravelMigrationViews\Facades\Schema;

class CreateViewSubChildMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $query = 'SELECT tscm.*, tcm.nama_child_menu, tpm.nama_parent_menu, tug.nama_group
        FROM tbl_sub_child_menu tscm
        INNER JOIN tbl_child_menu tcm ON tcm.id_child_menu = tscm.id_child_menu
        INNER JOIN tbl_parent_menu tpm ON tpm.id_parent_menu = tscm.id_parent_menu
        INNER JOIN tbl_user_group tug ON tug.id_user_group = tscm.id_user_group
        WHERE tscm.status = "1"';

        Schema::createOrReplaceView('view_sub_child_menu', $query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropViewIfExists('view_sub_child_menu');
    }
}
