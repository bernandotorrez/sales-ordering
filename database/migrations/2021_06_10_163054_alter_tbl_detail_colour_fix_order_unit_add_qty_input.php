<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTblDetailColourFixOrderUnitAddQtyInput extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_detail_color_fix_order_unit', function (Blueprint $table) {
            $table->tinyInteger('qty_input')->default('0')->nullable();
            $table->tinyInteger('qty_diff')->default('0')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_detail_color_fix_order_unit', function (Blueprint $table) {
            $table->dropColumn('qty_input');
            $table->dropColumn('qty_diff');
        });
    }
}
