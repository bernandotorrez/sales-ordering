<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Staudenmeir\LaravelMigrationViews\Facades\Schema;

class CreateViewRangeMonthRule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $query = 'SELECT trmfo.*, tmmo1.month AS nama_id_month, tmmo2.month as nama_month_id_to
        FROM tbl_range_month_fix_order trmfo
        INNER JOIN tbl_master_month_order tmmo1 ON tmmo1.id_month = trmfo.id_month
        INNER JOIN tbl_master_month_order tmmo2 ON tmmo2.id_month = trmfo.month_id_to';

        Schema::createOrReplaceView('view_range_month_rule', $query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropViewIfExists('view_range_month_rule');
    }
}
