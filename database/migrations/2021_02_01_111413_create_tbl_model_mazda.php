<?php

use App\Models\SalesOrdering\MazdaModel;
use App\Traits\WithWrsApi;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Schema;

class CreateTblModelMazda extends Migration
{
    use WithWrsApi;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tbl_model_mazda')) {
            Schema::create('tbl_model_mazda', function (Blueprint $table) {
                $table->id('id_model_mazda');
                $table->string('id_model', 25);
                $table->string('name_model', 100);
                $table->string('kind_vehicle', 100)->nullable();
                $table->char('model_year', 4)->nullable();
                $table->string('engine_type', 100)->nullable();
                $table->string('vehicle_type', 100)->nullable();
                $table->string('wmi', 100)->nullable();
                $table->string('model_moc', 100)->nullable();
                $table->enum('status', ['0', '1'])->default('1');
                $table->enum('flag_show_additional', ['0', '1'])->default('0');
                $table->enum('flag_show_fix', ['0', '1'])->default('0');
                $table->timestamps();
            });
        }

        $this->insertData();

    }

    private function insertData()
    {
        $data = Http::withHeaders([
            'X-Auth-Token' => Config::get('constants.api_token')
        ])
        ->get($this->wrsApi.'/model')
        ->json();

        foreach($data['data'] as $model)
        {
            $checkDuplicate = MazdaModel::firstWhere('id_model', $model['kd_model']);

            if(!$checkDuplicate) {
                MazdaModel::create([
                    'id_model' => $model['kd_model'],
                    'name_model' => $model['nm_model'],
                    'kind_vehicle' => $model['jenis_kendaraan'],
                    'model_year' => $model['model_year'],
                    'engine_type' => $model['engine_type'],
                    'vehicle_type' => $model['vehicle_type'],
                    'wmi' => $model['wmi'],
                    'model_moc' => $model['model_moc'],
                    'flag_show_additional' => '1',
                    'flag_show_fix' => '1',
                    'status' => '1'
                ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_model_mazda');
    }
}
