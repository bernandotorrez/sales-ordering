<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Staudenmeir\LaravelMigrationViews\Facades\Schema;

class CreateViewModelTypeMazda extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $query = 'SELECT tmm.id_model_mazda, tmm.id_model, tmm.name_model,
        tmm.kind_vehicle, tmm.model_year, tmm.engine_type, tmm.vehicle_type, tmm.wmi, tmm.model_moc,
        tmm.flag_show_additional AS flag_show_additional_model,
        tmm.flag_show_fix AS flag_show_fix_model,
        ttmm.id_type_model_mazda, ttmm.id_type_model, ttmm.name_type, ttmm.cylinder,
        ttmm.msc_code, ttmm.price, ttmm.tpt, ttmm.sut,
        ttmm.flag_show_additional AS flag_show_additional_type_model,
        ttmm.flag_show_fix AS flag_show_fix_type_model, ttmm.status
        FROM tbl_model_mazda tmm
        INNER JOIN tbl_type_model_mazda ttmm ON ttmm.id_model = tmm.id_model
        WHERE tmm.status = "1" AND ttmm.status = "1"';

        Schema::createOrReplaceView('view_model_type_mazda', $query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropViewIfExists('view_model_type_mazda');
    }
}
