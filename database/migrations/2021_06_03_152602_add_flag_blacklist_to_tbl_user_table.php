<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFlagBlacklistToTblUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_user', function (Blueprint $table) {
            $table->tinyInteger('flag_blacklist')->default('0')->after('status');
            $table->dateTime('date_blacklist')->after('flag_blacklist')->nullable();
            $table->string('blacklist_by', 200)->after('date_blacklist')->nullable();
            $table->text('remarks_blacklist')->after('blacklist_by')->nullable();
            $table->dateTime('date_whitelist')->after('remarks_blacklist')->nullable();
            $table->string('whitelist_by', 200)->after('date_whitelist')->nullable();
            $table->text('remarks_whitelist')->after('whitelist_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_user', function (Blueprint $table) {
            $table->dropColumn(['flag_blacklist', 'date_blacklist', 'blacklist_by', 'remark_blacklist', 'date_whitelist', 'whitelist_by', 'remarks_whitelist']);
        });
    }
}
