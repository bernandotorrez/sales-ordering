<?php

use App\Models\SalesOrdering\EmailAtpm;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblEmailAtpm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tbl_email_atpm')) {
            Schema::create('tbl_email_atpm', function (Blueprint $table) {
                $table->id('id_email_atpm');
                $table->string('email_atpm', 100);
                $table->string('id_dealer_level', 100)->nullable(true);
                $table->enum('status', ['0', '1'])->default('1');
                $table->timestamps();
            });
        }

        $this->insertData();
    }

    private function insertData()
    {
        EmailAtpm::create([
            'email_atpm' => 'jsuyamto@Mazda.co.id'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_email_atpm');
    }
}
