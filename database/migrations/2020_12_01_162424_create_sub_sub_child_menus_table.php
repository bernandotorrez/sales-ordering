<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateSubSubChildMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('tbl_sub_sub_child_menu')) {
            Schema::create('tbl_sub_sub_child_menu', function (Blueprint $table) {
                $table->id('id_sub_sub_child_menu');
                $table->bigInteger('id_sub_child_menu');
                $table->bigInteger('id_child_menu');
                $table->bigInteger('id_parent_menu');
                $table->bigInteger('id_user_group');
                $table->integer('sub_sub_child_position');
                $table->string('nama_sub_sub_child_menu', 100);
                $table->text('url');
                $table->string('icon', 100);
                $table->enum('status', ['0', '1'])->default(1);
                $table->timestamps();
            });
        }

        $this->insertData();
    }

    private function insertData()
    {
        DB::unprepared("INSERT INTO `tbl_sub_sub_child_menu` (`id_sub_sub_child_menu`, `id_sub_child_menu`, `id_child_menu`, `id_parent_menu`, `id_user_group`, `sub_sub_child_position`, `nama_sub_sub_child_menu`, `url`, `icon`, `status`, `created_at`, `updated_at`) VALUES
        (1, 1, 1, 1, 4, 1, 'Additional Order', 'sales/dealer/additional-order', '', '1', '2020-12-15 16:18:43', '2020-12-15 16:18:43'),
        (2, 1, 1, 1, 4, 1, 'Fix Order', 'sales/dealer/fix-order', '', '1', '2020-12-15 16:19:04', '2020-12-15 16:19:04'),
        (3, 2, 2, 2, 5, 1, 'Approval Fix Order', 'sales/dealer/fix-order-bm', '', '1', '2020-12-19 11:09:50', '2020-12-26 17:24:45'),
        (4, 2, 2, 2, 5, 2, 'Approval Additional order', 'sales/dealer/approval-bm', '', '1', '2020-12-19 14:48:34', '2020-12-26 17:24:38'),
        (5, 2, 2, 2, 5, 3, 'Approved BM', 'sales/dealer/approved-bm', '', '0', '2020-12-19 14:48:51', '2020-12-26 17:23:09'),
        (6, 3, 3, 3, 2, 1, 'Allocated Additional Order', 'sales/atpm/submit-atpm', '', '1', '2020-12-21 08:35:13', '2020-12-21 08:35:13'),
        (7, 3, 3, 3, 2, 2, 'Allocated Fix Order', 'sales/atpm/fix-order-atpm', '', '1', '2020-12-21 08:35:53', '2020-12-21 08:35:53'),
        (8, 4, 3, 3, 2, 2, 'Report Order', 'sales/atpm/report-order', '', '1', '2020-12-26 08:47:27', '2021-01-02 21:51:52'),
        (9, 5, 2, 2, 5, 2, 'Report Order', 'sales/dealer/report-order', '', '1', '2020-12-26 17:32:32', '2021-01-02 21:51:31'),
        (10, 4, 3, 3, 2, 1, 'Report Analyst', 'sales/atpm/report-analyst', '', '1', '2020-12-29 14:16:02', '2020-12-29 14:16:02'),
        (11, 5, 2, 2, 5, 1, 'Report Analyst', 'sales/dealer/report-analyst', '', '1', '2020-12-29 14:16:37', '2020-12-29 14:16:37'),
        (12, 6, 4, 4, 6, 1, 'Master Month', 'atpm-admin/master-month', '', '1', '2020-12-31 12:46:00', '2020-12-31 12:46:00'),
        (13, 6, 4, 4, 6, 2, 'Month Exception', 'atpm-admin/month-exception', '', '1', '2020-12-31 12:46:41', '2020-12-31 12:46:41'),
        (14, 6, 4, 4, 6, 3, 'Model Mazda', 'atpm-admin/model-mazda', '', '1', '2021-02-09 03:47:19', '2021-02-09 03:47:19'),
        (15, 6, 4, 4, 6, 4, 'Type Model Mazda', 'atpm-admin/type-model-mazda', '', '1', '2021-02-09 03:47:46', '2021-02-09 03:47:46'),
        (16, 6, 4, 4, 6, 5, 'Model Colour Mazda', 'atpm-admin/model-colour-mazda', '', '1', '2021-02-09 03:48:07', '2021-02-09 03:48:07'),
        (17, 7, 5, 5, 2, 1, 'Convert Excel to Txt', 'after-sales/warranty/atpm/convert', '', '1', '2021-02-09 03:48:07', '2021-02-09 03:48:07'),
        (18, 6, 4, 4, 6, 6, 'User Role', 'sales/atpm/user-role', '', '1', '2021-02-09 03:48:07', '2021-02-09 03:48:07');");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_sub_sub_child_menu');
    }
}
