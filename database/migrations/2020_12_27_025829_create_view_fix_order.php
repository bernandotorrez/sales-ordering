<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Staudenmeir\LaravelMigrationViews\Facades\Schema;

class CreateViewFixOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $query = 'select mfou.*, dcfou.id_colour, dcfou.colour_name, dcfou.qty, dtou.id_model, dtou.model_name, dtou.id_type, dtou.type_name, dtou.total_qty, dtou.year_production
        FROM tbl_master_fix_order_unit mfou
        INNER JOIN tbl_detail_fix_order_unit dtou ON dtou.id_master_fix_order_unit = mfou.id_master_fix_order_unit
        INNER JOIN tbl_detail_color_fix_order_unit dcfou ON dcfou.id_detail_fix_order_unit = dtou.id_detail_fix_order_unit
        WHERE mfou.status = "1" AND dtou.status = "1" AND dcfou.status = "1"';

        Schema::createOrReplaceView('view_fix_order', $query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropViewIfExists('view_fix_order');
    }
}
